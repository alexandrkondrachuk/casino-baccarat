import * as signalR from '@microsoft/signalr';
import * as _ from 'lodash';
// eslint-disable-next-line import/no-cycle
import store from '../store/store';
import Types from './Types';
import { app as appActions, game as gameActions, chat as chatActions } from '../store/actions';
import config from '../config';
import AlertModel from '../models/alert-model';
import BaccaratRoundModel from '../models/baccarat-round-model';
import ShoeStatModel from '../models/shoe-stat-model';
import PlayerInfoModel from '../models/player-info-model';
import BaccaratBetModel from '../models/baccarat-bet-model';
import ChatMessageModel from '../models/chat-message-model';
import CurrencyModel from '../models/currency-model';

const incomingMessages = Types.API__INCOMING__MESSAGES;

export default class Api {
    static RECONNECT__TIMEOUT = 5 * 1000;

    constructor({ url, options }) {
        this.url = url;
        this.options = options || {
            skipNegotiation: true,
            transport: signalR.HttpTransportType.WebSockets,
        };
        this.videoDelay = 0;

        this.connection = new signalR.HubConnectionBuilder()
            .configureLogging(signalR.LogLevel.Debug)
            .withUrl(this.url, this.options)
            .configureLogging(signalR.LogLevel.Information)
            .withAutomaticReconnect()
            .build();

        this.connection.onclose(this.start);
    }

    async start() {
        try {
            await this.connection.start();
            return true;
        } catch (err) {
            store.dispatch(gameActions.setReadyState(false));
            store.dispatch(appActions.setAlertModel(new AlertModel({
                type: 'error',
                open: true,
                autoHideDuration: 10 * 1000,
                text: 'Internal Server Error. Sorry for the inconvenience. Please, try again later',
            })));
            setTimeout(() => {
                this.start();
            }, Api.RECONNECT__TIMEOUT);
            return false;
        }
    }

    async init() {
        try {
            this.subscriptions();
            return await this.start();
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    subscriptions() {
        if (Array.isArray(incomingMessages) && incomingMessages.length > 0) {
            incomingMessages.forEach((message) => {
                this.connection.on(_.get(message, 'name'), _.get(this, `[${_.get(message, 'method')}]`));
            });
        }
    }

    onRoundState = (data) => {
        const state = store.getState();
        const bufferDelay = _.get(state, 'stream.bufferDelay', 0);
        const stage = _.get(data, 'stage');
        const id = _.get(state, 'game.round.id');
        const showLowBalanceMessage = _.get(state, 'game.showLowBalanceMessage');
        const initRoundId = _.get(state, 'game.initRoundId');
        this.videoDelay = bufferDelay;

        // Messages delay

        if (stage === 3 || stage === 4) {
            _.delay(() => {
                store.dispatch(gameActions.updateRoundStatus(new BaccaratRoundModel({ ...data })));
                if (!showLowBalanceMessage && (stage === 4)) store.dispatch(gameActions.setLowBalanceMessageStatus(true));
            }, (this.videoDelay + config.get('cardsDelay')) * 1000);
        } else {
            store.dispatch(gameActions.updateRoundStatus(new BaccaratRoundModel({ ...data })));
        }

        // Set init round id
        if (!initRoundId) store.dispatch(gameActions.setInitRound(id));

        // Skip current round
        if ((initRoundId === id) && (stage !== 1)) {
            store.dispatch(gameActions.skipCurrentRound(true));
        } else {
            store.dispatch(gameActions.skipCurrentRound());
        }
    };

    onShoeStat = (data) => {
        const state = store.getState();
        const isStatisticReady = _.get(state, 'game.shoeStat.isReady');
        const stage = _.get(state, 'game.round.stage');
        // Delay showing round statistics
        if (isStatisticReady && (stage !== 1) && (stage !== 2) && (stage !== 4)) {
            _.delay(() => {
                store.dispatch(gameActions.updateShoeStat(new ShoeStatModel({ ...data, isReady: true })));
            }, (this.videoDelay + config.get('statisticDelay')) * 1000);
        } else {
            store.dispatch(gameActions.updateShoeStat(new ShoeStatModel({ ...data, isReady: true })));
        }
    };

    onNewRound = (data) => {
        store.dispatch(gameActions.setBetsStatus(new ShoeStatModel({ ...data })));
    };

    onEndBetting = (data) => {
        store.dispatch(gameActions.updateRoundStatus(new BaccaratRoundModel({ ...data })));
        store.dispatch(gameActions.setBetsStatus(new ShoeStatModel({ ...data })));
    };

    onBetPlaced = (data) => {
        store.dispatch(gameActions.updatePlacedBet(new BaccaratBetModel({ ...data })));
    };

    onPlayerInfo = (data) => {
        const state = store.getState();
        const isPlayerInfoReady = _.get(state, 'game.playerInfo.isReady');
        const stage = _.get(state, 'game.round.stage');
        // Delay showing players' info
        if (isPlayerInfoReady && (stage !== 1) && (stage !== 2) && (stage !== 4)) {
            _.delay(() => {
                store.dispatch(gameActions.updatePlayerInfo(new PlayerInfoModel({ ...data, isReady: true, stage })));
            }, (this.videoDelay + config.get('playerInfoDelay')) * 1000);
        } else {
            store.dispatch(gameActions.updatePlayerInfo(new PlayerInfoModel({ ...data, isReady: true, stage })));
        }
    };

    onLeftUntilEndBet = (data) => {
        store.dispatch(gameActions.setBetsTimer(data));
    };

    onWin = (data) => {
        _.delay(() => {
            store.dispatch(gameActions.setWin(data));
        }, (this.videoDelay + config.get('cardsDelay')) * 1000);
    };

    onUndoBetPlaced = (data) => {
        console.log('Undo bet', data);
    };

    onChatUserMessage = (data) => {
        console.error('onChatUserMessage', data);
        store.dispatch(chatActions.saveChatMessage(new ChatMessageModel(data)));
    };

    onCurrency = (data) => {
        const {
            chipValue1 = 1,
            chipValue2 = 5,
            chipValue3 = 25,
            chipValue4 = 100,
            chipValue5 = 500,
            chipValue6 = 1000,
        } = data;
        const nominalArr = [
            chipValue1,
            chipValue2,
            chipValue3,
            chipValue4,
            chipValue5,
            chipValue6,
        ];

        store.dispatch(gameActions.setCurrencyData(new CurrencyModel(data)));
        store.dispatch(gameActions.setChipsNominal(nominalArr));
    };

    // Information Routes

    onPlayerBlocked = () => store.dispatch(appActions.setBlockStatus(true));

    onPlayerPaused = () => store.dispatch(appActions.setInactionStatus(true));

    onTechBreakStarted = () => store.dispatch(appActions.setMaintenanceStatus(true));

    onTechBreakStopped = () => store.dispatch(appActions.setMaintenanceStatus(false));

    doBet(model) {
        return this.connection.invoke('Bet', model);
    }

    /**
     *
     * @param type:string - LastBet | AllBets
     * @returns {Promise<any>}
     */
    doUndoBet(type = 'UndoBet') {
        return this.connection.invoke('UndoBet', type);
    }

    doSendChatMessage(message = '') {
        return this.connection.invoke('ChatUserMessage', message);
    }
}
