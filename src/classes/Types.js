import UIfx from 'uifx';
import alert from '../assets/sounds/alert.mp3';
import arcade from '../assets/sounds/arcade-game.mp3';
import chip from '../assets/sounds/chip-default.mp3';
import chipPrimary from '../assets/sounds/chip-primary.mp3';
import chipSecondary from '../assets/sounds/chip-secondary.mp3';
import consolas from '../assets/sounds/console.mp3';
import click from '../assets/sounds/mouse-click-01.mp3';
import clickSecondary from '../assets/sounds/mouse-click-02.mp3';
import popUp from '../assets/sounds/pop-up.mp3';
import press from '../assets/sounds/press.mp3';
import socket from '../assets/sounds/socket.mp3';
import success from '../assets/sounds/success-tone.mp3';
import system from '../assets/sounds/system.mp3';
import tap from '../assets/sounds/tap.mp3';

export default class Types {
    // @todo implement Types

    // Global App Info
    static GLOBAL_ATTR = 'Baccarat';

    static GLOBAL_VARIABLES = [
        {
            id: 0,
            name: `${Types.GLOBAL_ATTR}.branch`,
            path: 'branch',
            default: '',
        },
        {
            id: 1,
            name: `${Types.GLOBAL_ATTR}.tags`,
            path: 'tags',
            default: '',
        },
        {
            id: 2,
            name: `${Types.GLOBAL_ATTR}.date`,
            path: 'commit.date',
            default: '',
        },
        {
            id: 3,
            name: `${Types.GLOBAL_ATTR}.hash`,
            path: 'commit.hash',
            default: '',
        },
        {
            id: 4,
            name: `${Types.GLOBAL_ATTR}.message`,
            path: 'commit.message',
            default: '',
        },
        {
            id: 5,
            name: `${Types.GLOBAL_ATTR}.shortHash`,
            path: 'commit.shortHash',
            default: '',
        },
    ];

    // Sound Effects
    static SOUND_EFFECTS_DEFAULT_VOLUME = 1; // 0.1 - 1

    static SOUND_EFFECTS = [
        {
            id: 1,
            type: 'alert',
            sound: alert,
            uifx: new UIfx(alert, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 2,
            type: 'arcade',
            sound: arcade,
            uifx: new UIfx(arcade, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 3,
            type: 'chip',
            sound: chip,
            uifx: new UIfx(chip, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 4,
            type: 'chipPrimary',
            sound: chipPrimary,
            uifx: new UIfx(chipPrimary, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 5,
            type: 'chipSecondary',
            sound: chipSecondary,
            uifx: new UIfx(chipSecondary, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 6,
            type: 'consolas',
            sound: consolas,
            uifx: new UIfx(consolas, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 7,
            type: 'click',
            sound: click,
            uifx: new UIfx(click, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 8,
            type: 'clickSecondary',
            sound: clickSecondary,
            uifx: new UIfx(clickSecondary, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 9,
            type: 'popUp',
            sound: popUp,
            uifx: new UIfx(popUp, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 10,
            type: 'press',
            sound: press,
            uifx: new UIfx(press, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 11,
            type: 'socket',
            sound: socket,
            uifx: new UIfx(socket, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 12,
            type: 'success',
            sound: success,
            uifx: new UIfx(success, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 13,
            type: 'system',
            sound: system,
            uifx: new UIfx(system, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
        {
            id: 14,
            type: 'tap',
            sound: tap,
            uifx: new UIfx(tap, { volume: Types.SOUND_EFFECTS_DEFAULT_VOLUME }),
        },
    ];

    static SOUND_EFFECTS_MAP = Types.SOUND_EFFECTS.reduce((acc, effect) => acc.set(effect.type, effect), new Map());

    // Material UI Theme

    static MATERIAL_UI_THEME = {
        typography: {
            fontFamily: [
                '-apple-system',
                'Inter',
                'Roboto',
                'Helvetica Neue',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
            h1: {
                fontSize: '3rem',
            },
            h2: {
                fontSize: '1.875rem',
            },
            h3: {
                fontSize: '1.5rem',
            },
            h4: {
                fontSize: '1.0625rem',
            },
            h5: {
                fontSize: '0.75rem',
            },
            h6: {
                fontSize: '0.625rem',
            },
        },
        palette: {
            primary: {
                dark: '#4affff',
                main: '#96ffff',
                light: '#e3ffff',
                contrastText: '#FFF',
            },
            secondary: {
                dark: '#A28E5B',
                main: '#EBC200',
                light: '#FFEFCA',
                contrastText: '#FFF',
            },
            error: {
                dark: '#F8B0A3',
                main: '#FF473C',
                light: '#FFE5DB',
                contrastText: '#FFF',
            },
            text: {
                primary: '#545454',
                secondary: 'rgba(84, 84, 84, 0.5)',
            },
        },
    };

    // Device orientation
    static ORIENTATION__LANDSCAPE = 'landscape';

    static ORIENTATION__PORTRAIT = 'portrait';

    // Api incoming messages

    static API__INCOMING__MESSAGES = [
        { id: 0, name: 'RoundState', method: 'onRoundState' },
        { id: 1, name: 'ShoeStat', method: 'onShoeStat' },
        { id: 2, name: 'NewRound', method: 'onNewRound' },
        { id: 3, name: 'EndBetting', method: 'onEndBetting' },
        { id: 4, name: 'BetPlaced', method: 'onBetPlaced' },
        { id: 5, name: 'PlayerInfo', method: 'onPlayerInfo' },
        { id: 6, name: 'LeftUntilEndBet', method: 'onLeftUntilEndBet' },
        { id: 7, name: 'Win', method: 'onWin' },
        { id: 8, name: 'UndoBetPlaced', method: 'onUndoBetPlaced' },
        { id: 9, name: 'ChatUserMessage', method: 'onChatUserMessage' },
        { id: 10, name: 'Currency', method: 'onCurrency' },
        { id: 11, name: 'PlayerBlocked', method: 'onPlayerBlocked' },
        { id: 12, name: 'PlayerPaused', method: 'onPlayerPaused' },
        { id: 13, name: 'TechBreakStarted', method: 'onTechBreakStarted' },
        { id: 14, name: 'TechBreakStopped', method: 'onTechBreakStopped' },
    ];

    // Api Round Stage

    static API__ROUND__STAGE = {
        1: 'InitializeShoeOrDisplay',
        2: 'Bet',
        3: 'DealCard',
        4: 'FixRoundResult',
    };

    // Api Round Stage Description

    static API__STAGE__DESCRIPTION = {
        InitializeShoeOrDisplay: 'Place your bets',
        Bet: 'Place your bets',
        DealCard: 'Bets closed',
        FixRoundResult: '{name} win',
        WaitAnotherRound: 'Please, wait while new round start',
        LowBalance: 'Your balance is too low',
    };

    // Api Bet Type

    static API__BET__TYPE = {
        1: 'Player',
        2: 'Banker',
        3: 'Tie',
        4: 'PlayerPair',
        5: 'BankerPair',
    };

    static API__BET__TYPE__NAME = {
        1: 'Player',
        2: 'Banker',
        3: 'Tie',
        4: 'Player Pair',
        5: 'Banker Pair',
    };

    static API__BET__TYPE__CLASS = {
        Player: 'Player',
        Banker: 'Banker',
        Tie: 'Tie',
        'Player Pair': 'PlayerPair',
        'Banker Pair': 'BankerPair',
    };

    // Game Results

    static GAME__RESULTS = {
        ppair: {
            id: 0,
            className: 'player',
            text: 'player pair',
        },
        player: {
            id: 1,
            className: 'player',
            text: 'player',
        },
        tie: {
            id: 2,
            className: 'tie',
            text: 'tie',
        },
        banker: {
            id: 3,
            className: 'banker',
            text: 'banker',
        },
        bpair: {
            id: 4,
            className: 'banker',
            text: 'banker pair',
        },
    };

    // Chips

    static chips = [
        {
            id: 0, nominal: 1, color: 'blue', active: true,
        },
        {
            id: 1, nominal: 5, color: 'green', active: false,
        },
        {
            id: 3, nominal: 25, color: 'orange', active: false,
        },
        {
            id: 4, nominal: 100, color: 'red', active: false,
        },
        {
            id: 5, nominal: 500, color: 'purple', active: false,
        },
        {
            id: 6, nominal: 1000, color: 'dark', active: false,
        },
    ];

    static chipsMap = Types.chips.reduce((acc, c) => acc.set(c.id, c), new Map());

    // Beat Road

    static winMetric = {
        P: 'player',
        B: 'banker',
        T: 'tie',
    };

    static winPairMetric = {
        P: 'P',
        B: 'B',
    };

    // Date formats

    static dateFormatLong = 'YYYY-MM-DD HH:mm:ss';

    static dateFormatShort = 'YY/MM/DD HH:mm:ss';

    // Main Settings Tabs

    static mainSettingsTabs = [
        { index: 0, label: 'General', component: 'General' },
        { index: 1, label: 'Video', component: 'Video' },
        { index: 2, label: 'Sound', component: 'Sound' },
    ];

    static mainSettingsTabsMap = Types.mainSettingsTabs.reduce((acc, s) => acc.set(s.index, s), new Map());

    // Mobile Menu Items

    static mobileMenuItems = [
        {
            index: 0, type: 'chat', label: 'Chat', icon: 'chat', width: '100%', enabled: true,
        },
        {
            index: 1, type: 'limits', label: 'Limits', icon: 'limits', width: '90%', enabled: true,
        },
        {
            index: 2, type: 'history', label: 'History', icon: 'history', width: '90%', enabled: true,
        },
        {
            index: 3, type: 'settings', label: 'Settings', icon: 'settings', width: '90%', enabled: true,
        },
        {
            index: 4, type: 'help', label: 'Help', icon: 'help', width: '90%', enabled: true,
        },
    ];

    static mobileMenuItemsMap = Types.mobileMenuItems.reduce((acc, s) => acc.set(s.type, s), new Map());
}
