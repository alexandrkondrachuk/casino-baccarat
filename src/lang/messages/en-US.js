import { LOCALES } from '../constants';

export default {
    [LOCALES.en]: {
        'The game is loading': 'The game is loading',
        "Unfortunately the game doesn't work on your device.": "Unfortunately the game doesn't work on your device.",
    },
};
