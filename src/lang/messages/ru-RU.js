import { LOCALES } from '../constants';

export default {
    [LOCALES.ru]: {
        'The game is loading': 'The game is loading',
        "Unfortunately the game doesn't work on your device.": "Unfortunately the game doesn't work on your device.",
    },
};
