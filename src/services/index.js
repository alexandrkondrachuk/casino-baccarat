import * as _ from 'lodash';
import GitInfo from 'react-git-info/macro';
import * as axios from 'axios';
import Types from '../classes/Types';
// eslint-disable-next-line import/no-cycle
import Api from '../classes/Api';
// eslint-disable-next-line import/no-cycle
import Transport from '../classes/Transport';
import soundEffects from './sound-effects';
import config from '../config';

// eslint-disable-next-line import/no-mutable-exports
let api = null;

const transport = new Transport();

const setGitInfo = (attrName = 'name', attrPath = 'path', attrDefault = 'default') => {
    const gitInfo = GitInfo();
    window[Types.GLOBAL_ATTR] = !(_.has(window, Types.GLOBAL_ATTR)) ? {} : _.get(window, Types.GLOBAL_ATTR);
    Types.GLOBAL_VARIABLES.forEach((v) => {
        _.set(window, _.get(v, attrName), _.get(gitInfo, _.get(v, attrPath), _.get(v, attrDefault)));
    });
};

const getServerConfig = async () => {
    try {
        const conf = await axios.get(config.get('apiURLs.serverConfig'));
        const status = _.get(conf, 'status', 404);
        const data = _.get(conf, 'data', null);
        if (status === 200 && data) {
            const playerSettings = {
                url: _.get(data, 'RtmpUrl', ''),
                qualities: _.get(data, 'StreamConfig.qualities', {}),
            };
            return {
                config: data,
                playerSettings,
            };
        }
        return false;
    } catch (e) {
        console.log(e);
        return false;
    }
};

const initApi = (params) => {
    const defaultParams = config.get('server.params');
    const computedParams = { ...defaultParams, ...params };
    const queryString = Object.keys(computedParams).map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(computedParams[key])}`).join('&');
    const url = `${config.get('server.url')}?${queryString}`;
    api = new Api({ url });
    return { api, computedParams };
};

export {
    api,
    transport,
    setGitInfo,
    getServerConfig,
    soundEffects,
    initApi,
};
