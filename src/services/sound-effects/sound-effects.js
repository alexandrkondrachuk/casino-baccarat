import * as _ from 'lodash';
import Types from '../../classes/Types';

export const sounds = Types.SOUND_EFFECTS_MAP;

export const play = (type = 'alert', volume = 1) => {
    const soundInstance = sounds.get(type);
    const soundUI = _.get(soundInstance, 'uifx');
    if (soundInstance && soundUI && soundUI.play) {
        soundUI.play(volume);
    }
};
