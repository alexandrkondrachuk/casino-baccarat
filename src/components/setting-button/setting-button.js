import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import Icons from './icons';

import './setting-button.scss';

function SettingButton({ type, onClick, className }) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="36"
            height="36"
            fill="none"
            viewBox="0 0 36 36"
            className={cn({ SettingButton: true, [className]: !!className })}
            onClick={onClick}
        >
            <circle cx="18" cy="18" r="18" fill="#000" />
            <circle cx="18" cy="18" r="18" fill="url(#paint0_linear)" />
            <circle cx="18" cy="18" r="16" fill="url(#paint1_radial)" />
            {Icons[type]}
            <defs>
                <linearGradient
                    id="paint0_linear"
                    x1="-1.593"
                    x2="58.48"
                    y1="47.842"
                    y2="25.348"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#fff" stopOpacity="0" />
                    <stop offset="0.473" stopColor="#fff" />
                    <stop offset="1" stopColor="#fff" stopOpacity="0" />
                </linearGradient>
                <radialGradient
                    id="paint1_radial"
                    cx="0"
                    cy="0"
                    r="1"
                    gradientTransform="rotate(-95.375 42.236 24.48) scale(99.2599 20.7547)"
                    gradientUnits="userSpaceOnUse"
                >
                    <stop stopColor="#5E4D70" />
                    <stop offset="1" stopColor="#30253C" />
                </radialGradient>
            </defs>
        </svg>
    );
}

SettingButton.defaultProps = {
    type: 'menu',
    onClick: (e) => { console.log('SettingButton: ', e); },
    className: '',
};

SettingButton.propTypes = {
    type: PropTypes.oneOf(['volumeOn', 'volumeOff', 'quality', 'menu']),
    onClick: PropTypes.func,
    className: PropTypes.string,
};

export default SettingButton;
