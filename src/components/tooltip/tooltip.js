import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import { Tooltip } from 'reactstrap';

import './tooltip.scss';

function CustomTooltip({ targetId, placement, children }) {
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggle = () => setTooltipOpen(!tooltipOpen);

    return (
        <>
            <Tooltip
                className={cn({ Tooltip: true, [placement]: true })}
                placement={placement}
                isOpen={tooltipOpen}
                target={targetId}
                toggle={toggle}
            >
                { children }
            </Tooltip>
        </>
    );
}

CustomTooltip.defaultProps = {
    placement: 'auto',
    children: '',
};

CustomTooltip.propTypes = {
    targetId: PropTypes.string.isRequired,
    placement: PropTypes.oneOf([
        'auto',
        'top',
        'right',
        'bottom',
        'left',
    ]),
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
};

export default CustomTooltip;
