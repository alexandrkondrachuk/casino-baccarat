import React from 'react';
import * as PropTypes from 'prop-types';

import './bet-statistic.scss';

function BetStatistic({ percent, type }) {
    const colors = {
        player: '#0096FF',
        tie: '#00FF00',
        banker: '#FF0F0F',
    };
    const strokeDasharray = 339.292;
    const strokeDashoffset = strokeDasharray - percent * (strokeDasharray / 100);
    const color = colors[type];

    return (
        <svg width="120" height="120" viewBox="0 0 120 120">
            <circle cx="60" cy="60" r="50" />
            <circle
                cx="60"
                cy="60"
                r="54"
                fill="none"
                stroke={color}
                strokeOpacity="0.3"
                strokeWidth="8"
            />
            <circle
                cx="60"
                cy="60"
                r="54"
                fill="none"
                stroke={color}
                strokeDasharray={strokeDasharray}
                strokeDashoffset={strokeDashoffset}
                strokeWidth="8"
                transform="rotate(-90 60 60)"
                style={{
                    WebkitTransition: 'all 0.5s ease 0s',
                    transition: 'all 0.5s ease 0s',
                }}
            />
            <text
                x="50%"
                y="50%"
                fill={color}
                dy=".35em"
                fontFamily="Inter, sans-serif"
                fontSize="30"
                textAnchor="middle"
            >
                {percent}
                %
            </text>
        </svg>
    );
}

BetStatistic.defaultProps = {
    type: 'player',
    percent: 0,
};

BetStatistic.propTypes = {
    type: PropTypes.oneOf(['player', 'tie', 'banker']),
    percent: PropTypes.number,
};

export default BetStatistic;
