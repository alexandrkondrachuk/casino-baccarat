import React from 'react';
import './big-road-scale.scss';

function BigRoadScale() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39 9">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="39"
                height="6"
                x="0"
                y="0"
                className="BigRoadScale road-1 svg--2MOwB"
                data-role="Big-road"
                viewBox="0 0 39 6"
                style={{ width: '100%' }}
            >
                <svg className="svg--1nrnH" viewBox="0 0 39 6">
                    <path fill="#fff" d="M0 0H39V6H0z" />
                    <path
                        stroke="#A7A9AC"
                        strokeWidth="0.05"
                        d="M0 0.025L39 0.025"
                    />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 1L39 1" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 2L39 2" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 3L39 3" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 4L39 4" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 5L39 5" />
                    <path
                        stroke="#A7A9AC"
                        strokeWidth="0.05"
                        d="M0 5.975L39 5.975"
                    />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M0.025 0L0.025 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M1 0L1 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M2 0L2 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M3 0L3 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M4 0L4 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M5 0L5 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M6 0L6 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M7 0L7 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M8 0L8 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M9 0L9 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M10 0L10 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M11 0L11 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M12 0L12 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M13 0L13 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M14 0L14 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M15 0L15 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M16 0L16 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M17 0L17 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M18 0L18 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M19 0L19 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M20 0L20 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M21 0L21 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M22 0L22 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M23 0L23 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M24 0L24 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M25 0L25 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M26 0L26 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M27 0L27 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M28 0L28 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M29 0L29 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M30 0L30 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M31 0L31 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M32 0L32 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M33 0L33 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M34 0L34 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M35 0L35 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M36 0L36 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M37 0L37 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.05" d="M38 0L38 6" />
                    <path
                        stroke="#A7A9AC"
                        strokeWidth="0.05"
                        d="M38.975 0L38.975 6"
                    />
                </svg>
            </svg>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="13"
                height="3"
                x="0"
                y="6"
                className="road-2 svg--2MOwB"
                data-role="BigEye-road"
                viewBox="0 0 26 6"
                style={{ width: '100%' }}
            >
                <svg className="svg--1nrnH" viewBox="0 0 26 6">
                    <path fill="#fff" d="M0 0H26V6H0z" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 0.05L26 0.05" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 2L26 2" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 4L26 4" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 5.95L26 5.95" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0.05 0L0.05 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M2 0L2 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M4 0L4 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M6 0L6 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M8 0L8 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M10 0L10 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M12 0L12 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M14 0L14 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M16 0L16 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M18 0L18 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M20 0L20 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M22 0L22 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M24 0L24 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M25.95 0L25.95 6" />
                </svg>
            </svg>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="13"
                height="3"
                x="26"
                y="6"
                className="road-4 svg--2MOwB"
                data-role="Cockroach-road"
                viewBox="0 0 26 6"
                style={{ width: '100%' }}
            >
                <svg className="svg--1nrnH" viewBox="0 0 26 6">
                    <path fill="#fff" d="M0 0H26V6H0z" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 0.05L26 0.05" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 2L26 2" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 4L26 4" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 5.95L26 5.95" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0.05 0L0.05 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M2 0L2 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M4 0L4 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M6 0L6 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M8 0L8 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M10 0L10 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M12 0L12 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M14 0L14 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M16 0L16 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M18 0L18 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M20 0L20 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M22 0L22 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M24 0L24 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M25.95 0L25.95 6" />
                </svg>
            </svg>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="13"
                height="3"
                x="13"
                y="6"
                className="road-3 svg--2MOwB"
                data-role="Small-road"
                viewBox="0 0 26 6"
                style={{ width: '100%' }}
            >
                <svg className="svg--1nrnH" viewBox="0 0 26 6">
                    <path fill="#fff" d="M0 0H26V6H0z" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 0.05L26 0.05" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 2L26 2" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 4L26 4" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0 5.95L26 5.95" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M0.05 0L0.05 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M2 0L2 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M4 0L4 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M6 0L6 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M8 0L8 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M10 0L10 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M12 0L12 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M14 0L14 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M16 0L16 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M18 0L18 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M20 0L20 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M22 0L22 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M24 0L24 6" />
                    <path stroke="#A7A9AC" strokeWidth="0.1" d="M25.95 0L25.95 6" />
                </svg>
            </svg>
        </svg>
    );
}

export default BigRoadScale;
