import React from 'react';
import * as PropTypes from 'prop-types';
import {
    PCR,
    BCR,
} from '../index';

import './cockroach-road.scss';

const chockRoachRoad = {
    player: PCR,
    banker: BCR,
};

function ChockRoachRoad({ type }) {
    const TagName = chockRoachRoad[type];
    return (
        <>
            {TagName && (
                <TagName className="ChockRoachRoad" />
            )}
        </>
    );
}

ChockRoachRoad.propTypes = {
    type: PropTypes.oneOf(['player', 'banker']).isRequired,
};

export default ChockRoachRoad;
