import React from 'react';
import './p-c-r.scss';

function PCR() {
    return (
        <svg
            width="0.9"
            height="0.9"
            x="1.1"
            y="0.1"
            data-type="coordinates"
            data-x="1"
            data-y="0"
            className="PCR"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#185CC6"
                        d="M260 360L5 105l60-60 60-60 410 410 255 255-60 60-60 60"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                </svg>
            </svg>
        </svg>
    );
}

export default PCR;
