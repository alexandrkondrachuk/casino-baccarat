import BeadRoadScale from './bead-road-scale';
import BigRoadScale from './big-road-scale';

export {
    BeadRoadScale,
    BigRoadScale,
};
