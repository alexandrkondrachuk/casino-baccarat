import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import BeadRoadModel from '../../../../models/bead-road-model';
import BeadRoad from '../bead-road/bead-road';
import Types from '../../../../classes/Types';

import './bead-road-scale.scss';

function BeadRoadScale({ beadRoad }) {
    const { winMetric, winPairMetric } = Types;
    const beadRoadModels = beadRoad.map((stat, idx) => {
        const statChunk = _.chunk(Array.from(stat), 2);
        const type = _.get(winMetric, `[${_.get(statChunk, '[0][0]')}]`);
        const pairPlayer = ((_.get(statChunk, '[1][0]') === winPairMetric.P) || (_.get(statChunk, '[2][0]') === winPairMetric.P));
        const pairBanker = ((_.get(statChunk, '[1][0]') === winPairMetric.B) || (_.get(statChunk, '[2][0]') === winPairMetric.B));
        return new BeadRoadModel({
            index: idx,
            type,
            pairBanker,
            pairPlayer,
        });
    });

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="6"
            className="BeadRoadScale road-0 svg--2MOwB"
            data-mode="en"
            data-role="Bead-road"
            viewBox="0 0 20 6"
            style={{ width: '100%' }}
        >
            <svg className="svg--1nrnH" viewBox="0 0 20 6">
                <path fill="#fff" d="M0 0H20V6H0z" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 0.025L20 0.025" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 1L20 1" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 2L20 2" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 3L20 3" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 4L20 4" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 5L20 5" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0 5.975L20 5.975" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M0.025 0L0.025 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M1 0L1 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M2 0L2 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M3 0L3 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M4 0L4 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M5 0L5 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M6 0L6 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M7 0L7 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M8 0L8 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M9 0L9 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M10 0L10 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M11 0L11 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M12 0L12 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M13 0L13 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M14 0L14 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M15 0L15 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M16 0L16 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M17 0L17 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M18 0L18 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M19 0L19 6" />
                <path stroke="#A7A9AC" strokeWidth="0.05" d="M19.975 0L19.975 6" />
                {beadRoadModels.map((model) => (
                    <BeadRoad key={_.get(model, 'index')} model={model} />
                ))}
            </svg>
        </svg>
    );
}

BeadRoadScale.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    beadRoad: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
    const beadRoad = _.get(state, 'game.shoeStat.beadRoad');
    return {
        beadRoad,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(BeadRoadScale);
