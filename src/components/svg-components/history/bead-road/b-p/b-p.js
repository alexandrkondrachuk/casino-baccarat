import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './b-p.scss';

function BP({ className, onClick }) {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="4.05"
            y="2.05"
            data-type="coordinates"
            data-x="4"
            data-y="2"
            className={cn({ BP: true, [className]: !!className })}
            onClick={onClick}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#C52123"
                        d="M312 770c-64-13-157-67-203-117-22-24-55-74-72-111-27-59-31-78-31-152s4-93 31-152C115 71 281-21 453 10c37 6 67 15 67 20 0 4-7 24-15 43-56 135 71 282 215 249 25-6 47-8 50-5 3 2 5 40 4 84-1 69-5 87-37 152-44 88-103 147-190 187-69 33-165 45-235 30z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <circle cx="67" cy="63" r="10" fill="#185CC6" />
                    <text x="50%" y="75%" fill="#fff" fontSize="55" textAnchor="middle">
                        B
                    </text>
                </svg>
            </svg>
        </svg>
    );
}

BP.defaultProps = {
    className: '',
    onClick: (e) => { console.log('BeadRoad - onClick: ', e); },
};

BP.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default BP;
