import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import './t-b.scss';

function TB({ className, onClick }) {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="1.05"
            y="5.05"
            data-type="coordinates"
            data-x="1"
            data-y="5"
            className={cn({ TB: true, [className]: !!className })}
            onClick={onClick}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#51A548"
                        d="M308 768c-27-5-48-14-48-18s7-24 15-43c56-135-71-282-215-249-25 6-47 8-50 5-3-2-5-40-4-84 1-69 5-87 37-152C87 137 146 80 238 37c59-27 78-31 152-31s93 4 152 31c92 43 151 100 195 191 34 69 37 81 37 162s-3 93-37 162c-44 89-103 148-190 188-71 34-166 45-239 28z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <circle cx="10" cy="14" r="10" fill="#C52123" />
                    <text x="50%" y="75%" fill="#fff" fontSize="55" textAnchor="middle">
                        T
                    </text>
                </svg>
            </svg>
        </svg>
    );
}

TB.defaultProps = {
    className: '',
    onClick: (e) => { console.log('BeadRoad - onClick: ', e); },
};

TB.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default TB;
