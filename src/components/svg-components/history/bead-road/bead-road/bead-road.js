import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import BeadRoadModel from '../../../../../models/bead-road-model';
import './bead-road.scss';

function BeadRoad({ className, onClick, model }) {
    const {
        type, text, colors, pairPlayer, pairBanker, x = 0.05, y = 0.05,
    } = model;
    return (
        <>
            <svg
                width="0.95"
                height="0.95"
                x={x}
                y={y}
                data-type="coordinates"
                data-x="10"
                data-y="1"
                className={cn({ BeadRoad: true, [className]: !!className })}
                onClick={onClick}
            >
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="svg--1sK_i"
                    data-type="roadItem"
                    viewBox="0 0 80 80"
                >
                    <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                        {(!pairBanker && !pairPlayer) && (
                            <>
                                <path
                                    fill={colors[type]}
                                    d="M265 759C64 694-45 476 20 269 84 64 303-46 511 20c205 64 315 283 249 491-65 205-286 316-495 248z"
                                    data-type="roadItemColor"
                                    transform="matrix(.1 0 0 -.1 0 78)"
                                />
                            </>
                        )}
                        {(pairPlayer && !pairBanker) && (
                            <>
                                <path
                                    fill={colors[type]}
                                    d="M312 770c-64-13-157-67-203-117-22-24-55-74-72-111-27-59-31-78-31-152s4-93 31-152C115 71 281-21 453 10c37 6 67 15 67 20 0 4-7 24-15 43-56 135 71 282 215 249 25-6 47-8 50-5 3 2 5 40 4 84-1 69-5 87-37 152-44 88-103 147-190 187-69 33-165 45-235 30z"
                                    data-type="roadItemColor"
                                    transform="matrix(.1 0 0 -.1 0 78)"
                                />
                                <circle cx="67" cy="63" r="10" fill={colors.player} />
                            </>
                        )}
                        {(!pairPlayer && pairBanker) && (
                            <>
                                <path
                                    fill={colors[type]}
                                    d="M308 768c-27-5-48-14-48-18s7-24 15-43c56-135-71-282-215-249-25 6-47 8-50 5-3-2-5-40-4-84 1-69 5-87 37-152C87 137 146 80 238 37c59-27 78-31 152-31s93 4 152 31c92 43 151 100 195 191 34 69 37 81 37 162s-3 93-37 162c-44 89-103 148-190 188-71 34-166 45-239 28z"
                                    data-type="roadItemColor"
                                    transform="matrix(.1 0 0 -.1 0 78)"
                                />
                                <circle cx="10" cy="14" r="10" fill={colors.banker} />
                            </>
                        )}
                        {(pairPlayer && pairBanker) && (
                            <>
                                <path
                                    fill={colors[type]}
                                    d="M290 769c-19-6-36-12-38-13-1-2 5-19 13-39 48-114-25-243-144-255-27-2-63 0-80 6-36 13-41 4-41-78C0 178 175 0 383 0c54 0 147 17 147 27 0 2-8 15-17 30-23 35-23 118 1 167 12 23 38 50 65 68 40 25 54 29 101 25 30-2 62-7 71-11 14-6 18 1 24 42 21 156-80 329-232 399-65 30-190 41-253 22z"
                                    data-type="roadItemColor"
                                    transform="matrix(.1 0 0 -.1 0 78)"
                                />
                                <circle cx="10" cy="14" r="10" fill={colors.banker} />
                                <circle cx="67" cy="63" r="10" fill={colors.player} />
                            </>
                        )}
                        <text x="50%" y="75%" fill={colors.text} fontSize="55" textAnchor="middle">
                            {text[type]}
                        </text>
                    </svg>
                </svg>
            </svg>
        </>
    );
}

BeadRoad.defaultProps = {
    className: '',
    onClick: (e) => { console.log('BeadRoad - onClick: ', e); },
    model: new BeadRoadModel(),
};

BeadRoad.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    model: PropTypes.instanceOf(BeadRoadModel),
};

export default BeadRoad;
