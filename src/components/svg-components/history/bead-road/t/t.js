import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './t.scss';

function T({ className, onClick }) {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="2.05"
            y="2.05"
            data-type="coordinates"
            data-x="2"
            data-y="2"
            className={cn({ T: true, [className]: !!className })}
            onClick={onClick}
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#51A548"
                        d="M265 759C64 694-45 476 20 269 84 64 303-46 511 20c205 64 315 283 249 491-65 205-286 316-495 248z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <text x="50%" y="75%" fill="#fff" fontSize="55" textAnchor="middle">
                        T
                    </text>
                </svg>
            </svg>
        </svg>
    );
}

T.defaultProps = {
    className: '',
    onClick: (e) => { console.log('BeadRoad - onClick: ', e); },
};

T.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default T;
