import React from 'react';
import './b-d.scss';

function BD() {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="7.05"
            y="3.05"
            data-type="coordinates"
            data-x="7"
            data-y="3"
            className="BD"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#185CC6"
                        d="M290 769c-19-6-36-12-38-13-1-2 5-19 13-39 48-114-25-243-144-255-27-2-63 0-80 6-36 13-41 4-41-78C0 178 175 0 383 0c54 0 147 17 147 27 0 2-8 15-17 30-23 35-23 118 1 167 12 23 38 50 65 68 40 25 54 29 101 25 30-2 62-7 71-11 14-6 18 1 24 42 21 156-80 329-232 399-65 30-190 41-253 22z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <circle cx="10" cy="14" r="10" fill="#C52123" />
                    <circle cx="67" cy="63" r="10" fill="#185CC6" />
                    <text x="50%" y="75%" fill="#fff" fontSize="55" textAnchor="middle">
                        P
                    </text>
                </svg>
            </svg>
        </svg>
    );
}

export default BD;
