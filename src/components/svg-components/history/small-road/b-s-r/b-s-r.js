import React from 'react';
import './b-s-r.scss';

function BSR() {
    return (
        <svg
            width="0.9"
            height="0.9"
            x="0.1"
            y="1.1"
            data-type="coordinates"
            data-x="0"
            data-y="1"
            className="BSR"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#C52123"
                        d="M265 759C64 694-45 476 20 269 84 64 303-46 511 20c205 64 315 283 249 491-65 205-286 316-495 248z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                </svg>
            </svg>
        </svg>
    );
}

export default BSR;
