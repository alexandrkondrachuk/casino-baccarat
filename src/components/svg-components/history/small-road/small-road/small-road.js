import React from 'react';
import * as PropTypes from 'prop-types';
import {
    PSR,
    BSR,
} from '../index';

import './small-road.scss';

const smallRoad = {
    player: PSR,
    banker: BSR,
};

function SmallRoad({ type }) {
    const TagName = smallRoad[type];

    return (
        <>
            {TagName && (
                <TagName className="SmallRoad" />
            )}
        </>
    );
}

SmallRoad.propTypes = {
    type: PropTypes.oneOf(['player', 'banker']).isRequired,
};

export default SmallRoad;
