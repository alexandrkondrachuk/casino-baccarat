import React from 'react';
import './b-b-e-r.scss';

function BBER() {
    return (
        <svg
            width="0.9"
            height="0.9"
            x="20.1"
            y="2.1"
            data-type="coordinates"
            data-x="20"
            data-y="2"
            className="BBER"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#C52123"
                        d="M275 764C172 727 81 654 47 580c-81-174-56-342 67-466 154-153 398-153 552 0 123 124 148 292 67 466-21 44-109 132-153 153-105 48-221 60-305 31zm179-123c3-4 19-11 36-15 40-8 128-96 136-136 4-17 11-33 15-36 5-3 9-32 9-64s-4-61-9-64c-4-3-11-19-15-36-8-40-96-128-136-136-17-4-33-11-36-15-3-5-32-9-64-9s-61 4-64 9c-3 4-19 11-36 15-40 8-128 96-136 136-4 17-11 33-15 36-12 7-12 121 0 128 4 3 11 19 15 36 8 40 96 128 136 136 17 4 33 11 36 15 3 5 32 9 64 9s61-4 64-9z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                </svg>
            </svg>
        </svg>
    );
}

export default BBER;
