import React from 'react';
import * as PropTypes from 'prop-types';
import {
    PBER,
    BBER,
} from '../index';

import './big-eye-road.scss';

const bigEyeRoad = {
    player: PBER,
    banker: BBER,
};

function BigEyeRoad({ type }) {
    const TagName = bigEyeRoad[type];

    return (
        <>
            { TagName && <TagName /> }
        </>
    );
}

BigEyeRoad.propTypes = {
    type: PropTypes.oneOf(['player', 'banker']).isRequired,
};

export default BigEyeRoad;
