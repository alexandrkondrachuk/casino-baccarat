import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './b-t-b.scss';

function BTB({ className, onClick }) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            data-type="roadItem"
            viewBox="0 0 80 80"
            className={cn({ BTB: true, [className]: !!className })}
            onClick={onClick}
        >
            <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                <path
                    fill="#a7a9ac"
                    d="M308 768c-27-5-48-14-48-18s7-24 15-43c56-135-71-282-215-249-25 6-47 8-50 5-3-2-5-40-4-84 1-69 5-87 37-152C87 137 146 80 238 37c59-27 78-31 152-31s93 4 152 31c92 43 151 100 195 191 34 69 37 81 37 162s-3 93-37 162c-44 89-103 148-190 188-71 34-166 45-239 28z"
                    data-type="roadItemColor"
                    transform="matrix(.1 0 0 -.1 0 78)"
                />
                <circle cx="10" cy="14" r="10" fill="#C52123" />
            </svg>
        </svg>
    );
}

BTB.defaultProps = {
    className: '',
    onClick: (e) => { console.log('Total - onClick: ', e); },
};

BTB.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default BTB;
