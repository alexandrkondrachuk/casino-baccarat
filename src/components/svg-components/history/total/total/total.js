import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import {
    PT, BT, TT, PPT, BTB, PTN, BTN,
} from '../index';

import './total.scss';

const total = {
    PT, BT, TT, PPT, BTB, PTN, BTN,
};

function Total({ path, className, onClick }) {
    const TagName = total[path];
    return (
        <>
            {TagName && <TagName className={cn({ Total: true, [className]: !!className })} onClick={onClick} />}
        </>
    );
}

Total.defaultProps = {
    className: '',
    onClick: (e) => { console.log('Total - onClick: ', e); },
};

Total.propTypes = {
    path: PropTypes.oneOf([
        'PT', 'BT', 'TT', 'PPT', 'BTB', 'PTN', 'BTN',
    ]).isRequired,
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default Total;
