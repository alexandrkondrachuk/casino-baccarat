import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './b-t-n.scss';

function BTN({ className, onClick }) {
    return (
        <svg
            width="180"
            height="100"
            viewBox="0 0 180 100"
            className={cn({ BTN: true, [className]: !!className })}
            onClick={onClick}
        >
            <text x="90" y="76.923" fill="#fff" fontSize="76.923" textAnchor="middle">
                B?
            </text>
        </svg>
    );
}

BTN.defaultProps = {
    className: '',
    onClick: (e) => { console.log('Total - onClick: ', e); },
};

BTN.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default BTN;
