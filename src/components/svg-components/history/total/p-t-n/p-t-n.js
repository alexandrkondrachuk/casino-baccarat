import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './p-t-n.scss';

function PTN({ className, onClick }) {
    return (
        <svg
            width="180"
            height="100"
            viewBox="0 0 180 100"
            className={cn({ PTN: true, [className]: !!className })}
            onClick={onClick}
        >
            <text x="90" y="76.923" fill="#fff" fontSize="76.923" textAnchor="middle">
                P?
            </text>
        </svg>
    );
}

PTN.defaultProps = {
    className: '',
    onClick: (e) => { console.log('Total - onClick: ', e); },
};

PTN.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};

export default PTN;
