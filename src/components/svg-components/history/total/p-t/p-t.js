import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './p-t.scss';

export default function PT({ className, onClick }) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            data-type="roadItem"
            viewBox="0 0 80 80"
            className={cn({ PT: true, [className]: !!className })}
            onClick={onClick}
        >
            <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                <path
                    fill="#185CC6"
                    d="M265 759C64 694-45 476 20 269 84 64 303-46 511 20c205 64 315 283 249 491-65 205-286 316-495 248z"
                    data-type="roadItemColor"
                    transform="matrix(.1 0 0 -.1 0 78)"
                />
                <text x="50%" y="75%" fill="#fff" fontSize="55" textAnchor="middle">
                    P
                </text>
            </svg>
        </svg>
    );
}

PT.defaultProps = {
    className: '',
    onClick: (e) => { console.log('Total - onClick: ', e); },
};

PT.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
};
