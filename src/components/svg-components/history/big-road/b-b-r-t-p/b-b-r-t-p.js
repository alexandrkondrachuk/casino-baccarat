import React from 'react';
import './b-b-r-t-p.scss';

function BBRTP() {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="1.05"
            y="3.05"
            data-type="coordinates"
            data-x="1"
            data-y="3"
            className="BBRTP"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#C52123"
                        d="M275 764c-78-28-110-48-161-98C-9 542-34 374 47 200 68 156 156 68 200 47 302 0 415-14 487 12c32 12 45 40 23 53-5 3-10 22-10 41 0 37-22 56-43 35-13-13-123-15-131-2-3 4-19 11-36 15-40 8-128 96-136 136-4 17-11 33-15 36-12 7-12 121 0 128 4 3 11 19 15 36 8 40 96 128 136 136 17 4 33 11 36 15 3 5 32 9 64 9s61-4 64-9c3-4 19-11 36-15 38-7 127-94 136-133 4-15 11-33 16-40 5-6 8-37 7-67-1-31-1-57 2-60 2-2 32-6 67-10l62-7v73c0 115-37 206-114 284-52 52-84 71-166 98-55 19-171 19-225 0z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <path
                        fill="#51A548"
                        d="M260 360L5 105l60-60 60-60 410 410 255 255-60 60-60 60"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <circle cx="67" cy="63" r="10" fill="#185CC6" />
                </svg>
            </svg>
        </svg>
    );
}

export default BBRTP;
