import React from 'react';
import './b-b-r-t-d.scss';

function BBRTD() {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="8.05"
            y="0.05"
            data-type="coordinates"
            data-x="8"
            data-y="0"
            className="BBRTD"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <g fill="#C52123" data-type="roadItemColor">
                        <path
                            d="M283 767c-36-12-42-27-18-47 9-7 15-29 15-51 0-32 4-39 19-39 11 0 23 5 26 10 8 12 122 13 129 1 3-4 19-11 36-15 38-7 127-94 136-133 4-15 11-33 16-40 5-6 8-38 7-70l-3-58 58-8c32-5 61-5 65-2 10 11 6 137-7 182-34 121-137 227-259 266-57 19-169 20-220 4zM0 398C1 211 107 69 291 11c47-15 154-14 196 1 32 12 43 31 24 43-6 3-13 26-17 50-6 43-24 59-39 35-8-12-122-13-129-1-3 4-19 11-36 15-38 7-127 94-136 133-4 15-11 33-16 40-5 6-8 37-7 67 1 31 1 57-2 60-2 2-32 6-66 10l-63 7v-73z"
                            transform="matrix(.1 0 0 -.1 0 78)"
                        />
                    </g>
                    <path
                        fill="#51A548"
                        d="M260 360L5 105l60-60 60-60 410 410 255 255-60 60-60 60"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <circle cx="10" cy="14" r="10" fill="#C52123" />
                    <circle cx="67" cy="63" r="10" fill="#185CC6" />
                </svg>
            </svg>
        </svg>
    );
}

export default BBRTD;
