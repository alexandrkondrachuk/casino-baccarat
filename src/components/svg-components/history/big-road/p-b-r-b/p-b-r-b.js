import React from 'react';
import './p-b-r-b.scss';

function PBRB() {
    return (
        <svg
            width="0.95"
            height="0.95"
            x="1.05"
            y="2.05"
            data-type="coordinates"
            data-x="1"
            data-y="2"
            className="PBRB"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg--1sK_i"
                data-type="roadItem"
                viewBox="0 0 80 80"
            >
                <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                    <path
                        fill="#185CC6"
                        d="M288 767c-28-11-38-40-18-52 6-3 10-22 10-41 0-37 22-56 43-35 13 13 123 15 131 2 3-4 19-11 36-15 40-8 128-96 136-136 4-17 11-33 15-36 5-3 9-32 9-64s-4-61-9-64c-4-3-11-19-15-36-8-40-96-128-136-136-17-4-33-11-36-15-7-12-121-12-128 0-3 4-19 11-36 15-38 7-127 94-136 133-4 15-11 33-16 40-5 6-8 37-7 67 1 31 1 57-2 60-2 2-32 6-67 10l-62 7v-73c0-115 37-206 114-284 154-153 398-153 552 0 153 154 153 398 0 552-52 52-84 71-166 98-51 18-171 19-212 3z"
                        data-type="roadItemColor"
                        transform="matrix(.1 0 0 -.1 0 78)"
                    />
                    <circle cx="10" cy="14" r="10" fill="#C52123" />
                </svg>
            </svg>
        </svg>
    );
}

export default PBRB;
