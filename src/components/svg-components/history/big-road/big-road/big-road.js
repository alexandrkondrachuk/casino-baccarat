import React from 'react';
import * as PropTypes from 'prop-types';
import BigRoadModel from '../../../../../models/big-road-model';

import './big-road.scss';

function BigRoad({ roadModel }) {
    const {
        colors, pairBanker, pairPlayer, text, tie, type,
    } = roadModel;
    return (
        <>
            <svg
                width="0.95"
                height="0.95"
                x="28.05"
                y="0.05"
                data-type="coordinates"
                data-x="28"
                data-y="0"
                className="BigRoad"
            >
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="svg--1sK_i"
                    data-type="roadItem"
                    viewBox="0 0 80 80"
                >
                    <svg width="68" height="68" x="6" y="6" viewBox="0 0 80 80">
                        { (!pairBanker && !pairPlayer) && (
                            <path
                                fill={colors[type]}
                                d="M275 764C172 727 81 654 47 580c-81-174-56-342 67-466 154-153 398-153 552 0 123 124 148 292 67 466-21 44-109 132-153 153-105 48-221 60-305 31zm179-123c3-4 19-11 36-15 40-8 128-96 136-136 4-17 11-33 15-36 5-3 9-32 9-64s-4-61-9-64c-4-3-11-19-15-36-8-40-96-128-136-136-17-4-33-11-36-15-3-5-32-9-64-9s-61 4-64 9c-3 4-19 11-36 15-40 8-128 96-136 136-4 17-11 33-15 36-12 7-12 121 0 128 4 3 11 19 15 36 8 40 96 128 136 136 17 4 33 11 36 15 3 5 32 9 64 9s61-4 64-9z"
                                data-type="roadItemColor"
                                transform="matrix(.1 0 0 -.1 0 78)"
                            />
                        ) }
                        {(pairBanker && !pairPlayer) && (
                            <>
                                <path
                                    fill={colors[type]}
                                    d="M288 767c-28-11-38-40-18-52 6-3 10-22 10-41 0-37 22-56 43-35 13 13 123 15 131 2 3-4 19-11 36-15 40-8 128-96 136-136 4-17 11-33 15-36 5-3 9-32 9-64s-4-61-9-64c-4-3-11-19-15-36-8-40-96-128-136-136-17-4-33-11-36-15-7-12-121-12-128 0-3 4-19 11-36 15-38 7-127 94-136 133-4 15-11 33-16 40-5 6-8 37-7 67 1 31 1 57-2 60-2 2-32 6-67 10l-62 7v-73c0-115 37-206 114-284 154-153 398-153 552 0 153 154 153 398 0 552-52 52-84 71-166 98-51 18-171 19-212 3z"
                                    data-type="roadItemColor"
                                    transform="matrix(.1 0 0 -.1 0 78)"
                                />
                                <circle cx="10" cy="14" r="10" fill={colors.banker} />
                            </>
                        )}
                        {(!pairBanker && pairPlayer) && (
                            <>
                                <path
                                    fill={colors[type]}
                                    d="M275 764c-78-28-110-48-161-98C-9 542-34 374 47 200 68 156 156 68 200 47 302 0 415-14 487 12c32 12 45 40 23 53-5 3-10 22-10 41 0 37-22 56-43 35-13-13-123-15-131-2-3 4-19 11-36 15-40 8-128 96-136 136-4 17-11 33-15 36-12 7-12 121 0 128 4 3 11 19 15 36 8 40 96 128 136 136 17 4 33 11 36 15 3 5 32 9 64 9s61-4 64-9c3-4 19-11 36-15 38-7 127-94 136-133 4-15 11-33 16-40 5-6 8-37 7-67-1-31-1-57 2-60 2-2 32-6 67-10l62-7v73c0 115-37 206-114 284-52 52-84 71-166 98-55 19-171 19-225 0z"
                                    data-type="roadItemColor"
                                    transform="matrix(.1 0 0 -.1 0 78)"
                                />
                                <circle cx="67" cy="63" r="10" fill={colors.player} />
                            </>
                        )}
                        {(pairBanker && pairPlayer) && (
                            <>
                                <g fill={colors[type]} data-type="roadItemColor">
                                    <path
                                        d="M283 767c-36-12-42-27-18-47 9-7 15-29 15-51 0-32 4-39 19-39 11 0 23 5 26 10 8 12 122 13 129 1 3-4 19-11 36-15 38-7 127-94 136-133 4-15 11-33 16-40 5-6 8-38 7-70l-3-58 58-8c32-5 61-5 65-2 10 11 6 137-7 182-34 121-137 227-259 266-57 19-169 20-220 4zM0 398C1 211 107 69 291 11c47-15 154-14 196 1 32 12 43 31 24 43-6 3-13 26-17 50-6 43-24 59-39 35-8-12-122-13-129-1-3 4-19 11-36 15-38 7-127 94-136 133-4 15-11 33-16 40-5 6-8 37-7 67 1 31 1 57-2 60-2 2-32 6-66 10l-63 7v-73z"
                                        transform="matrix(.1 0 0 -.1 0 78)"
                                    />
                                </g>
                                <circle cx="10" cy="14" r="10" fill={colors.banker} />
                                <circle cx="67" cy="63" r="10" fill={colors.player} />
                            </>
                        )}
                        {tie && (
                            <path
                                fill={colors.tie}
                                d="M260 360L5 105l60-60 60-60 410 410 255 255-60 60-60 60"
                                data-type="roadItemColor"
                                transform="matrix(.1 0 0 -.1 0 78)"
                            />
                        )}
                        {!!text && (
                            <text x="50%" y="75%" fontSize="55" textAnchor="middle">
                                {text}
                            </text>
                        )}
                    </svg>
                </svg>
            </svg>
        </>
    );
}

BigRoad.defaultProps = {
    roadModel: new BigRoadModel(),
};

BigRoad.propTypes = {
    roadModel: PropTypes.instanceOf(BigRoadModel),
};

export default BigRoad;
