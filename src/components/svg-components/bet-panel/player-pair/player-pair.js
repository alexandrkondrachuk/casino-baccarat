import React from 'react';

import './player-pair.scss';

function PlayerPairSvg() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="74"
            height="147"
            preserveAspectRatio="none"
            viewBox="0 0 74 147"
            className="PlayerPairSvg"
        >
            <defs>
                <path
                    id="SideBetspotShape-0"
                    d="M73.748 0H73.6C33.142 0 .344 32.773.344 73.2s32.798 73.2 73.256 73.2h.148V0z"
                />
            </defs>
            <g fill="none" fillRule="evenodd" transform="matrix(-1 0 0 1 74.092 0)">
                <use
                    id="player-pair-shape"
                    fill="#0061EE"
                    fillOpacity="0.4"
                    xlinkHref="#SideBetspotShape-0"
                />
                <path
                    stroke="#0061EE"
                    strokeWidth="0.8"
                    d="M73.348.4C33.227.536.744 33.077.744 73.2s32.483 72.664 72.604 72.8V.4z"
                />
            </g>
        </svg>
    );
}

export default PlayerPairSvg;
