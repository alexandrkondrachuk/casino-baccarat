import PlayerSvg from './player';
import PlayerPairSvg from './player-pair';
import BankerSvg from './banker';
import BankerPairSvg from './banker-pair';
import TieSvg from './tie';

export {
    PlayerSvg,
    PlayerPairSvg,
    BankerSvg,
    BankerPairSvg,
    TieSvg,
};
