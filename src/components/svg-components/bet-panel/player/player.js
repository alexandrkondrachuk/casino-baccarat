import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './player.scss';

function PlayerSvg({
    className, fill, fillOpacity, stroke, strokeWidth,
}) {
    return (
        <svg
            fill={fill}
            fillOpacity={fillOpacity}
            stroke={stroke}
            strokeWidth={strokeWidth}
            className={cn({ PlayerSvg: true, [className]: !!className })}
            preserveAspectRatio="none"
            viewBox="0 0 225 185"
        >
            <path d="M.3.4h223.8v184H82.6V90.3A86 86 0 00.3 4.4v-4z" />
        </svg>
    );
}

PlayerSvg.defaultProps = {
    className: '',
    fill: '#0061EE',
    fillOpacity: '0.35',
    stroke: '#0061EE',
    strokeWidth: '1.5',
};

PlayerSvg.propTypes = {
    className: PropTypes.string,
    fill: PropTypes.string,
    fillOpacity: PropTypes.string,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.string,
};

export default PlayerSvg;
