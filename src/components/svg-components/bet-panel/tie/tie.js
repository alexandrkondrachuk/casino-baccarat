import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './tie.scss';

function TieSvg({
    className, fill, fillOpacity, stroke, strokeWidth,
}) {
    return (
        <svg
            fill={fill}
            fillOpacity={fillOpacity}
            fillRule="evenodd"
            stroke={stroke}
            strokeWidth={strokeWidth}
            className={cn({ TieSvg: true, [className]: !!className })}
            preserveAspectRatio="none"
            viewBox="0 0 157 174"
        >
            <path d="M156 173.4V79.8C153.8 35.5 119.8 1 78.5 1S3.2 35.5 1.1 79.3L1 173.4h155z" />
        </svg>
    );
}

TieSvg.defaultProps = {
    className: '',
    fill: '#0F0',
    fillOpacity: '0.35',
    stroke: '#0F0',
    strokeWidth: '1.5',
};

TieSvg.propTypes = {
    className: PropTypes.string,
    fill: PropTypes.string,
    fillOpacity: PropTypes.string,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.string,
};

export default TieSvg;
