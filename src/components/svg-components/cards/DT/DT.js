import React from 'react';
import './DT.scss';

function DT() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="240"
            height="336"
            className="card DT"
            preserveAspectRatio="none"
            viewBox="-120 -168 240 336"
        >
            <symbol
                id="SDT"
                preserveAspectRatio="xMinYMid"
                viewBox="-600 -600 1200 1200"
            >
                <path
                    fill="red"
                    d="M-400 0C-350 0 0-450 0-500 0-450 350 0 400 0 350 0 0 450 0 500 0 450-350 0-400 0z"
                />
            </symbol>
            <symbol
                id="VDT"
                preserveAspectRatio="xMinYMid"
                viewBox="-500 -500 1000 1000"
            >
                <path
                    fill="none"
                    stroke="red"
                    strokeLinecap="square"
                    strokeMiterlimit="1.5"
                    strokeWidth="80"
                    d="M-260 430v-860M-50 0v-310a150 150 0 01300 0v620a150 150 0 01-300 0z"
                />
            </symbol>
            <rect
                width="239"
                height="335"
                x="-119.5"
                y="-167.5"
                fill="#fff"
                stroke="#000"
                strokeWidth="4"
                rx="12"
                ry="12"
            />
            <g transform="translate(172, 172) scale(2)">
                <use height="70" x="-122" y="-156" xlinkHref="#VDT" />
                <use height="58.558" x="-116.279" y="-81" xlinkHref="#SDT" />
            </g>
        </svg>
    );
}

export default DT;
