import React from 'react';
import './CA.scss';

function CA() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="240"
            height="336"
            className="card CA"
            preserveAspectRatio="none"
            viewBox="-120 -168 240 336"
        >
            <symbol
                id="SCA"
                preserveAspectRatio="xMinYMid"
                viewBox="-600 -600 1200 1200"
            >
                <path d="M30 150c5 235 55 250 100 350h-260c45-100 95-115 100-350a10 10 0 00-20 0 210 210 0 11-74-201 10 10 0 0014-14 230 230 0 11220 0 10 10 0 0014 14 210 210 0 11-74 201 10 10 0 00-20 0z" />
            </symbol>
            <symbol
                id="VCA"
                preserveAspectRatio="xMinYMid"
                viewBox="-500 -500 1000 1000"
            >
                <path
                    fill="none"
                    stroke="#000"
                    strokeLinecap="square"
                    strokeMiterlimit="1.5"
                    strokeWidth="80"
                    d="M-270 460h160m-90-10L0-460l200 910m-90 10h160m-390-330h240"
                />
            </symbol>
            <rect
                width="239"
                height="335"
                x="-119.5"
                y="-167.5"
                fill="#fff"
                stroke="#000"
                strokeWidth="4"
                rx="12"
                ry="12"
            />
            <g transform="translate(172, 172) scale(2)">
                <use height="70" x="-122" y="-156" xlinkHref="#VCA" />
                <use height="58.558" x="-116.279" y="-81" xlinkHref="#SCA" />
            </g>
        </svg>
    );
}

export default CA;
