import React from 'react';
import './C5.scss';

function C5() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="240"
            height="336"
            className="card C5"
            preserveAspectRatio="none"
            viewBox="-120 -168 240 336"
        >
            <symbol
                id="SC5"
                preserveAspectRatio="xMinYMid"
                viewBox="-600 -600 1200 1200"
            >
                <path d="M30 150c5 235 55 250 100 350h-260c45-100 95-115 100-350a10 10 0 00-20 0 210 210 0 11-74-201 10 10 0 0014-14 230 230 0 11220 0 10 10 0 0014 14 210 210 0 11-74 201 10 10 0 00-20 0z" />
            </symbol>
            <symbol
                id="VC5"
                preserveAspectRatio="xMinYMid"
                viewBox="-500 -500 1000 1000"
            >
                <path
                    fill="none"
                    stroke="#000"
                    strokeLinecap="square"
                    strokeMiterlimit="1.5"
                    strokeWidth="80"
                    d="M170-460h-345l-35 345s10-85 210-85c100 0 255 120 255 320S180 460-20 460s-235-175-235-175"
                />
            </symbol>
            <rect
                width="239"
                height="335"
                x="-119.5"
                y="-167.5"
                fill="#fff"
                stroke="#000"
                strokeWidth="4"
                rx="12"
                ry="12"
            />
            <g transform="translate(172, 172) scale(2)">
                <use height="70" x="-122" y="-156" xlinkHref="#VC5" />
                <use height="58.558" x="-116.279" y="-81" xlinkHref="#SC5" />
            </g>
        </svg>
    );
}

export default C5;
