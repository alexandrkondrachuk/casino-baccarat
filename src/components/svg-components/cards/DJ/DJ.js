import React from 'react';
import './DJ.scss';

function DJ() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="240"
            height="336"
            className="card DJ"
            preserveAspectRatio="none"
            viewBox="-120 -168 240 336"
        >
            <symbol
                id="SDJ"
                preserveAspectRatio="xMinYMid"
                viewBox="-600 -600 1200 1200"
            >
                <path
                    fill="red"
                    d="M-400 0C-350 0 0-450 0-500 0-450 350 0 400 0 350 0 0 450 0 500 0 450-350 0-400 0z"
                />
            </symbol>
            <symbol
                id="VDJ"
                preserveAspectRatio="xMinYMid"
                viewBox="-500 -500 1000 1000"
            >
                <path
                    fill="none"
                    stroke="red"
                    strokeLinecap="square"
                    strokeMiterlimit="1.5"
                    strokeWidth="80"
                    d="M50-460h200m-100 0v710a100 100 0 01-400 0v-30"
                />
            </symbol>
            <rect
                width="239"
                height="335"
                x="-119.5"
                y="-167.5"
                fill="#fff"
                stroke="#000"
                strokeWidth="4"
                rx="12"
                ry="12"
            />
            <g transform="translate(172, 172) scale(2)">
                <use height="70" x="-122" y="-156" xlinkHref="#VDJ" />
                <use height="58.558" x="-116.279" y="-81" xlinkHref="#SDJ" />
            </g>
        </svg>
    );
}

export default DJ;
