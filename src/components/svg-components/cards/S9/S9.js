import React from 'react';
import './S9.scss';

function S9() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            width="240"
            height="336"
            className="card S9"
            preserveAspectRatio="none"
            viewBox="-120 -168 240 336"
        >
            <symbol
                id="SS9"
                preserveAspectRatio="xMinYMid"
                viewBox="-600 -600 1200 1200"
            >
                <path d="M0-500c100 250 355 400 355 685a150 150 0 01-300 0 10 10 0 00-20 0c0 200 50 215 95 315h-260c45-100 95-115 95-315a10 10 0 00-20 0 150 150 0 01-300 0c0-285 255-435 355-685z" />
            </symbol>
            <symbol
                id="VS9"
                preserveAspectRatio="xMinYMid"
                viewBox="-500 -500 1000 1000"
            >
                <path
                    fill="none"
                    stroke="#000"
                    strokeLinecap="square"
                    strokeMiterlimit="1.5"
                    strokeWidth="80"
                    d="M250-100a250 250 0 01-500 0v-110a250 250 0 01500 0v420A250 250 0 010 460c-150 0-180-60-200-85"
                />
            </symbol>
            <rect
                width="239"
                height="335"
                x="-119.5"
                y="-167.5"
                fill="#fff"
                stroke="#000"
                strokeWidth="4"
                rx="12"
                ry="12"
            />
            <g transform="translate(172, 172) scale(2)">
                <use height="70" x="-122" y="-156" xlinkHref="#VS9" />
                <use height="58.558" x="-116.279" y="-81" xlinkHref="#SS9" />
            </g>
        </svg>
    );
}

export default S9;
