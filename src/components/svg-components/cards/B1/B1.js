import React from 'react';
import './B1.scss';

function B1() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="240"
            height="336"
            className="card B1"
            preserveAspectRatio="none"
            viewBox="-120 -168 240 336"
        >
            <defs>
                <pattern id="B1" width="6" height="6" patternUnits="userSpaceOnUse">
                    <path d="M3 0l3 3-3 3-3-3z" />
                </pattern>
            </defs>
            <rect
                width="239"
                height="335"
                x="-119.5"
                y="-167.5"
                fill="#fff"
                stroke="#000"
                rx="12"
                ry="12"
            />
            <rect
                width="216"
                height="312"
                x="-108"
                y="-156"
                fill="url(#B1)"
                strokeWidth="4"
                rx="12"
                ry="12"
            />
        </svg>
    );
}

export default B1;
