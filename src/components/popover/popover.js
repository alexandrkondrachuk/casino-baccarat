import React from 'react';
import * as PropTypes from 'prop-types';
import {
    Popover, PopoverHeader, PopoverBody,
} from 'reactstrap';
import { Icon } from '../svg-components';

import './popover.scss';

const PopoverCustom = ({
    target, isOpen, title, children, toggle, placement,
}) => (
    <>
        <Popover className="Popover" placement={placement} isOpen={isOpen} target={target}>
            <PopoverHeader>
                <span>{title}</span>
                {' '}
                <span>
                    <Icon className="Popover__Close" path="close" onClick={toggle} />
                    {' '}
                </span>
            </PopoverHeader>
            <PopoverBody>{children}</PopoverBody>
        </Popover>
    </>
);

PopoverCustom.defaultProps = {
    placement: 'right-end',
};

PopoverCustom.propTypes = {
    target: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    toggle: PropTypes.func.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node,
        PropTypes.func,
        PropTypes.string,
    ]).isRequired,
    placement: PropTypes.oneOf([
        'auto',
        'auto-start',
        'auto-end',
        'top',
        'top-start',
        'top-end',
        'right',
        'right-start',
        'right-end',
        'bottom',
        'bottom-start',
        'bottom-end',
        'left',
        'left-start',
        'left-end',
    ]),
};

export default PopoverCustom;
