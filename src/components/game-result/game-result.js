import React from 'react';
import * as _ from 'lodash';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import { Animated } from 'react-animated-css';
import getSymbolFromCurrency from 'currency-symbol-map';
import Types from '../../classes/Types';
import { number } from '../../lang';

import './game-result.scss';

function GameResult({
    type, winAmount, showRoundTimer, currency,
}) {
    const results = Types.GAME__RESULTS;
    const current = _.get(results, `[${type}]`);
    const currencySymbol = getSymbolFromCurrency(currency);
    const { className, text } = current;

    return (
        <div className={cn({ GameResult: true, [className]: !!className })}>
            <Animated className="Animated" animationIn="zoomIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible={showRoundTimer}>
                <div className="GameResult__Before" />
                <div className="GameResult__Main">
                    <h2 className="status">{text}</h2>
                </div>
                <div className="GameResult__After">
                    {!!winAmount && (
                        <div className="winner">
                            <h3>You win</h3>
                            <p>
                                {currencySymbol}
                                {number({ value: winAmount })}
                            </p>
                        </div>
                    )}
                </div>
            </Animated>
        </div>
    );
}

GameResult.defaultProps = {
    type: 'player',
    winAmount: 0,
};

GameResult.propTypes = {
    type: PropTypes.oneOf(['ppair', 'player', 'tie', 'banker', 'bpair']),
    winAmount: PropTypes.number,
    showRoundTimer: PropTypes.bool.isRequired,
    currency: PropTypes.string.isRequired,
};

export default GameResult;
