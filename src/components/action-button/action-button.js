import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';

import './action-button.scss';

function ActionButton({ onClick, className, text }) {
    return (
        <button type="button" className={cn({ ActionButton: true, [className]: !!className })} onClick={onClick}>
            <div className="ActionButton__Border-Primary">
                <div className="ActionButton__Border-Secondary">
                    <div className="ActionButton__Border-Default">
                        <span>{ text }</span>
                    </div>
                </div>
            </div>
        </button>
    );
}

ActionButton.defaultProps = {
    onClick: (e) => { console.log('ActionButton: ', e); },
    className: '',
    text: '',
};

ActionButton.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
    text: PropTypes.string,
};

export default ActionButton;
