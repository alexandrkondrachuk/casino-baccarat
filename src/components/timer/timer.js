import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import TimerIcon from '@material-ui/icons/Timer';
import T from 'react-compound-timer';
import * as _ from 'lodash';

import './timer.scss';
import { Animated } from 'react-animated-css';

class Timer extends Component {
    static TIMER__LENGTH__PROPERTY = '--timer-length';

    static TIMER__WIDTH__PROPERTY = '--timer-width';

    componentDidMount() {
        this.startTimer();
    }

    componentWillUnmount() {
        this.resetTimer();
    }

    startTimer = () => {
        const { length } = this.props;
        const rootStyle = document.documentElement.style;

        rootStyle.setProperty(Timer.TIMER__LENGTH__PROPERTY, length);
        _.delay(() => {
            rootStyle.setProperty(Timer.TIMER__WIDTH__PROPERTY, '0');
        }, 0);
    };

    resetTimer = () => {
        const { length } = this.props;
        const rootStyle = document.documentElement.style;

        rootStyle.setProperty(Timer.TIMER__LENGTH__PROPERTY, length);
        rootStyle.setProperty(Timer.TIMER__WIDTH__PROPERTY, '100');
    };

    render() {
        const { length } = this.props;
        const seconds = length * 1000;

        return (
            <div className="Timer">
                <Animated animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible>
                    <div className="Timer__Percent" />
                    <div className="Timer__Number">
                        <div className="Timer__Number__Content">
                            <TimerIcon />
                            {' '}
                            <span>
                                <T initialTime={seconds} direction="backward">
                                    <T.Seconds />
                                </T>
                            </span>
                        </div>
                    </div>
                </Animated>
            </div>
        );
    }
}

Timer.defaultProps = {
    length: 10,
};

Timer.propTypes = {
    length: PropTypes.number,
};

export default Timer;
