import React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import * as PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import AlertModel from '../../models/alert-model';
import { app as appActions } from '../../store/actions';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

function AlertCustom({ model, dispatch }) {
    const {
        open,
        type,
        autoHideDuration,
        text,
        anchorOrigin,
    } = model;
    const classes = useStyles();
    const alertTypes = {
        error: 'error',
        warning: 'warning',
        info: 'info',
        success: 'success',
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        dispatch(appActions.setAlertModel(new AlertModel({ ...model, open: false })));
    };

    const useType = () => _.get(alertTypes, `[${type}]`, 'success');

    const alertType = useType();

    return (
        <div className={classes.root}>
            <Snackbar
                open={open}
                autoHideDuration={autoHideDuration}
                onClose={handleClose}
                anchorOrigin={anchorOrigin}
            >
                <Alert onClose={handleClose} severity={alertType}>
                    { text }
                </Alert>
            </Snackbar>
        </div>
    );
}

AlertCustom.propTypes = {
    model: PropTypes.instanceOf(AlertModel).isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const model = _.get(state, 'app.alertModel');
    return {
        model: model || new AlertModel(),
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(AlertCustom);
