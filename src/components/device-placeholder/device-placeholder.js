import React from 'react';
import { t } from '../../lang';

import './device-placeholder.scss';

export default function DevicePlaceholder() {
    return (
        <div className="DevicePlaceholder">
            <div className="DevicePlaceholder__Icon" />
            <h1 className="DevicePlaceholder__Message">{ t("Unfortunately the game doesn't work on your device.") }</h1>
        </div>
    );
}
