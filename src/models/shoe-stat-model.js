import BaseModel from './base';

export default class ShoeStatModel extends BaseModel {
    constructor(initDate) {
        super();
        this.bankerWins = 0;
        this.beadRoad = [];
        this.playerWins = 0;
        this.roundsCount = 0;
        this.isReady = false;

        this.copyFrom(initDate);
    }
}
