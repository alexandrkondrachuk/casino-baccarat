import BaseModel from './base';

export default class StreamH5liveModel extends BaseModel {
    constructor(initData) {
        super();
        this.rtmp = {
            url: 'rtmp://bintu-play.nanocosmos.de/play',
            streamname: 'oecLT-Rxmbm',
        };
        this.server = {
            websocket: 'wss://bintu-h5live.nanocosmos.de:443/h5live/stream.mp4',
            hls: 'https://bintu-h5live.nanocosmos.de:443/h5live/http/playlist.m3u8',
            progressive: 'https://bintu-h5live.nanocosmos.de:443/h5live/http/stream.mp4',
        };
        this.token = '';
        this.security = {};
        this.copyFrom(initData);
    }
}
