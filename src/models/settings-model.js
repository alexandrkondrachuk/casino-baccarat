import BaseModel from './base';

export default class SettingsModel extends BaseModel {
    constructor(initDate) {
        super();
        this.showChat = false;
        this.enableSound = false;
        this.showConfig = false;
        this.showHistory = false;
        this.showHelp = false;
        this.enableFullscreen = false;
        this.infoOpen = false;

        this.copyFrom(initDate);
    }
}
