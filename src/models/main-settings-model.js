import BaseModel from './base';

export default class MainSettingsModel extends BaseModel {
    constructor(initData) {
        super();
        this.voice = 0;
        this.sound = 0;
        this.hideMessages = false;
        this.showBetStatistic = true;
        this.copyFrom(initData);
    }
}
