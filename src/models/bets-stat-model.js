import BaseModel from './base';

export default class BetsStatModel extends BaseModel {
    constructor(initDate) {
        super();
        this.bankerBetsCount = 0;
        this.bankerBetsSum = 0;
        this.bankerPercent = 0;
        this.playerBetsCount = 0;
        this.playerBetsSum = 0;
        this.playerPercent = 0;
        this.tieBetsCount = 0;
        this.tieBetsSum = 0;
        this.tiePercent = 0;

        this.copyFrom(initDate);
    }
}
