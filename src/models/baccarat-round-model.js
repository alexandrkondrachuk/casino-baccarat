import * as _ from 'lodash';
import BaseModel from './base';
import Types from '../classes/Types';
import BetsStatModel from './bets-stat-model';

const roundStage = Types.API__ROUND__STAGE;

export default class BaccaratRoundModel extends BaseModel {
    constructor(initDate) {
        super();
        this.bankerCards = [];
        this.bankerScore = 0;
        this.betsStat = new BetsStatModel();
        this.currentCard = null;
        this.isCardForPlayer = false;
        this.isFinished = false;
        this.playerCards = [];
        this.playerScore = 0;
        this.result = '';
        this.stage = 1;
        this.winBetTypes = [];
        // eslint-disable-next-line prefer-destructuring
        this.stageName = roundStage[this.stage];

        this.copyFrom(initDate);
        this.init(initDate);
    }

    init(initDate) {
        this.betsStat = new BetsStatModel(_.get(initDate, 'betsStat'));
        this.stageName = roundStage[_.get(initDate, 'stage')];
    }
}
