import BaseModel from './base';

export default class ChatMessageModel extends BaseModel {
    constructor(initData) {
        super();
        this.id = new Date().getTime();
        this.playerId = '';
        this.message = '';
        this.date = new Date().getTime();
        this.copyFrom(initData);
    }
}
