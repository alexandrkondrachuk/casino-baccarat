import * as _ from 'lodash';
import BaseModel from './base';
import Types from '../classes/Types';

export default class BaccaratBetModel extends BaseModel {
    constructor(initData) {
        super();
        this.BetValue = 0;
        this.BetType = 1;
        // eslint-disable-next-line prefer-destructuring
        this.BetName = Types.API__BET__TYPE[0];
        this.copyFrom(initData);
        this.init(initData);
    }

    init(initData) {
        this.BetName = Types.API__BET__TYPE[_.get(initData, 'BetType', 1)];
    }
}
