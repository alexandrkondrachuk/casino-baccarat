import BaseModel from './base';

export default class BigRoadModel extends BaseModel {
    constructor(initDate) {
        super();
        this.type = 'banker'; // banker | player
        this.pairPlayer = false;
        this.pairBanker = false;
        this.tie = false;
        this.text = '';
        this.colors = {
            banker: '#C52123',
            player: '#185CC6',
            tie: '#51A548',
        };
        this.copyFrom(initDate);
    }
}
