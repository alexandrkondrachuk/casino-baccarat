import BaseModel from './base';

export default class StreamInfoModel extends BaseModel {
    constructor(initData) {
        super();
        this.bitrate = 2500;
        this.width = 1920;
        this.height = 1080;
        this.framerate = 30;
        this.copyFrom(initData);
    }
}
