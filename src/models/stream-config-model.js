import BaseModel from './base';
import StreamModel from './stream-model';
import StreamInfoModel from './stream-info-model';
import StreamH5liveModel from './stream-h5live-model';

export default class StreamConfigModel extends BaseModel {
    constructor(initData) {
        super();
        this.source = {
            entries: [
                new StreamModel({
                    index: 0,
                    label: '1080p',
                    info: new StreamInfoModel({
                        bitrate: 3000,
                        width: 1920,
                        height: 1080,
                        framerate: 30,
                    }),
                    h5live: new StreamH5liveModel({
                        rtmp: {
                            url: 'rtmp://bintu-play.nanocosmos.de/play',
                            streamname: 'oecLT-Rxmbm',
                        },
                    }),
                }),
                new StreamModel({
                    index: 1,
                    label: '480p',
                    info: new StreamInfoModel({
                        bitrate: 640,
                        width: 852,
                        height: 480,
                        framerate: 25,
                    }),
                    h5live: new StreamH5liveModel({
                        rtmp: {
                            url: 'rtmp://bintu-play.nanocosmos.de/play',
                            streamname: 'oecLT-SjA6n',
                        },
                    }),
                }),
                new StreamModel({
                    index: 2,
                    label: '360p',
                    info: new StreamInfoModel({
                        bitrate: 400,
                        width: 640,
                        height: 360,
                        framerate: 15,
                    }),
                    h5live: new StreamH5liveModel({
                        rtmp: {
                            url: 'rtmp://bintu-play.nanocosmos.de/play',
                            streamname: 'oecLT-K14MO',
                        },
                    }),
                }),
            ],
            options: {
                adaption: {
                    rule: 'deviationOfMean', // enable ABR
                },
                switch: {
                    method: 'server',
                    pauseOnError: false,
                    forcePlay: true,
                    fastStart: false,
                    timeout: 10,
                },
            },
            startIndex: 0,
        };
        this.playback = {
            autoplay: true,
            automute: false,
            muted: true,
            forceTech: 'h5live',
            flashplayer: '//demo.nanocosmos.de/nanoplayer/nano.player.swf',
        };
        this.style = {
            controls: false,
            displayMutedAutoplay: false,
            scaling: 'fill',
            // poster: './casino-online.jpg',
        };
        this.metrics = {
            accountId: 'nanocosmos1',
            accountKey: 'nc1wj472649fkjah',
            userId: 'nanoplayer-demo',
            eventId: 'nanocosmos-demo',
            statsInterval: 10,
            customField1: 'demo',
            customField2: 'public',
            customField3: 'online resource',
        };
        this.copyFrom(initData);
    }
}
