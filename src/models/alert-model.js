import BaseModel from './base';

export default class AlertModel extends BaseModel {
    constructor(initDate) {
        super();
        this.type = 'success';
        this.open = false;
        this.autoHideDuration = 15 * 1000;
        this.anchorOrigin = { vertical: 'top', horizontal: 'center' };
        this.text = 'Alert Message';
        this.copyFrom(initDate);
    }
}
