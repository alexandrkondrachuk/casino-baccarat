import * as _ from 'lodash';
import BaseModel from './base';

export default class BeadRoadModel extends BaseModel {
    constructor(initData) {
        super();
        this.index = 0;
        this.type = 'banker'; // banker | player | tie
        this.pairPlayer = false;
        this.pairBanker = false;
        this.x = 0.05;
        this.y = 0.05;
        this.text = {
            banker: 'B',
            player: 'P',
            tie: 'T',
        };
        this.colors = {
            banker: '#C52123',
            player: '#185CC6',
            tie: '#51A548',
            text: '#FFF',
        };
        this.copyFrom(initData);
        this.init(initData);
    }

    init(initData) {
        const index = _.get(initData, 'index', 0);
        // eslint-disable-next-line radix
        const x = parseInt(index / 6);
        // eslint-disable-next-line radix
        const y = parseInt(index - (6 * x));
        this.x = x + 0.05;
        this.y = y + 0.05;
    }
}
