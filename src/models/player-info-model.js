import * as _ from 'lodash';
import BaseModel from './base';
import config from '../config';

export default class PlayerInfoModel extends BaseModel {
    constructor(initData) {
        super();
        this.balance = 0;
        this.cid = 'parimatch';
        this.country = null;
        this.currency = 'USD';
        this.lang = 'en';
        this.nickname = null;
        this.sessionToken = '';
        this.totalBetOfCurrentRound = '';
        this.isReady = false;
        this.isLowBalance = false;

        this.copyFrom(initData);
        this.checkBalance(initData);
    }

    checkBalance(initData) {
        const minLimit = config.get('limits.min');
        const balance = _.get(initData, 'balance', 0);
        const stage = _.get(initData, 'stage');

        this.isLowBalance = ((balance < minLimit) && (stage !== 1) && (stage !== 2));
    }
}
