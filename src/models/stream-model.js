import BaseModel from './base';
import StreamInfoModel from './stream-info-model';
import StreamH5liveModel from './stream-h5live-model';

export default class StreamModel extends BaseModel {
    constructor(initData) {
        super();
        this.index = 0;
        this.label = '1080p';
        this.tag = '';
        this.info = new StreamInfoModel();
        this.hls = '';
        this.h5live = new StreamH5liveModel();
        this.bintu = {};
        this.copyFrom(initData);
    }
}
