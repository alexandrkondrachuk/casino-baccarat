const port = 5001;
const domain = 'https://product.nextgenplay.tk';
const dataPoint = 'Baccarat';

const configProduction = {
    version: '1.1.4',
    name: 'Gold Baccarat Live',
    title: 'Casino - Baccarat - (production)',
    server: {
        host: `${domain}:${port}`,
        url: `${domain}:${port}/${dataPoint}`,
        params: {
            cid: 'parimatch',
            productId: 'NGP_Baccarat',
            sessionToken: 'qwerty1234',
            lang: 'en',
        },
    },
    apiURLs: {
        main: '/',
        auth: '/auth',
        desktop: '/desktop',
        tablet: '/tablet',
        mobile: '/mobile',
        issue: '/issue',
        maintenance: '/maintenance',
        inaction: '/inaction',
        serverConfig: './server_config.json',
    },
    apiDataPoints: {
        history: '/api/Bet/GetBetsFromPlayerId',
    },
    isSupportMobile: true,
    isSupportTablet: false,
    limits: {
        min: 1,
        max: 10000,
    },
    currency: 'EUR',
    authDelay: 2 * 1000,
    undoDelay: 1000,
    cardsDelay: 1.5,
    statisticDelay: 1.75,
    playerInfoDelay: 1.75,
    playerThrottleDelay: 10000,
    playerInitDelay: 3500,
    menuResetDelay: 400,
    animationDuration: {
        settings: 500,
        limits: 500,
        chatMessage: 400,
        menuItem: 1000,
    },
};
export default configProduction;
