import config from 'react-global-configuration';
import development from './config.development';
import production from './config.production';
import def from './config.default';

if (process.env.REACT_APP_CUSTOM_NODE_ENV === 'development') {
    config.set(development);
} else if (process.env.REACT_APP_CUSTOM_NODE_ENV === 'production') {
    config.set(production);
} else {
    config.set(def);
}
export default config;
