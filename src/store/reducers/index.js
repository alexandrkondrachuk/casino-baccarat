import { combineReducers } from 'redux';

import app from './app';
import game from './game';
import stream from './stream';
import chat from './chat';

export default combineReducers({
    app, game, stream, chat,
});
