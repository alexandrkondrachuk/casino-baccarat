import { Record, Map } from 'immutable';
import { chat as chatActions } from '../../actions';

const initState = Record({
    messages: new Map(),
    messageIndex: 0,
    filteredMessages: [],
})();

function saveMessage(list, message, id) {
    return list.set(id, message);
}

function filterMessages(list) {
    const listObj = list.toObject();
    return Object.values(listObj).map((v) => (v)).reverse();
}

const chat = (state = initState, action) => {
    switch (action.type) {
    case chatActions.CHAT__SAVE__MESSAGE:
        return state.merge({ messages: saveMessage(state.messages, action.payload, state.messageIndex), filteredMessages: filterMessages(saveMessage(state.messages, action.payload, state.messageIndex)), messageIndex: state.messageIndex + 1 });
    default:
        return state;
    }
};

export default chat;
