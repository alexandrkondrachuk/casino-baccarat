import { Record } from 'immutable';
import { app as appActions } from '../../actions';
import { LOCALES } from '../../../lang';
import MainSettingsModel from '../../../models/main-settings-model';

const initState = Record({
    lang: LOCALES.en,
    soundEffectVolume: 0,
    alertModel: null,
    soundEffects: null,
    maskStatus: true,
    settings: {
        voice: true,
        sound: true,
    },
    mainSettings: new MainSettingsModel(),
    blockStatus: false,
    maintenanceStatus: false,
    inactionStatus: false,
    reconnectStatus: false,
})();

function mergeSettings(settings, target) {
    return { ...settings, [_.get(target, 'name')]: _.get(target, 'value') };
}

function setMainSettings(settings, target) {
    return new MainSettingsModel({ ...settings, [_.get(target, 'name')]: _.get(target, 'value') });
}
const app = (state = initState, action) => {
    switch (action.type) {
    case appActions.APP__CHANGE__LANG:
        return state.merge({ lang: action.payload });
    case appActions.APP__SET__SOUND__EFFECT__VOLUME:
        return state.merge({ soundEffectVolume: action.payload });
    case appActions.APP__SET__ALERT__MODEL:
        return state.merge({ alertModel: action.payload });
    case appActions.APP__SET__MASK__STATUS:
        return state.merge({ maskStatus: action.payload });
    case appActions.APP__SET__SETTING:
        return state.merge({ settings: mergeSettings(state.settings, action.payload) });
    case appActions.APP__SET__MAIN__SETTING:
        return state.merge({ mainSettings: setMainSettings(state.mainSettings, action.payload) });
    case appActions.APP__SET__BLOCK__STATUS:
        return state.merge({ blockStatus: action.payload });
    case appActions.APP__SET__MAINTENANCE__STATUS:
        return state.merge({ maintenanceStatus: action.payload });
    case appActions.APP__SET__INACTION__STATUS:
        return state.merge({ inactionStatus: action.payload });
    case appActions.APP__SET__RECONNECT__STATUS:
        return state.merge({ reconnectStatus: action.payload });
    default:
        return state;
    }
};

export default app;
