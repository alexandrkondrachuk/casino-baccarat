import { Record, Map } from 'immutable';
import { game as gameActions } from '../../actions';
import BaccaratRoundModel from '../../../models/baccarat-round-model';
import ShoeStatModel from '../../../models/shoe-stat-model';
import PlayerInfoModel from '../../../models/player-info-model';
import BaccaratBetModel from '../../../models/baccarat-bet-model';
import ChipModel from '../../../models/chip-model';
import SettingsModel from '../../../models/settings-model';
import Types from '../../../classes/Types';
import CurrencyModel from '../../../models/currency-model';

const chips = Types.chips.reduce((acc, c) => acc.set(c.id, new ChipModel({ ...c })), new Map());

const initialState = new Record({
    settings: new SettingsModel(),
    readyState: false,
    showLimits: false,
    query: null,
    queryComputed: null,
    round: new BaccaratRoundModel(),
    shoeStat: new ShoeStatModel(),
    playerInfo: new PlayerInfoModel(),
    betsStatus: new BaccaratRoundModel(),
    placedBet: new BaccaratBetModel(),
    currency: new CurrencyModel(),
    betsTimer: 10 * 1000,
    showTimer: false,
    roundTimer: 10 * 1000,
    showRoundTimer: false,
    chips,
    activeChip: chips.get(0),
    win: 0,
    initRoundId: null,
    skipCurrentRound: false,
    showLowBalanceMessage: true,
    showActiveChipPanel: false,
})();

function setActiveChip(currentChips, id) {
    const chipsArray = Object.values(currentChips.toObject()).map((v) => v);
    const updatedArray = chipsArray.map((c) => {
        if (c.id === id) {
            return {
                ...c, active: true,
            };
        }
        return {
            ...c, active: false,
        };
    });
    return updatedArray.reduce((acc, c) => acc.set(c.id, new ChipModel({ ...c })), new Map());
}

function getActiveChip(list, id) {
    return new ChipModel({ ...list.get(id) });
}

function setChipNominal(c, nominal) {
    const chipsArray = Object.values(c.toObject()).map((v, key) => ({ ...v, nominal: nominal[key] }));
    return chipsArray.reduce((acc, cc) => acc.set(cc.id, new ChipModel({ ...cc })), new Map());
}

const game = (state = initialState, action) => {
    switch (action.type) {
    case gameActions.GAME__SET__READY__STATE:
        return state.merge({ readyState: action.payload });
    case gameActions.GAME__GET__QUERY:
        return state.merge({ query: action.payload });
    case gameActions.GAME__SET__QUERY__COMPUTED:
        return state.merge({ queryComputed: action.payload });
    case gameActions.GAME__SHOW__LIMITS:
        return state.merge({ showLimits: action.payload });
    case gameActions.GAME__UPDATE__ROUND__STATUS:
        return state.merge({ round: action.payload });
    case gameActions.GAME__UPDATE__SHOE__STAT:
        return state.merge({ shoeStat: action.payload });
    case gameActions.GAME__UPDATE__PLAYER__INFO:
        return state.merge({ playerInfo: action.payload });
    case gameActions.GAME__SET__BETS__STATUS:
        return state.merge({ betsStatus: action.payload });
    case gameActions.GAME__UPDATE__PLACED__BET:
        return state.merge({ placedBet: action.payload });
    case gameActions.GAME__SET__SETTINGS:
        return state.merge({ settings: action.payload });
    case gameActions.GAME__SET__BETS__TIMER:
        return state.merge({ betsTimer: action.payload / 1000 });
    case gameActions.GAME__SET__BETS__TIMER__STATUS:
        return state.merge({ showTimer: action.payload });
    case gameActions.GAME__SET__ROUND__TIMER:
        return state.merge({ roundTimer: action.payload });
    case gameActions.GAME__SET__ROUND__TIMER__STATUS:
        return state.merge({ showRoundTimer: action.payload });
    case gameActions.GAME__SET__CHIPS__NOMINAL:
        // eslint-disable-next-line no-unused-vars,no-case-declarations
        const chipsWithNominal = setChipNominal(state.chips, action.payload);
        return state.merge({ chips: chipsWithNominal, activeChip: getActiveChip(chipsWithNominal, 0) });
    case gameActions.GAME__SET__ACTIVE__CHIP:
        // eslint-disable-next-line no-case-declarations
        const updatedList = setActiveChip(state.chips, action.payload);
        return state.merge({ chips: updatedList, activeChip: getActiveChip(updatedList, action.payload) });
    case gameActions.GAME__SET__WIN:
        return state.merge({ win: action.payload });
    case gameActions.GAME__SET__INIT__ROUND:
        return state.merge({ initRoundId: action.payload });
    case gameActions.GAME__SKIP__CURRENT__ROUND:
        return state.merge({ skipCurrentRound: action.payload });
    case gameActions.GAME__SET__LOW__BALANCE__MESSAGE__STATUS:
        return state.merge({ showLowBalanceMessage: action.payload });
    case gameActions.GAME__SET__CURRENCY__DATA:
        return state.merge({ currency: action.payload });
    case gameActions.GAME__SET__ACTIVE__CHIP__PANEL__STATUS:
        return state.merge({ showActiveChipPanel: action.payload });
    default:
        return state;
    }
};

export default game;
