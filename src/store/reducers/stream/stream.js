import { Record } from 'immutable';
import * as _ from 'lodash';
import StreamConfigModel from '../../../models/stream-config-model';
import { stream as streamActions } from '../../actions';
import StreamModel from '../../../models/stream-model';
import StreamInfoModel from '../../../models/stream-info-model';
import StreamH5liveModel from '../../../models/stream-h5live-model';

const initState = Record({
    config: new StreamConfigModel(),
    playerInstance: null,
    bufferDelay: 0,
    enableSound: false,
})();

function updateConfig(config, settings) {
    const url = _.get(settings, 'url');
    const qualities = _.get(settings, 'qualities');
    let streams = [];
    if (url && qualities) {
        streams = Object.keys(qualities).map((key, idx) => {
            const entry = _.get(qualities, `[${key}]`);
            if (!entry) return new StreamModel();
            const {
                bitrate = 2500,
                width = 1920,
                height = 1080,
                framerate = 30,
                streamId: streamname,
            } = entry;
            return new StreamModel({
                index: idx,
                label: key,
                info: new StreamInfoModel({
                    bitrate,
                    width,
                    height,
                    framerate,
                }),
                h5live: new StreamH5liveModel({
                    rtmp: {
                        url,
                        streamname,
                    },
                }),
            });
        });
        return new StreamConfigModel({
            ..._.merge(config, {
                source: {
                    entries: streams,
                },
            }),
        });
    }
    return config;
}

const stream = (state = initState, action) => {
    switch (action.type) {
    case streamActions.STREAM__UPDATE__CONFIG:
        return state.merge({ config: updateConfig(state.config, action.payload) });
    case streamActions.STREAM__GET__PLAYER__INSTANCE:
        return state.merge({ playerInstance: action.payload });
    case streamActions.STREAM__GET__BUFFER__DELAY:
        return state.merge({ bufferDelay: action.payload });
    case streamActions.STREAM__ENABLE__SOUND:
        return state.merge({ enableSound: action.payload });
    default:
        return state;
    }
};

export default stream;
