import * as _ from 'lodash';
import { game as gameActions } from '../../actions';

const timerMiddleware = (store) => (next) => (action) => {
    const result = next(action);

    switch (action.type) {
    case gameActions.GAME__SET__BETS__TIMER:
        store.dispatch(gameActions.setBetsTimerStatus(true));
        _.delay(() => {
            store.dispatch(gameActions.setBetsTimerStatus(false));
        }, action.payload);
        break;
    case gameActions.GAME__UPDATE__ROUND__STATUS:
        if (action.payload.stage === 4) {
            store.dispatch(gameActions.setRoundTimerStatus(true));
        } else {
            store.dispatch(gameActions.setRoundTimerStatus(false));
            store.dispatch(gameActions.setWin(0));
        }
        break;
    default:
        break;
    }
    return result;
};

export default timerMiddleware;
