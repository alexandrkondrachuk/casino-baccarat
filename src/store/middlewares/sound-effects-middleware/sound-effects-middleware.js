import * as _ from 'lodash';
import { app as appActions, game as gameActions, chat as chatActions } from '../../actions';
// eslint-disable-next-line import/no-cycle
import { soundEffects } from '../../../services';

const soundEffectsMiddleware = (store) => (next) => (action) => {
    const result = next(action);
    const volume = _.get(store.getState(), 'app.soundEffectVolume');
    switch (action.type) {
    case appActions.APP__CHANGE__LANG:
    case appActions.APP__SET__SOUND__EFFECT__VOLUME:
    case gameActions.GAME__SET__SETTINGS:
        soundEffects.play('click', volume);
        break;
    case appActions.APP__SET__ALERT__MODEL:
        soundEffects.play('popUp', volume);
        break;
    case gameActions.GAME__SET__BETS__STATUS:
        // @todo choose better sound here, disabled for now !!!
        // soundEffects.play('arcade', 0);
        break;
    case gameActions.GAME__UPDATE__PLACED__BET:
        soundEffects.play('chipPrimary', volume);
        break;
    case gameActions.GAME__SET__ACTIVE__CHIP:
        soundEffects.play('chip', volume);
        break;
    case chatActions.CHAT__SAVE__MESSAGE:
        soundEffects.play('popUp', volume);
        break;
    default:
        break;
    }
    return result;
};

export default soundEffectsMiddleware;
