// eslint-disable-next-line import/no-cycle
import soundEffectsMiddleware from './sound-effects-middleware';
import timerMiddleware from './timer-middleware';

export {
    soundEffectsMiddleware,
    timerMiddleware,
};
