export const STREAM__UPDATE__CONFIG = 'STREAM__UPDATE__CONFIG';
export const STREAM__GET__PLAYER__INSTANCE = 'STREAM__GET__PLAYER__INSTANCE';
export const STREAM__GET__BUFFER__DELAY = 'STREAM__GET__BUFFER__DELAY';
export const STREAM__ENABLE__SOUND = 'STREAM__ENABLE__SOUND';

export function updateConfig(settings = null) {
    return {
        type: STREAM__UPDATE__CONFIG,
        payload: settings,
    };
}

export function getPlayerInstance(instance = null) {
    return {
        type: STREAM__GET__PLAYER__INSTANCE,
        payload: instance,
    };
}

export function getBufferDelay(delay = 0) {
    return {
        type: STREAM__GET__BUFFER__DELAY,
        payload: delay,
    };
}

export function enableSound(status = false) {
    return {
        type: STREAM__ENABLE__SOUND,
        payload: status,
    };
}
