export const CHAT__SAVE__MESSAGE = 'CHAT__SAVE__MESSAGE';

export function saveChatMessage(model = null) {
    return {
        type: CHAT__SAVE__MESSAGE,
        payload: model,
    };
}
