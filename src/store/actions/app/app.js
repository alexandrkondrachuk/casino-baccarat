import { LOCALES } from '../../../lang';

export const APP__CHANGE__LANG = 'APP__CHANGE__LANG';
export const APP__SET__SOUND__EFFECT__VOLUME = 'APP__SET__SOUND__EFFECT__VOLUME';
export const APP__SET__ALERT__MODEL = 'APP__SET__ALERT__MODEL';
export const APP__SET__MASK__STATUS = 'APP__SET__MASK__STATUS';
export const APP__SET__SETTING = 'APP__SET__SETTING';
export const APP__SET__MAIN__SETTING = 'APP__SET__MAIN__SETTING';
export const APP__SET__BLOCK__STATUS = 'APP__SET__BLOCK__STATUS';
export const APP__SET__MAINTENANCE__STATUS = 'APP__SET__MAINTENANCE__STATUS';
export const APP__SET__INACTION__STATUS = 'APP__SET__INACTION__STATUS';
export const APP__SET__RECONNECT__STATUS = 'APP__SET__RECONNECT__STATUS';

export function changeLang(lang = LOCALES.en) {
    return {
        type: APP__CHANGE__LANG,
        payload: lang,
    };
}

export function setSoundEffectVolume(volume = 0.75) {
    return {
        type: APP__SET__SOUND__EFFECT__VOLUME,
        payload: volume,
    };
}

export function setAlertModel(model = null) {
    return {
        type: APP__SET__ALERT__MODEL,
        payload: model,
    };
}

export function setMaskStatus(status = true) {
    return {
        type: APP__SET__MASK__STATUS,
        payload: status,
    };
}

export function setSetting(setting = null) {
    return {
        type: APP__SET__SETTING,
        payload: setting,
    };
}

export function setMainSetting(setting = null) {
    return {
        type: APP__SET__MAIN__SETTING,
        payload: setting,
    };
}

export function setBlockStatus(status = false) {
    return {
        type: APP__SET__BLOCK__STATUS,
        payload: status,
    };
}

export function setMaintenanceStatus(status = false) {
    return {
        type: APP__SET__MAINTENANCE__STATUS,
        payload: status,
    };
}

export function setInactionStatus(status = false) {
    return {
        type: APP__SET__INACTION__STATUS,
        payload: status,
    };
}

export function setReconnectStatus(status = false) {
    return {
        type: APP__SET__RECONNECT__STATUS,
        payload: status,
    };
}
