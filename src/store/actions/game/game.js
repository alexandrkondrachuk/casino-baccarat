export const GAME__SET__READY__STATE = 'GAME__SET__READY__STATE';
export const GAME__GET__QUERY = 'GAME__GET__QUERY';
export const GAME__SET__QUERY__COMPUTED = 'GAME__SET__QUERY__COMPUTED';
export const GAME__SHOW__LIMITS = 'GAME__SHOW__LIMITS';
export const GAME__UPDATE__ROUND__STATUS = 'GAME__UPDATE__ROUND__STATUS';
export const GAME__UPDATE__SHOE__STAT = 'GAME__UPDATE__SHOE__STAT';
export const GAME__UPDATE__PLAYER__INFO = 'GAME__UPDATE__PLAYER__INFO';
export const GAME__SET__BETS__STATUS = 'GAME__SET__BETS__STATUS';
export const GAME__UPDATE__PLACED__BET = 'GAME__UPDATE__PLACED__BET';
export const GAME__SET__SETTINGS = 'GAME__SET__SETTINGS';
export const GAME__SET__BETS__TIMER = 'GAME__SET__BETS__TIMER';
export const GAME__SET__BETS__TIMER__STATUS = 'GAME__SET__BETS__TIMER__STATUS';
export const GAME__SET__ROUND__TIMER = 'GAME__SET__ROUND__TIMER';
export const GAME__SET__ROUND__TIMER__STATUS = 'GAME__SET__ROUND__TIMER__STATUS';
export const GAME__SET__ACTIVE__CHIP = 'GAME__SET__ACTIVE__CHIP';
export const GAME__SET__WIN = 'GAME__SET__WIN';
export const GAME__SET__INIT__ROUND = 'GAME__SET__INIT__ROUND';
export const GAME__SKIP__CURRENT__ROUND = 'GAME__SKIP__CURRENT__ROUND';
export const GAME__SET__LOW__BALANCE__MESSAGE__STATUS = 'GAME__SET__LOW__BALANCE__MESSAGE__STATUS';
export const GAME__SET__CURRENCY__DATA = 'GAME__SET__CURRENCY__DATA';
export const GAME__SET__CHIPS__NOMINAL = 'GAME__SET__CHIPS__NOMINAL';
export const GAME__SET__ACTIVE__CHIP__PANEL__STATUS = 'GAME__SET__ACTIVE__CHIP__PANEL__STATUS';

export function setReadyState(state = false) {
    return {
        type: GAME__SET__READY__STATE,
        payload: state,
    };
}

export function getQuery(query = null) {
    return {
        type: GAME__GET__QUERY,
        payload: query,
    };
}

export function setQueryComputed(query = null) {
    return {
        type: GAME__SET__QUERY__COMPUTED,
        payload: query,
    };
}

export function showLimits(status = false) {
    return {
        type: GAME__SHOW__LIMITS,
        payload: status,
    };
}

export function updateRoundStatus(model = null) {
    return {
        type: GAME__UPDATE__ROUND__STATUS,
        payload: model,
    };
}

export function updateShoeStat(model = null) {
    return {
        type: GAME__UPDATE__SHOE__STAT,
        payload: model,
    };
}

export function updatePlayerInfo(model = null) {
    return {
        type: GAME__UPDATE__PLAYER__INFO,
        payload: model,
    };
}

export function setBetsStatus(model = null) {
    return {
        type: GAME__SET__BETS__STATUS,
        payload: model,
    };
}

export function updatePlacedBet(model = null) {
    return {
        type: GAME__UPDATE__PLACED__BET,
        payload: model,
    };
}

export function setSettings(model = null) {
    return {
        type: GAME__SET__SETTINGS,
        payload: model,
    };
}

export function setBetsTimer(length = 10 * 1000) {
    return {
        type: GAME__SET__BETS__TIMER,
        payload: length,
    };
}

export function setBetsTimerStatus(status = false) {
    return {
        type: GAME__SET__BETS__TIMER__STATUS,
        payload: status,
    };
}

export function setRoundTimer(length = 10 * 1000) {
    return {
        type: GAME__SET__ROUND__TIMER,
        payload: length,
    };
}

export function setRoundTimerStatus(status = false) {
    return {
        type: GAME__SET__ROUND__TIMER__STATUS,
        payload: status,
    };
}

export function setActiveChip(id = 0) {
    return {
        type: GAME__SET__ACTIVE__CHIP,
        payload: id,
    };
}

export function setWin(value = 0) {
    return {
        type: GAME__SET__WIN,
        payload: value,
    };
}

export function setInitRound(id = null) {
    return {
        type: GAME__SET__INIT__ROUND,
        payload: id,
    };
}

export function skipCurrentRound(status = false) {
    return {
        type: GAME__SKIP__CURRENT__ROUND,
        payload: status,
    };
}

export function setLowBalanceMessageStatus(status = true) {
    return {
        type: GAME__SET__LOW__BALANCE__MESSAGE__STATUS,
        payload: status,
    };
}

export function setCurrencyData(model = null) {
    return {
        type: GAME__SET__CURRENCY__DATA,
        payload: model,
    };
}

export function setChipsNominal(nominal = []) {
    return {
        type: GAME__SET__CHIPS__NOMINAL,
        payload: nominal,
    };
}
export function setActiveChipPanelStatus(status = false) {
    return {
        type: GAME__SET__ACTIVE__CHIP__PANEL__STATUS,
        payload: status,
    };
}
