import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import DevicePlaceholder from '../../../components/device-placeholder';
import config from '../../../config';

import './tablet.scss';

const isSupportTablet = config.get('isSupportTablet');

function Tablet({
    readyState, blockStatus, inactionStatus, maintenanceStatus,
}) {
    const renderRedirect = (path = '/') => (<Redirect to={path} />);
    return (
        <div className="Tablet">
            {blockStatus && renderRedirect(config.get('apiURLs.issue'))}
            {inactionStatus && renderRedirect(config.get('apiURLs.inaction'))}
            {maintenanceStatus && renderRedirect(config.get('apiURLs.maintenance'))}
            { !readyState && renderRedirect() }
            {isSupportTablet && <h1>Tablet View</h1> }
            { !isSupportTablet && <DevicePlaceholder /> }
        </div>
    );
}

Tablet.propTypes = {
    readyState: PropTypes.bool.isRequired,
    // dispatch: PropTypes.func.isRequired,
    blockStatus: PropTypes.bool.isRequired,
    inactionStatus: PropTypes.bool.isRequired,
    maintenanceStatus: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const readyState = _.get(state, 'game.readyState');
    const blockStatus = _.get(state, 'app.blockStatus');
    const inactionStatus = _.get(state, 'app.inactionStatus');
    const maintenanceStatus = _.get(state, 'app.maintenanceStatus');
    return {
        readyState,
        blockStatus,
        inactionStatus,
        maintenanceStatus,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(Tablet);
