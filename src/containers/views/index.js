import Desktop from './desktop';
import Mobile from './mobile';
import Tablet from './tablet';
import Issue from './issue';
import Inaction from './inaction';
import Maintenance from './maintenance';

export {
    Desktop,
    Mobile,
    Tablet,
    Issue,
    Inaction,
    Maintenance,
};
