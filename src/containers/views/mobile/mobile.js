import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { withOrientationChange } from 'react-device-detect';
import useOrientationChange from 'use-orientation-change';
import { MobileFullscreen } from 'react-mobile-fullscreen';
import { Landscape, Portrait, Mask } from './sub-views';
import Types from '../../../classes/Types';
import config from '../../../config';
import DevicePlaceholder from '../../../components/device-placeholder';
import { ClickIcon } from './sub-views/mask/icons';
import { app as appActions } from '../../../store/actions';

import './mobile.scss';

const isSupportMobile = config.get('isSupportMobile');

function Mobile({
    readyState,
    player,
    dispatch,
    blockStatus,
    inactionStatus,
    maintenanceStatus,
}) {
    const [showAppMask, setAppMaskStatus] = useState(true);
    const orientation = useOrientationChange();
    const renderRedirect = (path = '/') => (<Redirect to={path} />);
    const ua = window.navigator.userAgent;
    const isStartFromApp = /Parimatch/.test(ua);
    const content = (
        <>
            {blockStatus && renderRedirect(config.get('apiURLs.issue'))}
            {inactionStatus && renderRedirect(config.get('apiURLs.inaction'))}
            {maintenanceStatus && renderRedirect(config.get('apiURLs.maintenance'))}
            { !readyState && renderRedirect() }
            { (isSupportMobile && (orientation === Types.ORIENTATION__LANDSCAPE)) && (<Landscape />) }
            { (isSupportMobile && (orientation === Types.ORIENTATION__PORTRAIT)) && (<Portrait />) }
            { !isSupportMobile && <DevicePlaceholder /> }
        </>
    );
    const enableSound = () => {
        dispatch(appActions.setSoundEffectVolume(1));
        if (player) {
            player.unmute();
            player.setVolume(1);
        }
    };
    const appMask = (
        // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
        <div
            className="app-mask"
            onClick={() => {
                _.delay(() => { enableSound(); }, config.get('playerInitDelay'));
                setAppMaskStatus(false);
            }}
        >
            <div className="app-mask-icon">
                <ClickIcon />
            </div>
            <div className="app-mask-title">
                Click to play
            </div>
        </div>
    );
    return (
        <>
            {!isStartFromApp ? (
                <MobileFullscreen mask={Mask}>
                    <div id="Mobile" className="Mobile">
                        {content}
                    </div>
                </MobileFullscreen>
            ) : (
                <div id="Mobile" className="Mobile">
                    {content}
                    {showAppMask && appMask}
                </div>
            )}
        </>
    );
}

Mobile.defaultProps = {
    player: null,
};

Mobile.propTypes = {
    readyState: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
    dispatch: PropTypes.func.isRequired,
    blockStatus: PropTypes.bool.isRequired,
    inactionStatus: PropTypes.bool.isRequired,
    maintenanceStatus: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const readyState = _.get(state, 'game.readyState');
    const player = _.get(state, 'stream.playerInstance');
    const blockStatus = _.get(state, 'app.blockStatus');
    const inactionStatus = _.get(state, 'app.inactionStatus');
    const maintenanceStatus = _.get(state, 'app.maintenanceStatus');
    return {
        readyState,
        player,
        blockStatus,
        inactionStatus,
        maintenanceStatus,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

const MobileWrapper = withOrientationChange(Mobile);

export default connect(mapStateToProps, dispatchStateToProps)(MobileWrapper);
