import React from 'react';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { isIOS } from 'react-device-detect';
import {
    // eslint-disable-next-line import/named
    Header, VideoPanel, GamePanel, Footer, ChipsPanel, IOSSoundModal,
} from './components';
import BaccaratRoundModel from '../../../../../models/baccarat-round-model';
import PlayerInfoModel from '../../../../../models/player-info-model';
import Types from '../../../../../classes/Types';
import MobileMenu from '../menu';

import './portrait.scss';

function Portrait({
    round,
    playerInfo,
    skipCurrentRound,
    maskStatus,
    player,
}) {
    const stages = Types.API__ROUND__STAGE;
    const status = _.get(round, 'stageName', '');
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    const isPanelDown = !!((status === stages[3]) || (status === stages[4]) || skipCurrentRound || isLowBalance);
    const ua = window.navigator.userAgent;
    const isStartFromApp = /Parimatch/.test(ua);

    const enableSound = () => {
        if (player) {
            player.unmute();
            player.setVolume(1);
        }
    };
    const enableGameSounds = () => {
        enableSound();
    };

    return (
        <div id="Portrait" className={cn({ Portrait: true, web: !isStartFromApp, app: isStartFromApp })}>
            <MobileMenu pageWrapId="Portrait" outerContainerId="Mobile" />
            <Header />
            <VideoPanel />
            <div className="Portrait__Game__Wrapper">
                <GamePanel />
                {!isPanelDown && <ChipsPanel />}
            </div>
            <Footer />
            {(isIOS && !isStartFromApp && !maskStatus) && <IOSSoundModal enableGameSounds={enableGameSounds} />}
        </div>
    );
}

const mapStateToProps = (state) => {
    const round = _.get(state, 'game.round');
    const playerInfo = _.get(state, 'game.playerInfo');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    const maskStatus = _.get(state, 'app.maskStatus');
    const player = _.get(state, 'stream.playerInstance');
    return {
        round,
        playerInfo,
        skipCurrentRound,
        maskStatus,
        player,
    };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

Portrait.defaultProps = {
    player: null,
};

Portrait.propTypes = {
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
    maskStatus: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
    // dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(Portrait);
