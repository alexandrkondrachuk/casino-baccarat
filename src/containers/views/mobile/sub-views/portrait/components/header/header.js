import React from 'react';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import getSymbolFromCurrency from 'currency-symbol-map';
import T from 'react-compound-timer';
import PlayerInfoModel from '../../../../../../../models/player-info-model';
import config from '../../../../../../../config';
import { number } from '../../../../../../../lang';
import Types from '../../../../../../../classes/Types';
import BaccaratRoundModel from '../../../../../../../models/baccarat-round-model';

import './header.scss';

function Header({
    playerInfo, round, skipCurrentRound, betsTimer, showTimer,
}) {
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const balance = _.get(playerInfo, 'balance', 0);
    const totalBet = _.get(playerInfo, 'totalBetOfCurrentRound', 0);
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    const currencySymbol = getSymbolFromCurrency(currency);

    // Round Description Information
    const stages = Types.API__ROUND__STAGE;
    const stagesDescription = Types.API__STAGE__DESCRIPTION;
    const status = _.get(round, 'stageName', '');
    // eslint-disable-next-line no-nested-ternary
    const description = skipCurrentRound
        ? _.get(stagesDescription, 'WaitAnotherRound', '')
        : isLowBalance
            ? _.get(stagesDescription, 'LowBalance', '')
            : _.get(stagesDescription, `[${status}]`, '');
    const roundResult = _.get(round, 'roundResult');
    const seconds = betsTimer * 1000;
    const isEnableTimer = (showTimer && !skipCurrentRound && !isLowBalance && (status !== stages[1]));

    return (
        <div className="Header__Portrait">
            <div className="Header__Portrait__Balance__Bet">
                <div className="Balance">
                    <div className="title">Balance</div>
                    <div className="value">
                        {currencySymbol}
                        {number({ value: balance })}
                    </div>
                </div>
                <div className="Bet">
                    <div className="title">Total Bet</div>
                    <div className="value">
                        {currencySymbol}
                        {number({ value: totalBet })}
                    </div>
                </div>
            </div>
            <div className="Header__Portrait__RoundStatus">
                <div className={cn({ RoundStatus: true, [status]: !!status, skipCurrentRound: !!skipCurrentRound })}>
                    <div className="RoundStatus__Title">
                        {description.replace(/{name}/g, roundResult)}
                        &nbsp;
                        {isEnableTimer && (
                            <div className="RoundStatus__Bet__Timer">
                                <T initialTime={seconds} direction="backward">
                                    <T.Seconds />
                                </T>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    const playerInfo = _.get(state, 'game.playerInfo');
    const currencyModel = _.get(state, 'game.currency');
    const round = _.get(state, 'game.round');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    const betsTimer = _.get(state, 'game.betsTimer');
    const showTimer = _.get(state, 'game.showTimer');

    return {
        playerInfo,
        currencyModel,
        round,
        skipCurrentRound,
        betsTimer,
        showTimer,
    };
};
const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

Header.propTypes = {
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
    betsTimer: PropTypes.number.isRequired,
    showTimer: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(Header);
