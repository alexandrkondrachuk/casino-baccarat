import Header from './header';
import VideoPanel from './video-panel';
import GamePanel from './game-panel';
import Footer from './footer';
import { ChipsPanel } from './game-panel/components';
import IOSSoundModal from './ios-sound-modal';

export {
    Header,
    VideoPanel,
    GamePanel,
    Footer,
    ChipsPanel,
    IOSSoundModal,
};
