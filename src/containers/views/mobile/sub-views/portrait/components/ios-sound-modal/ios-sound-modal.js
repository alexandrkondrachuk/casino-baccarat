import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import {
    Button, Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

import './ios-sound-modal.scss';

const IOSSoundModal = ({ enableGameSounds }) => {
    const [modal, setModal] = useState(true);

    const toggle = () => {
        enableGameSounds();
        setModal(!modal);
    };

    const disagreeHandler = () => setModal(!modal);

    return (
        <div>
            <Modal isOpen={modal} toggle={toggle} contentClassName="IOSSoundModal">
                <ModalHeader toggle={toggle} className="IOSSoundModal__Header">Would you like to enable game sound?</ModalHeader>
                <ModalBody className="IOSSoundModal__Body">
                    Let your game to be more interesting and excited.
                </ModalBody>
                <ModalFooter className="IOSSoundModal__Footer">
                    <Button className="IOSSoundModal__Button disagree" color="secondary" onClick={disagreeHandler}>Disagree</Button>
                    {' '}
                    <Button className="IOSSoundModal__Button agree" color="primary" onClick={toggle}>Agree</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
};

IOSSoundModal.propTypes = {
    enableGameSounds: PropTypes.func.isRequired,
};

export default IOSSoundModal;
