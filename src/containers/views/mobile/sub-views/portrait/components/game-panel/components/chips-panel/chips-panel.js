import React, { useRef, useState } from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import * as cn from 'classnames';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';
import ClickNHold from 'react-click-n-hold';
import { Icon } from '../../../../../../../../../components/svg-components';
import Chip from '../../../../../../../../../components/chip';
import { game as gameActions } from '../../../../../../../../../store/actions';
import { useOutsideClick } from '../../../../../../../../../hooks';
import { api } from '../../../../../../../../../services';
import config from '../../../../../../../../../config';

import './chips-panel.scss';

function ChipsPanel({ chips, showActiveChipPanel, dispatch }) {
    const [undoInterval, setUndoInterval] = useState(null);
    const chipsArray = Object.values(chips.toObject()).map((v) => v);
    const activeChip = chipsArray.find((c) => !!(c.active));
    const notActiveChips = chipsArray.filter((c) => !(c.active));
    const panelRef = useRef(null);

    const toggleChipPanel = () => {
        dispatch(gameActions.setActiveChipPanelStatus(!showActiveChipPanel));
    };

    const setActiveChip = (id) => {
        dispatch(gameActions.setActiveChip(id));
        _.delay(() => {
            dispatch(gameActions.setActiveChipPanelStatus(false));
        }, 100);
    };

    /**
     * @param type:string - LastBet | AllBets
     * @returns void
     */
    const doUndoBet = (type = 'LastBet') => {
        api
            .doUndoBet(type)
            .then((res) => {
                console.info('doUndo: ', res);
            })
            .catch((err) => {
                console.error('doUndo: ', err);
            });
    };

    /**
     *  Hold effect
     * */

    const start = () => {
        console.log('START');
    };

    const end = () => {
        if (undoInterval) clearInterval(undoInterval);
    };

    const clickNHold = () => {
        const uInterval = setInterval(() => {
            doUndoBet();
        }, config.get('undoDelay', 1000));
        setUndoInterval(uInterval);
    };

    useOutsideClick(panelRef, toggleChipPanel);

    return (
        <div className="ChipsPanel__Portrait">
            <div className="action undo">
                <div className="action-text">Undo</div>
                <ClickNHold
                    time={1}
                    onStart={(e) => start(e)}
                    onClickNHold={(e) => clickNHold(e)}
                    onEnd={(e, enough) => end(e, enough)}
                >
                    <Button className="action-button" color="primary" variant="outlined">
                        <Icon path="undo" onClick={() => doUndoBet()} />
                    </Button>
                </ClickNHold>
            </div>
            {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
            <div className="action choose" onClick={toggleChipPanel}>
                {!showActiveChipPanel && (
                    <>
                        <Chip nominal={_.get(activeChip, 'nominal')} color={_.get(activeChip, 'color')} className="action-chip active" />
                        { notActiveChips && notActiveChips.map((chip, key) => (
                            <Chip key={_.get(chip, 'id')} nominal={_.get(chip, 'nominal')} color={_.get(chip, 'color')} className={cn({ 'action-chip': true, [`index-${key}`]: true })} />
                        )) }
                    </>
                )}
            </div>
            <div className="action repeat">
                <Button className="action-button" color="primary" variant="outlined">
                    <Icon path="repeat" />
                </Button>
                <div className="action-text">Repeat</div>
            </div>
            {showActiveChipPanel && (
                <div className="activation-chip-panel" ref={panelRef}>
                    <Chip nominal={_.get(activeChip, 'nominal')} color={_.get(activeChip, 'color')} className="action-chip active" onClick={toggleChipPanel} />
                    { chipsArray && chipsArray.map((chip, key) => (
                        <Chip key={_.get(chip, 'id')} nominal={_.get(chip, 'nominal')} color={_.get(chip, 'color')} className={cn({ 'action-chip': true, [`circle-index-${key}`]: true })} onClick={() => setActiveChip(_.get(chip, 'id', key))} />
                    )) }
                </div>
            )}
        </div>
    );
}

const mapStateToProps = (state) => {
    const chips = _.get(state, 'game.chips');
    const showActiveChipPanel = _.get(state, 'game.showActiveChipPanel');

    return {
        chips,
        showActiveChipPanel,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

ChipsPanel.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    chips: PropTypes.object.isRequired,
    showActiveChipPanel: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(ChipsPanel);
