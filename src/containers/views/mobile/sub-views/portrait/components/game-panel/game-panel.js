import React from 'react';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import Types from '../../../../../../../classes/Types';
import { BetZonesPanel, StatisticPanel } from './components';
import BaccaratRoundModel from '../../../../../../../models/baccarat-round-model';
import PlayerInfoModel from '../../../../../../../models/player-info-model';

import './game-panel.scss';

function GamePanel({
    round,
    playerInfo,
    skipCurrentRound,
}) {
    const stages = Types.API__ROUND__STAGE;
    const status = _.get(round, 'stageName', '');
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    const isPanelDown = !!((status === stages[3]) || (status === stages[4]) || skipCurrentRound || isLowBalance);

    return (
        <div className={cn({ GamePanel__Portrait: true, down: isPanelDown })}>
            <BetZonesPanel />
            <StatisticPanel />
        </div>
    );
}

const mapStateToProps = (state) => {
    const round = _.get(state, 'game.round');
    const playerInfo = _.get(state, 'game.playerInfo');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    return {
        round,
        playerInfo,
        skipCurrentRound,
    };
};

const dispatchStateToProps = (dispatch) => ({ dispatch });

GamePanel.propTypes = {
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(GamePanel);
