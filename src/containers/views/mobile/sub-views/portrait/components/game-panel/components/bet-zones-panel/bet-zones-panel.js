import React from 'react';
import * as cn from 'classnames';
import * as _ from 'lodash';
import * as PropTypes from 'prop-types';
import getSymbolFromCurrency from 'currency-symbol-map';
import { connect } from 'react-redux';
import PersonIcon from '@material-ui/icons/Person';
import { useWindowSize } from '@react-hook/window-size';
import { PlayerSvg, BankerSvg, TieSvg } from '../../../../../../../../../components/svg-components/bet-panel';
import BetStatistic from '../../../../../../../../../components/svg-components/bet-statistic';
import Icon from '../../../../../../../../../components/svg-components/icons/icon';
import BaccaratRoundModel from '../../../../../../../../../models/baccarat-round-model';
import PlayerInfoModel from '../../../../../../../../../models/player-info-model';
import config from '../../../../../../../../../config';
import { number } from '../../../../../../../../../lang';
import { Card } from '../../../../../../../../../components/svg-components';
import Types from '../../../../../../../../../classes/Types';
import BaccaratBetModel from '../../../../../../../../../models/baccarat-bet-model';
import { api } from '../../../../../../../../../services';
import ChipModel from '../../../../../../../../../models/chip-model';
import ChipMobile from '../../../../../../../../../components/chip-mobile';

import './bet-zones-panel.scss';

function BetZonesPanel({
    round, playerInfo, skipCurrentRound, activeChip, chips,
}) {
    const stages = Types.API__ROUND__STAGE;
    const status = _.get(round, 'stageName', '');
    const betsStat = _.get(round, 'betsStat');
    const roundResult = _.get(round, 'roundResult');
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const currencySymbol = getSymbolFromCurrency(currency);
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    const chipsArray = Object.values(chips.toObject()).map((v) => v);
    const isPanelDown = !!((status === stages[3]) || (status === stages[4]) || skipCurrentRound || isLowBalance);

    /*
     * @type:string - player | banker
     */

    const renderCards = (type = 'player') => {
        const isDealCards = ((status === stages[3]) || (status === stages[4]));
        const cards = _.get(round, `${type}Cards`);
        const points = _.get(round, `${type}Score`, 0);
        const showPoints = (Array.isArray(cards) && (cards.length > 0) && isDealCards);
        const score = showPoints ? points : '';
        const primaryCard = _.get(cards, '[0]', null);
        const primaryComponent = (primaryCard && isDealCards) ? (<Card type={primaryCard} />) : ' ';
        const secondaryCard = _.get(cards, '[1]', null);
        const secondaryComponent = (secondaryCard && isDealCards) ? (<Card type={secondaryCard} />) : ' ';
        const additionalCard = _.get(cards, '[2]', null);
        const additionalComponent = (additionalCard && isDealCards) ? (<Card type={additionalCard} />) : ' ';

        return (
            <>
                <div className="Cards__Points">{score}</div>
                <div className="Cards__Wrapper">
                    {(type === 'player') ? (
                        <>
                            <div className="Card Card__Additional">{additionalComponent}</div>
                            <div className="Card Card__Primary">{primaryComponent}</div>
                            <div className="Card Card__Secondary">{secondaryComponent}</div>
                        </>
                    ) : (
                        <>
                            <div className="Card Card__Primary">{primaryComponent}</div>
                            <div className="Card Card__Secondary">{secondaryComponent}</div>
                            <div className="Card Card__Additional">{additionalComponent}</div>
                        </>
                    )}
                </div>
            </>
        );
    };

    /*
    *  @type:number - 1 (player) | 2 (banker) | 3 (tie) | 4 (player pair) | 5 (banker pair)
    */
    const doBet = (type = 1) => {
        if (isPanelDown) return;

        const model = new BaccaratBetModel({
            BetValue: _.get(activeChip, 'nominal', 0),
            BetType: type,
        });

        api.doBet(model);
    };

    /*
    *  @type:number - 1 (player) | 2 (banker) | 3 (tie) | 4 (player pair) | 5 (banker pair)
    */
    const renderBet = (type = 1) => {
        const totalBets = _.get(playerInfo, 'totalBets', []);
        const currentBet = totalBets.find((bet) => _.get(bet, 'betType', 6) === type);
        const nominal = _.get(currentBet, 'betValue', 0);
        let color = 'blue';

        chipsArray.forEach((c) => {
            if (nominal >= c.nominal) { color = c.color; }
        });

        if (nominal > 0) {
            return (
                <div className={`bet-chip-${type}`}>
                    <ChipMobile nominal={nominal} color={color} />
                </div>
            );
        }

        return null;
    };

    // Get Panel Height
    const getPanelHeight = () => {
        const ua = window.navigator.userAgent;
        const isStartFromApp = /Parimatch/.test(ua);
        const isIOS = /iOS/.test(ua);
        const rate = (isStartFromApp && isIOS) ? '--mobile-portrait-bet-rate-sm' : '--mobile-portrait-bet-rate';
        const panelHeightRate = getComputedStyle(document.documentElement)
            .getPropertyValue(rate);
        const [width, height] = useWindowSize();
        const w = Math.min(width, height);
        const panelHeight = panelHeightRate * w;
        document.documentElement.style
            .setProperty('--mobile-portrait-bet-width', `${w}px`);
        document.documentElement.style
            .setProperty('--mobile-portrait-bet-height', `${panelHeight}px`);
    };

    getPanelHeight();

    return (
        <div className="BetZonesPanel__Portrait">
            <div className="Zone Zone__Main">
                <div className="Main__Col Player">
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div className="Bet Bet__Player" onClick={() => { doBet(1); }}>
                        <PlayerSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Player')) })} />
                        <div className="Bet__Player__Content">
                            <div className="Content__Statistic">
                                <div className="Statistic__Diagram">
                                    <BetStatistic percent={_.get(betsStat, 'playerPercent', 0)} type="player" />
                                </div>
                                <div className="Statistic__Details">
                                    <div className="Details__Col">
                                        <span className="Details__Symbol">{currencySymbol}</span>
                                        <span className="Details_Value">{_.get(betsStat, 'playerBetsSum', 0)}</span>
                                    </div>
                                    <div className="Details__Col">
                                        <span className="Details__Symbol"><PersonIcon /></span>
                                        <span className="Details_Value">{_.get(betsStat, 'playerBetsCount', 0)}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="Content__Cards">
                                {renderCards('player')}
                            </div>
                            <div className="Content__Info">
                                <div className="Info__Icon">
                                    <Icon path="player" />
                                </div>
                                <div className="Info__Title">
                                    Player
                                </div>
                                { renderBet() }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Main__Col Tie">
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div className="Bet Bet__Tie" onClick={() => { doBet(3); }}>
                        <TieSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Tie')) })} />
                        <div className="Bet__Tie__Content">
                            <div className="Content__Statistic">
                                <div className="Statistic__Diagram">
                                    <BetStatistic percent={_.get(betsStat, 'tiePercent', 0)} type="tie" />
                                </div>
                                <div className="Statistic__Details">
                                    <div className="Details__Col">
                                        <span className="Details__Symbol">{currencySymbol}</span>
                                        <span className="Details_Value">{number({ value: _.get(betsStat, 'tieBetsSum', 0), maximumFractionDigits: 2 })}</span>
                                    </div>
                                    <div className="Details__Col">
                                        <span className="Details__Symbol"><PersonIcon /></span>
                                        <span className="Details_Value">{_.get(betsStat, 'tieBetsCount', 0)}</span>
                                    </div>
                                </div>
                            </div>
                            <div className="Content__Info">
                                <div className="Info__Title">
                                    Tie 8:1
                                </div>
                                {renderBet(3)}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Main__Col Banker">
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div className="Bet Bet__Banker" onClick={() => { doBet(2); }}>
                        <BankerSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Banker')) })} />
                        <div className="Bet__Banker__Content">
                            <div className="Content__Statistic">
                                <div className="Statistic__Details">
                                    <div className="Details__Col">
                                        <span className="Details__Symbol">{currencySymbol}</span>
                                        <span className="Details_Value">{number({ value: _.get(betsStat, 'bankerBetsSum', 0), maximumFractionDigits: 2 })}</span>
                                    </div>
                                    <div className="Details__Col">
                                        <span className="Details__Symbol"><PersonIcon /></span>
                                        <span className="Details_Value">{_.get(betsStat, 'bankerBetsCount', 0)}</span>
                                    </div>
                                </div>
                                <div className="Statistic__Diagram">
                                    <BetStatistic percent={_.get(betsStat, 'bankerPercent', 0)} type="banker" />
                                </div>
                            </div>
                            <div className="Content__Cards">
                                {renderCards('banker')}
                            </div>
                            <div className="Content__Info">
                                <div className="Info__Ico">
                                    <Icon path="banker" />
                                </div>
                                <div className="Info__Title">
                                    Banker
                                </div>
                                {renderBet(2)}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="Zone Zone_Pair">
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div className="Pair__Col" onClick={() => { doBet(4); }}>
                    <div className="Bet Bet__PPair">
                        <div className="Bet__PPair__Title">
                            P Pair
                        </div>
                        <div className="Bet__PPair__Factor">
                            11:1
                        </div>
                        {renderBet(4)}
                    </div>
                </div>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div className="Pair__Col fix-end" onClick={() => { doBet(5); }}>
                    <div className="Bet Bet__BPair">
                        <div className="Bet__BPair__Title">
                            B Pair
                        </div>
                        <div className="Bet__BPair__Factor">
                            11:1
                        </div>
                        {renderBet(5)}
                    </div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    const round = _.get(state, 'game.round');
    const playerInfo = _.get(state, 'game.playerInfo');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    const activeChip = _.get(state, 'game.activeChip');
    const chips = _.get(state, 'game.chips');

    return {
        round,
        playerInfo,
        skipCurrentRound,
        activeChip,
        chips,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

BetZonesPanel.propTypes = {
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
    activeChip: PropTypes.instanceOf(ChipModel).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    chips: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(BetZonesPanel);
