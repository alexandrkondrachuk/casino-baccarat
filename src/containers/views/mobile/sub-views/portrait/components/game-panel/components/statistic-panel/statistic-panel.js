import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import BeadRoadScale
    from '../../../../../../../../../components/svg-components/history/bead-road-scale/bead-road-scale';
import Tooltip from '../../../../../../../../../components/tooltip';
import {
    BT, BTB, PPT, PT, TT,
} from '../../../../../../../../../components/svg-components/history/total';
import ShoeStatModel from '../../../../../../../../../models/shoe-stat-model';

import './statistic-panel.scss';

function StatisticPanel({ shoeStat }) {
    return (
        <div className="StatisticPanel__Portrait">
            <div className="StatisticPanel__Portrait__Main">
                <div className="Main">
                    <div className="Main__Col" id="rounds-count">
                        <span className="Main__Col__Icon">#</span>
                        <span className="Main__Col__Value">{_.get(shoeStat, 'roundsCount', 0)}</span>
                        <Tooltip targetId="rounds-count" placement="top">Total number of rounds</Tooltip>
                    </div>
                    <div className="Main__Col" id="player-wins">
                        <span className="Main__Col__Icon"><PT /></span>
                        <span className="Main__Col__Value">{_.get(shoeStat, 'playerWins', 0)}</span>
                        <Tooltip targetId="player-wins" placement="top">Player</Tooltip>
                    </div>
                    <div className="Main__Col" id="banker-wins">
                        <span className="Main__Col__Icon"><BT /></span>
                        <span className="Main__Col__Value">{_.get(shoeStat, 'bankerWins', 0)}</span>
                        <Tooltip targetId="banker-wins" placement="top">Banker</Tooltip>
                    </div>
                    <div className="Main__Col" id="tie-wins">
                        <span className="Main__Col__Icon"><TT /></span>
                        <span className="Main__Col__Value">{_.get(shoeStat, 'tieWins', 0)}</span>
                        <Tooltip targetId="tie-wins" placement="top">Tie</Tooltip>
                    </div>
                    <div className="Main__Col" id="player-pair-wins">
                        <span className="Main__Col__Icon"><PPT /></span>
                        <span className="Main__Col__Value">{_.get(shoeStat, 'playerPairWins', 0)}</span>
                        <Tooltip targetId="player-pair-wins" placement="top">Player pair</Tooltip>
                    </div>
                    <div className="Main__Col" id="banker-pair-wins">
                        <span className="Main__Col__Icon"><BTB /></span>
                        <span className="Main__Col__Value">{_.get(shoeStat, 'bankerPairWins', 0)}</span>
                        <Tooltip targetId="banker-pair-wins" placement="top">Banker pair</Tooltip>
                    </div>
                </div>
            </div>
            <BeadRoadScale />
        </div>
    );
}

const mapStateToProps = (state) => {
    const shoeStat = _.get(state, 'game.shoeStat');
    return {
        shoeStat,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

StatisticPanel.propTypes = {
    shoeStat: PropTypes.instanceOf(ShoeStatModel).isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(StatisticPanel);
