import BetZonesPanel from './bet-zones-panel';
import StatisticPanel from './statistic-panel';
import ChipsPanel from './chips-panel';

export {
    BetZonesPanel, StatisticPanel, ChipsPanel,
};
