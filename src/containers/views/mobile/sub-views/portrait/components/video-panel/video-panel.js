import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import WebrtcPlayer from '../../../../../../webrtc-player/webrtc-player';
import { WinResult } from './components';
import BaccaratRoundModel from '../../../../../../../models/baccarat-round-model';

import './video-panel.scss';

function VideoPanel({
    showRoundTimer,
    round,
    win,
    currency,
}) {
    const roundResult = _.get(round, 'roundResult');
    return (
        <div className="VideoPanel__Portrait">
            <WebrtcPlayer />
            {(showRoundTimer && roundResult && (win > 0)) && <WinResult showRoundTimer={showRoundTimer} winAmount={win} currency={currency} />}
        </div>
    );
}

const mapStateToProps = (state) => {
    const showRoundTimer = _.get(state, 'game.showRoundTimer');
    const round = _.get(state, 'game.round');
    const win = _.get(state, 'game.win');
    const currency = _.get(state, 'game.playerInfo.currency', 'USD');
    return {
        showRoundTimer,
        round,
        win,
        currency,
    };
};

const dispatchStateToProps = (dispatch) => ({ dispatch });

VideoPanel.propTypes = {
    showRoundTimer: PropTypes.bool.isRequired,
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    win: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(VideoPanel);
