import React from 'react';
import * as PropTypes from 'prop-types';
import getSymbolFromCurrency from 'currency-symbol-map';
import { Animated } from 'react-animated-css';
import { number } from '../../../../../../../../../lang';

import './win-result.scss';

export default function WinResult({
    winAmount,
    showRoundTimer,
    currency,
}) {
    const currencySymbol = getSymbolFromCurrency(currency);
    return (
        <div className="WinResult">
            <Animated className="Animated" animationIn="fadeIn" animationOut="fadeOut" animationInDuration={1000} animationOutDuration={1000} isVisible={showRoundTimer}>
                <h4 className="WinResult__Message">You win</h4>
                <h5 className="WinResult__Amount">
                    {currencySymbol}
                    {number({ value: winAmount })}
                </h5>
            </Animated>
        </div>
    );
}

WinResult.defaultProps = {
    winAmount: 0,
};

WinResult.propTypes = {
    winAmount: PropTypes.number,
    showRoundTimer: PropTypes.bool.isRequired,
    currency: PropTypes.string.isRequired,
};
