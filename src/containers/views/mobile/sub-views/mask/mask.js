import React from 'react';
import { connect } from 'react-redux';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import * as _ from 'lodash';
import { ScrollIcon, ClickIcon } from './icons';
import { app as appActions } from '../../../../../store/actions';

import './mask.scss';

function Mask({ fullscreenType, player, dispatch }) {
    const nativeMask = (
        <div className="native-mask">
            <div className="native-mask-icon">
                <ClickIcon />
            </div>
            <div className="native-mask-title">
                Click to play
            </div>
        </div>
    );
    const minimalUIMask = (
        <div className="minimal-ui-mask">
            <div className="minimal-ui-mask-icon">
                <ScrollIcon />
            </div>
            <div className="minimal-ui-mask-title">
                Swipe Up to play
            </div>
        </div>
    );
    const disableMask = () => dispatch(appActions.setMaskStatus(false));
    const enableEffects = () => dispatch(appActions.setSoundEffectVolume(1));
    const enableSound = () => {
        if (player) {
            player.unmute();
            player.setVolume(1);
        }
    };
    const enableGameSounds = () => {
        enableEffects();
        enableSound();
    };
    React.useEffect(() => {
        document.addEventListener('touchend', disableMask);
        return () => document.removeEventListener('touchend', disableMask);
    });
    return (
        // eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions
        <div className={cn({ Mask: true, [fullscreenType]: true })} onClick={enableGameSounds}>
            {fullscreenType === 'native' && nativeMask}
            {fullscreenType === 'minimal-ui' && minimalUIMask}
            {(fullscreenType !== 'native' && fullscreenType !== 'minimal-ui') && nativeMask}
        </div>
    );
}

Mask.defaultProps = {
    fullscreenType: 'native',
    player: null,
};

Mask.propTypes = {
    fullscreenType: PropTypes.oneOf(['native', 'minimal-ui', null]),
    dispatch: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
};

const mapStateToProps = (state) => {
    const player = _.get(state, 'stream.playerInstance');
    return {
        player,
    };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, dispatchStateToProps)(Mask);
