import React from 'react';
import * as _ from 'lodash';
import * as PropTypes from 'prop-types';
import * as cn from 'classnames';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';
import ClickNHold from 'react-click-n-hold';
import { Icon } from '../../../../../../../../../components/svg-components';
import Chip from '../../../../../../../../../components/chip';
import { useOutsideClick } from '../../../../../../../../../hooks';
import { game as gameActions } from '../../../../../../../../../store/actions';
import { api } from '../../../../../../../../../services';
import config from '../../../../../../../../../config';

import './chip-panel.scss';

function ChipPanel({ chips, dispatch, isPanelDown }) {
    const chipsRef = React.useRef(null);
    const [undoInterval, setUndoInterval] = React.useState(null);
    const [showChips, setChipsStatus] = React.useState(false);
    const chipsArray = Object.values(chips.toObject()).map((v) => v);
    const toggleChipsPanel = () => setChipsStatus(!showChips);
    const setActiveChip = (id) => {
        dispatch(gameActions.setActiveChip(id));
        _.delay(() => {
            toggleChipsPanel();
        }, 100);
    };

    useOutsideClick(chipsRef, () => { toggleChipsPanel(); });

    /**
     * @param type:string - LastBet | AllBets
     * @returns void
     */
    const doUndoBet = (type = 'LastBet') => {
        api
            .doUndoBet(type)
            .then((res) => {
                console.info('doUndo: ', res);
            })
            .catch((err) => {
                console.error('doUndo: ', err);
            });
    };

    /**
     *  Hold effect
     * */

    const start = () => {
        console.log('START');
    };

    const end = () => {
        if (undoInterval) clearInterval(undoInterval);
    };

    const clickNHold = () => {
        const uInterval = setInterval(() => {
            doUndoBet();
        }, config.get('undoDelay', 1000));
        setUndoInterval(uInterval);
    };

    return (
        <div className={cn('ChipPanel__Landscape', { down: isPanelDown })}>
            <div className="Section undo">
                <h4 className="title">Undo</h4>
                <ClickNHold
                    time={1}
                    onStart={(e) => start(e)}
                    onClickNHold={(e) => clickNHold(e)}
                    onEnd={(e, enough) => end(e, enough)}
                >
                    <Button className="action-button" color="primary" variant="outlined">
                        <Icon path="undo" onClick={() => { doUndoBet(); }} />
                    </Button>
                </ClickNHold>
            </div>
            <div className="Section chips">
                { chipsArray && chipsArray.map((chip, key) => (
                    <Chip
                        key={_.get(chip, 'id')}
                        nominal={_.get(chip, 'nominal')}
                        color={_.get(chip, 'color')}
                        className={cn({ active: chip.active, [`index-${key}`]: true })}
                        onClick={toggleChipsPanel}
                    />
                )) }
            </div>
            <div className="Section repeat">
                <Button className="action-button" color="primary" variant="outlined">
                    <Icon path="repeat" />
                </Button>
                <h4 className="title">Repeat</h4>
            </div>
            { showChips && (
                <div ref={chipsRef} className="Chips__Full">
                    { chipsArray && chipsArray.map((chip, key) => (
                        <Chip
                            key={_.get(chip, 'id')}
                            nominal={_.get(chip, 'nominal')}
                            color={_.get(chip, 'color')}
                            className={cn('full', { active: chip.active })}
                            onClick={() => setActiveChip(_.get(chip, 'id', key))}
                        />
                    )) }
                </div>
            ) }
        </div>
    );
}

const mapStateToProps = (state) => {
    const chips = _.get(state, 'game.chips');
    const showActiveChipPanel = _.get(state, 'game.showActiveChipPanel');

    return {
        chips,
        showActiveChipPanel,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

ChipPanel.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    chips: PropTypes.object.isRequired,
    isPanelDown: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(ChipPanel);
