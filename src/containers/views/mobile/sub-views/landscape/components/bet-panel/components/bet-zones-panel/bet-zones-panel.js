import React from 'react';
import * as cn from 'classnames';
import * as _ from 'lodash';
import PersonIcon from '@material-ui/icons/Person';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import getSymbolFromCurrency from 'currency-symbol-map';
import {
    BankerPairSvg,
    BankerSvg,
    PlayerPairSvg,
    PlayerSvg,
    TieSvg,
} from '../../../../../../../../../components/svg-components/bet-panel';
import BetStatistic from '../../../../../../../../../components/svg-components/bet-statistic';
import { number } from '../../../../../../../../../lang';
import Icon from '../../../../../../../../../components/svg-components/icons/icon';
import BaccaratRoundModel from '../../../../../../../../../models/baccarat-round-model';
import PlayerInfoModel from '../../../../../../../../../models/player-info-model';
import ChipModel from '../../../../../../../../../models/chip-model';
import Types from '../../../../../../../../../classes/Types';
import config from '../../../../../../../../../config';
import { Card } from '../../../../../../../../../components/svg-components';
import BaccaratBetModel from '../../../../../../../../../models/baccarat-bet-model';
import { api } from '../../../../../../../../../services';
import Chip from '../../../../../../../../../components/chip';
import './bet-zones-panel.scss';

function BetZonesPanel({
    round,
    playerInfo,
    chips,
    activeChip,
    skipCurrentRound,
}) {
    const stages = Types.API__ROUND__STAGE;
    const status = _.get(round, 'stageName', '');
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    const isPanelDown = !!((status === stages[3]) || (status === stages[4]) || skipCurrentRound || isLowBalance);
    const roundResult = _.get(round, 'roundResult');
    const betsStat = _.get(round, 'betsStat');
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const currencySymbol = getSymbolFromCurrency(currency);
    const chipsArray = Object.values(chips.toObject()).map((v) => v);

    /*
     * @type:string - player | banker
     */

    const renderCards = (type = 'player') => {
        const isDealCards = ((status === stages[3]) || (status === stages[4]));
        const cards = _.get(round, `${type}Cards`);
        const points = _.get(round, `${type}Score`, 0);
        const showPoints = (Array.isArray(cards) && (cards.length > 0) && isDealCards);
        const score = showPoints ? points : '';
        const primaryCard = _.get(cards, '[0]', null);
        const primaryComponent = (primaryCard && isDealCards) ? (<Card type={primaryCard} />) : ' ';
        const secondaryCard = _.get(cards, '[1]', null);
        const secondaryComponent = (secondaryCard && isDealCards) ? (<Card type={secondaryCard} />) : ' ';
        const additionalCard = _.get(cards, '[2]', null);
        const additionalComponent = (additionalCard && isDealCards) ? (<Card type={additionalCard} />) : ' ';

        return (
            <>
                <div className="Cards__Points">{score}</div>
                <div className="Cards__Wrapper">
                    {(type === 'player') ? (
                        <>
                            <div className="Card Card__Additional">{additionalComponent}</div>
                            <div className="Card Card__Primary">{primaryComponent}</div>
                            <div className="Card Card__Secondary">{secondaryComponent}</div>
                        </>
                    ) : (
                        <>
                            <div className="Card Card__Primary">{primaryComponent}</div>
                            <div className="Card Card__Secondary">{secondaryComponent}</div>
                            <div className="Card Card__Additional">{additionalComponent}</div>
                        </>
                    )}
                </div>
            </>
        );
    };

    /*
    *  @type:number - 1 (player) | 2 (banker) | 3 (tie) | 4 (player pair) | 5 (banker pair)
    */
    const doBet = (type = 1) => {
        if (isPanelDown) return;

        const model = new BaccaratBetModel({
            BetValue: _.get(activeChip, 'nominal', 0),
            BetType: type,
        });

        api.doBet(model);
    };

    /*
    *  @type:number - 1 (player) | 2 (banker) | 3 (tie) | 4 (player pair) | 5 (banker pair)
    */
    const renderBet = (type = 1) => {
        const totalBets = _.get(playerInfo, 'totalBets', []);
        const currentBet = totalBets.find((bet) => _.get(bet, 'betType', 6) === type);
        const nominal = _.get(currentBet, 'betValue', 0);
        let color = 'blue';

        chipsArray.forEach((c) => {
            if (nominal >= c.nominal) { color = c.color; }
        });

        if (nominal > 0) {
            return (
                <div className={`bet-chip-${type}`}>
                    <Chip nominal={nominal} color={color} />
                </div>
            );
        }

        return null;
    };

    return (
        <div className="BetZonesPanel__Landscape">
            <div className="GamePanel__Bet__Panel__Zones">
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div
                    className={cn({ Zone: true, Zone__PlayerPair: true, active: !isPanelDown })}
                    onClick={() => { doBet(4); }}
                >
                    <PlayerPairSvg />
                    <div className="Zone__PlayerPair__Wrapper">
                        <div className="Zone__PlayerPair__Name">
                            P Pair
                            { renderBet(4) }
                        </div>
                        <div className="Zone__PlayerPair__Factor">11:1</div>
                    </div>
                </div>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div
                    className={cn({ Zone: true, Zone__Player: true, active: !isPanelDown })}
                    onClick={() => { doBet(1); }}
                >
                    <PlayerSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Player')) })} />
                    <div className="Zone__Player__Wrapper">
                        <div className="Zone__Player__Statistic">
                            <div className="Zone__Player__Statistic__Details">
                                <div className="Details__Percent">
                                    <BetStatistic percent={_.get(betsStat, 'playerPercent', 0)} type="player" />
                                </div>
                                <div className="Details__Numbers">
                                    <div className="Numbers__Row">
                                        <div className="Numbers__Col icon">{currencySymbol}</div>
                                        {' '}
                                        <div className="Numbers__Col count">{number({ value: _.get(betsStat, 'playerBetsSum', 0) })}</div>
                                    </div>
                                    <div className="Numbers__Row">
                                        <div className="Numbers__Col icon"><PersonIcon /></div>
                                        {' '}
                                        <div className="Numbers__Col count">{_.get(betsStat, 'playerBetsCount', 0)}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="Zone__Player__Cards">
                            {renderCards('player')}
                        </div>
                        <div className="Zone__Player__Name">
                            <div>
                                <Icon path="player" />
                            </div>
                            <div>Player</div>
                            { renderBet() }
                        </div>
                    </div>
                </div>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div
                    className={cn({ Zone: true, Zone__Tie: true, active: !isPanelDown })}
                    onClick={() => { doBet(3); }}
                >
                    <TieSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Tie')) })} />
                    <div className="Zone__Tie__Wrapper">
                        <div className="Zone__Tie__Statistic">
                            <div className="Statistic__Percent">
                                <BetStatistic percent={_.get(betsStat, 'tiePercent', 0)} type="tie" />
                            </div>
                            <div className="Statistic__Numbers">
                                <div className="Numbers__Row">
                                    <div className="Numbers__Col icon">{currencySymbol}</div>
                                    {' '}
                                    <div className="Numbers__Col count">{number({ value: _.get(betsStat, 'tieBetsSum', 0), maximumFractionDigits: 2 })}</div>
                                </div>
                                <div className="Numbers__Row">
                                    <div className="Numbers__Col icon"><PersonIcon /></div>
                                    {' '}
                                    <div className="Numbers__Col count">{_.get(betsStat, 'tieBetsCount', 0)}</div>
                                </div>
                            </div>
                        </div>
                        <div className="Zone__Tie__Name">
                            Tie 8:1
                            { renderBet(3) }
                        </div>
                    </div>
                </div>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div
                    className={cn({ Zone: true, Zone__Banker: true, active: !isPanelDown })}
                    onClick={() => { doBet(2); }}
                >
                    <BankerSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Banker')) })} />
                    <div className="Zone__Banker__Wrapper">
                        <div className="Zone__Banker__Statistic">
                            <div className="Zone__Banker__Statistic__Details">
                                <div className="Details__Numbers">
                                    <div className="Numbers__Row">
                                        <div className="Numbers__Col icon">{currencySymbol}</div>
                                        {' '}
                                        <div className="Numbers__Col count">{number({ value: _.get(betsStat, 'bankerBetsSum', 0), maximumFractionDigits: 2 })}</div>
                                    </div>
                                    <div className="Numbers__Row">
                                        <div className="Numbers__Col icon"><PersonIcon /></div>
                                        {' '}
                                        <div className="Numbers__Col count">{_.get(betsStat, 'bankerBetsCount', 0)}</div>
                                    </div>
                                </div>
                                <div className="Details__Percent">
                                    <BetStatistic percent={_.get(betsStat, 'bankerPercent', 0)} type="banker" />
                                </div>
                            </div>
                        </div>
                        <div className="Zone__Banker__Cards">
                            {renderCards('banker')}
                        </div>
                        <div className="Zone__Banker__Name">
                            <div>
                                <Icon path="banker" />
                            </div>
                            <div>Banker</div>
                            { renderBet(2) }
                        </div>
                    </div>
                </div>
                {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                <div
                    className={cn({ Zone: true, Zone__BankerPair: true, active: !isPanelDown })}
                    onClick={() => { doBet(5); }}
                >
                    <BankerPairSvg />
                    <div className="Zone__BankerPair__Wrapper">
                        <div className="Zone__BankerPair__Name">
                            B Pair
                            { renderBet(5) }
                        </div>
                        <div className="Zone__BankerPair__Factor">11:1</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

BetZonesPanel.propTypes = {
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    chips: PropTypes.object.isRequired,
    // dispatch: PropTypes.func.isRequired,
    activeChip: PropTypes.instanceOf(ChipModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const round = _.get(state, 'game.round');
    const playerInfo = _.get(state, 'game.playerInfo');
    const chips = _.get(state, 'game.chips');
    const activeChip = _.get(state, 'game.activeChip');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    return {
        round, playerInfo, chips, activeChip, skipCurrentRound,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(BetZonesPanel);
