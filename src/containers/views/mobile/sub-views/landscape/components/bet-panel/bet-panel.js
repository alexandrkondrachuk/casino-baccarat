import React from 'react';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import * as cn from 'classnames';
import { BetZonesPanel, StatisticPanel, ChipPanel } from './components';
import Types from '../../../../../../../classes/Types';
import BaccaratRoundModel from '../../../../../../../models/baccarat-round-model';
import PlayerInfoModel from '../../../../../../../models/player-info-model';

import './bet-panel.scss';

function BetPanel({ round, playerInfo, skipCurrentRound }) {
    const stages = Types.API__ROUND__STAGE;
    const status = _.get(round, 'stageName', '');
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    const isPanelDown = !!((status === stages[3]) || (status === stages[4]) || skipCurrentRound || isLowBalance);
    return (
        <div className="BetPanel__Landscape">
            <div className="BetPanel__Landscape__Left">
                <ChipPanel isPanelDown={isPanelDown} />
            </div>
            <div className="BetPanel__Landscape__Middle">
                <div className={cn('Middle__Wrapper', { down: isPanelDown })}>
                    <BetZonesPanel />
                    <StatisticPanel />
                </div>
            </div>
            <div className="BetPanel__Landscape__Right" />
        </div>
    );
}

BetPanel.propTypes = {
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const round = _.get(state, 'game.round');
    const playerInfo = _.get(state, 'game.playerInfo');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    return {
        round, playerInfo, skipCurrentRound,
    };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, dispatchStateToProps)(BetPanel);
