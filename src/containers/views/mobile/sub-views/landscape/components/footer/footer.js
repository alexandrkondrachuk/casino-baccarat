import React from 'react';
import * as _ from 'lodash';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import getSymbolFromCurrency from 'currency-symbol-map';
import CurrencyModel from '../../../../../../../models/currency-model';
import PlayerInfoModel from '../../../../../../../models/player-info-model';
import config from '../../../../../../../config';
import { number } from '../../../../../../../lang';

import './footer.scss';

function Footer({
    currencyModel,
    playerInfo,
    roundId,
}) {
    const { minBet, maxPlayerBankerBet } = currencyModel;
    const name = config.get('name', '').split(' ');
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const currencySymbol = getSymbolFromCurrency(currency);

    return (
        <div className="Footer__Landscape">
            <div className="Footer__Landscape__Limits">
                <span className="name">
                    { _.get(name, '[0]', '') }
                    { ' ' }
                    { _.get(name, '[1]', '') }
                    { ' ' }
                    { _.get(name, '[2]', '') }
                    { ' ' }
                </span>
                &nbsp;
                <span className="limits">
                    {currencySymbol}
                    {number({ value: minBet })}
                    { '-' }
                    {number({ value: maxPlayerBankerBet })}
                </span>
            </div>
            <div className="Footer__Landscape__Round">
                #
                {roundId}
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    const currencyModel = _.get(state, 'game.currency');
    const playerInfo = _.get(state, 'game.playerInfo');
    const roundId = _.get(state, 'game.round.id', 0);
    return {
        currencyModel, playerInfo, roundId,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

Footer.propTypes = {
    currencyModel: PropTypes.instanceOf(CurrencyModel).isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    roundId: PropTypes.number.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(Footer);
