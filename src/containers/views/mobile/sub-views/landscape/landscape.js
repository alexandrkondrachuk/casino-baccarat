import React from 'react';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { isIOS } from 'react-device-detect';
import WebrtcPlayer from '../../../../webrtc-player';
import { IOSSoundModal } from '../portrait/components';
import { Header, Footer, BetPanel } from './components';
import BaccaratRoundModel from '../../../../../models/baccarat-round-model';
import GameResult from '../../../../../components/game-result';
import MobileMenu from '../menu';

import './landscape.scss';

function Landscape({
    player,
    maskStatus,
    showRoundTimer,
    round,
    win,
    currency,
}) {
    const ua = window.navigator.userAgent;
    const isStartFromApp = /Parimatch/.test(ua);
    const roundResult = _.get(round, 'roundResult');

    const enableSound = () => {
        if (player) {
            player.unmute();
            player.setVolume(1);
        }
    };
    const enableGameSounds = () => {
        enableSound();
    };
    return (
        <div id="Landscape" className={cn('Landscape')}>
            <MobileMenu pageWrapId="Landscape" outerContainerId="Mobile" />
            <WebrtcPlayer />
            <div className="Landscape__Wrapper">
                <Header />
                <BetPanel />
                <Footer />
            </div>
            {(showRoundTimer && roundResult) && <GameResult showRoundTimer={showRoundTimer} type={roundResult.toLowerCase()} winAmount={win} currency={currency} />}
            {(isIOS && !isStartFromApp && !maskStatus) && <IOSSoundModal enableGameSounds={enableGameSounds} />}
        </div>
    );
}

const mapStateToProps = (state) => {
    const player = _.get(state, 'stream.playerInstance');
    const maskStatus = _.get(state, 'app.maskStatus');
    const showRoundTimer = _.get(state, 'game.showRoundTimer');
    const round = _.get(state, 'game.round');
    const win = _.get(state, 'game.win');
    const currency = _.get(state, 'game.playerInfo.currency', 'USD');
    return {
        player,
        maskStatus,
        showRoundTimer,
        round,
        win,
        currency,
    };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

Landscape.defaultProps = {
    player: null,
};
Landscape.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
    maskStatus: PropTypes.bool.isRequired,
    showRoundTimer: PropTypes.bool.isRequired,
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    win: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(Landscape);
