import React from 'react';
import * as PropTypes from 'prop-types';
import Chat from '../chat';
import Help from '../help';
import History from '../history';
import Limits from '../limits';
import Settings from '../settings';

export default function MenuRenderer({ type }) {
    const components = {
        chat: Chat,
        help: Help,
        history: History,
        limits: Limits,
        settings: Settings,
    };
    const TagName = components[type];
    return (<TagName />);
}

MenuRenderer.propTypes = {
    type: PropTypes.oneOf(['chat', 'help', 'history', 'limits', 'settings']).isRequired,
};
