import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import MaintenanceIcon from './maintenance-icon';
import config from '../../../config';

import './maintenance.scss';

function Maintenance({ maintenanceStatus }) {
    const renderRedirect = (path = '/') => (<Redirect to={path} />);
    return (
        <div className="Maintenance">
            {!maintenanceStatus && renderRedirect(config.get('apiURLs.main'))}
            <div className="Maintenance__Header">
                <MaintenanceIcon />
            </div>
            <div className="Maintenance__Body">
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                <p>Dear player! Unfortunately the game is down for scheduled maintenance. But soon we'll be up and the game will start again.</p>
            </div>
        </div>
    );
}

Maintenance.propTypes = {
    maintenanceStatus: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const maintenanceStatus = _.get(state, 'app.maintenanceStatus');
    return {
        maintenanceStatus,
    };
};

const dispatchStateToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, dispatchStateToProps)(Maintenance);
