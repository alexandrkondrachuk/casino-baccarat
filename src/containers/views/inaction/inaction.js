import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';
import InactionIcon from './inaction-icon';
import config from '../../../config';
import { app as appActions } from '../../../store/actions';

import './inaction.scss';

function Inaction({ inactionStatus, dispatch }) {
    const backGame = () => {
        dispatch(appActions.setReconnectStatus(true));
        dispatch(appActions.setInactionStatus(!inactionStatus));
    };
    const renderRedirect = (path = '/') => (<Redirect to={path} />);
    return (
        <div className="Inaction">
            { !inactionStatus && renderRedirect(config.get('apiURLs.main')) }
            <div className="Inaction__Header">
                <InactionIcon />
            </div>
            <div className="Inaction__Body">
                <p>Dear player! Your game session was stopped due an inactivity for a long period of time. If you would like continue press button below.</p>
                <Button color="primary" variant="outlined" onClick={backGame}>Back to the game</Button>
            </div>
        </div>
    );
}

Inaction.propTypes = {
    inactionStatus: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const inactionStatus = _.get(state, 'app.inactionStatus');
    return { inactionStatus };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, dispatchStateToProps)(Inaction);
