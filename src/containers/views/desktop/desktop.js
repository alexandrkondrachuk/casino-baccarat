import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Limits from '../../limits';
import Settings from '../../settings';
import WebrtcPlayer from '../../webrtc-player';
import GamePanel from '../../game-panel';
import GameResult from '../../../components/game-result';
import BaccaratRoundModel from '../../../models/baccarat-round-model';
import config from '../../../config';

import './desktop.scss';

function Desktop({
    readyState,
    showRoundTimer,
    round,
    win,
    currency,
    blockStatus,
    inactionStatus,
    maintenanceStatus,
}) {
    const renderRedirect = (path = '/') => (<Redirect to={path} />);
    const roundResult = _.get(round, 'roundResult');
    return (
        <div className="Desktop">
            {blockStatus && renderRedirect(config.get('apiURLs.issue'))}
            {inactionStatus && renderRedirect(config.get('apiURLs.inaction'))}
            {maintenanceStatus && renderRedirect(config.get('apiURLs.maintenance'))}
            {!readyState && renderRedirect() }
            <div className="Desktop__Container">
                <Limits />
                <Settings />
                <WebrtcPlayer />
                <GamePanel />
                {(showRoundTimer && roundResult) && <GameResult showRoundTimer={showRoundTimer} type={roundResult.toLowerCase()} winAmount={win} currency={currency} />}
            </div>
        </div>
    );
}

Desktop.defaultProps = {};

Desktop.propTypes = {
    readyState: PropTypes.bool.isRequired,
    showRoundTimer: PropTypes.bool.isRequired,
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    win: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    // dispatch: PropTypes.func.isRequired,
    blockStatus: PropTypes.bool.isRequired,
    inactionStatus: PropTypes.bool.isRequired,
    maintenanceStatus: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const readyState = _.get(state, 'game.readyState');
    const player = _.get(state, 'stream.playerInstance');
    const showRoundTimer = _.get(state, 'game.showRoundTimer');
    const round = _.get(state, 'game.round');
    const win = _.get(state, 'game.win');
    const currency = _.get(state, 'game.playerInfo.currency', 'USD');
    const blockStatus = _.get(state, 'app.blockStatus');
    const inactionStatus = _.get(state, 'app.inactionStatus');
    const maintenanceStatus = _.get(state, 'app.maintenanceStatus');
    return {
        readyState,
        player,
        showRoundTimer,
        round,
        win,
        currency,
        blockStatus,
        inactionStatus,
        maintenanceStatus,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(Desktop);
