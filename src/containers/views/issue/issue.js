import React from 'react';
import { connect } from 'react-redux';
import { IssueIcon } from './ components';

import './issue.scss';

function Issue() {
    return (
        <div className="Issue">
            <div className="Issue__Header">
                <IssueIcon />
            </div>
            <div className="Issue__Body">
                <p>
                    Dear player!
                    Unfortunately the game is unavailable at the moment due a technical issue.
                    The problem will be resolved as fast as it possible.
                    Please try again later. Sorry for the inconvenience.
                </p>
            </div>
        </div>
    );
}

export default connect()(Issue);
