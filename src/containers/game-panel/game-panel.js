import React from 'react';
import { connect } from 'react-redux';
import { BeadRoad, BetsPanel, BigRoad } from './components';

import './game-panel.scss';

function GamePanel() {
    return (
        <div className="GamePanel unselectable">
            <BeadRoad />
            <BetsPanel />
            <BigRoad />
        </div>
    );
}

GamePanel.propTypes = {
    // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    state,
});

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(GamePanel);
