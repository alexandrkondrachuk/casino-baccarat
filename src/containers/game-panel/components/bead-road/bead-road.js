import React, { useRef } from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import getSymbolFromCurrency from 'currency-symbol-map';
import { BeadRoadScale } from '../../../../components/svg-components/history';
import { number } from '../../../../lang';
import config from '../../../../config';
import PlayerInfoModel from '../../../../models/player-info-model';
import PopoverCustom from '../../../../components/popover';
import { game as gameActions } from '../../../../store/actions';

import './bead-road.scss';

function BeadRoad({ playerInfo, showLowBalanceMessage, dispatch }) {
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const currencySymbol = getSymbolFromCurrency(currency);
    const isBalanceLow = _.get(playerInfo, 'isLowBalance', true);
    const showMessage = (isBalanceLow && showLowBalanceMessage);
    const balanceRef = useRef(null);

    const togglePopover = () => dispatch(gameActions.setLowBalanceMessageStatus(!showLowBalanceMessage));

    return (
        <div className="GamePanel__Col GamePanel__Bead__Road">
            <div className="Bead__Road">
                <div className="Bead__Road__Statistic">
                    <BeadRoadScale />
                </div>
                <div className="Bead__Road__Balance">
                    <Button id="balance" className="Balance__Button" variant="outlined" color="primary" disabled ref={balanceRef}>
                        <div className="title">Balance</div>
                        <div className="value">
                            {currencySymbol}
                            {number({ value: _.get(playerInfo, 'balance', 0) })}
                        </div>
                    </Button>
                    <Button className="Balance__Button" variant="outlined" color="primary" disabled>
                        <div className="title">Total Bet</div>
                        <div className="value">
                            {currencySymbol}
                            {number({ value: _.get(playerInfo, 'totalBetOfCurrentRound', 0) })}
                        </div>
                    </Button>
                    {(balanceRef && balanceRef.current) && (
                        <PopoverCustom
                            title="Your balance is too low"
                            target="balance"
                            isOpen={showMessage}
                            toggle={togglePopover}
                            placement="top-end"
                        >
                            Your balance is too low to make bets. Please fund your account to play.
                        </PopoverCustom>
                    )}
                </div>
            </div>
        </div>
    );
}

BeadRoad.propTypes = {
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    showLowBalanceMessage: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const playerInfo = _.get(state, 'game.playerInfo');
    const showLowBalanceMessage = _.get(state, 'game.showLowBalanceMessage');
    return {
        playerInfo,
        showLowBalanceMessage,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(BeadRoad);
