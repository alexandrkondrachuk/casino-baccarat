import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import {
    BT, BTB, PPT, PT, TT,
} from '../../../../components/svg-components/history/total';
import { BigRoadScale } from '../../../../components/svg-components/history';
import ShoeStatModel from '../../../../models/shoe-stat-model';
import Tooltip from '../../../../components/tooltip';

import './big-road.scss';

function BigRoad({ shoeStat }) {
    return (
        <div className="GamePanel__Col GamePanel__Big__Road">
            <div className="Big__Road">
                <div className="Big__Road__Statistic">
                    <div className="Big__Road__Statistic__Title">
                        <div className="Title__Col Title__Total">
                            <div id="rounds-count" className="Element Total__Round">
                                <span className="mark-info">#</span>
                                &nbsp;
                                <span className="value">{_.get(shoeStat, 'roundsCount', 0)}</span>
                            </div>
                            <Tooltip targetId="rounds-count" placement="top">Total number of rounds</Tooltip>
                            <div id="player-wins" className="Element Total__Player">
                                <span className="mark-info"><PT /></span>
                                <span className="value">{_.get(shoeStat, 'playerWins', 0)}</span>
                            </div>
                            <Tooltip targetId="player-wins" placement="top">Player</Tooltip>
                            <div id="banker-wins" className="Element Total__Banker">
                                <span className="mark-info"><BT /></span>
                                <span className="value">{_.get(shoeStat, 'bankerWins', 0)}</span>
                            </div>
                            <Tooltip targetId="banker-wins" placement="top">Banker</Tooltip>
                            <div id="tie-wins" className="Element Total__Tie">
                                <span className="mark-info"><TT /></span>
                                <span className="value">{_.get(shoeStat, 'tieWins', 0)}</span>
                            </div>
                            <Tooltip targetId="tie-wins" placement="top">Tie</Tooltip>
                            <div id="player-pair-wins" className="Element Total__PPair">
                                <span className="mark-info"><PPT /></span>
                                <span className="value">{_.get(shoeStat, 'playerPairWins', 0)}</span>
                            </div>
                            <Tooltip targetId="player-pair-wins" placement="top">Player pair</Tooltip>
                            <div id="banker-pair-wins" className="Element Total__BPair">
                                <span className="mark-info"><BTB /></span>
                                <span className="value">{_.get(shoeStat, 'bankerPairWins', 0)}</span>
                            </div>
                            <Tooltip targetId="banker-pair-wins" placement="top">Banker pair</Tooltip>
                        </div>
                        <div className="Title__Col Title__Legend">{ ' ' }</div>
                    </div>
                    <div className="Big__Road__Statistic__Content">
                        <BigRoadScale />
                    </div>
                </div>
                <div className="Big__Road__Panel">
                    <Button className="Balance__Button" variant="outlined" color="primary">
                        Lobby
                    </Button>
                </div>
            </div>
        </div>
    );
}

BigRoad.propTypes = {
    shoeStat: PropTypes.instanceOf(ShoeStatModel).isRequired,
};

const mapStateToProps = (state) => {
    const shoeStat = _.get(state, 'game.shoeStat');
    return {
        shoeStat,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(BigRoad);
