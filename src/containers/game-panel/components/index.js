import BeadRoad from './bead-road';
import BetsPanel from './bets-panel';
import BigRoad from './big-road';

export {
    BeadRoad, BetsPanel, BigRoad,
};
