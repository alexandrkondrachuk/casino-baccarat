import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import * as cn from 'classnames';
import { connect } from 'react-redux';
import PersonIcon from '@material-ui/icons/Person';
import getSymbolFromCurrency from 'currency-symbol-map';
import ClickNHold from 'react-click-n-hold';
import Timer from '../../../../components/timer';
import config from '../../../../config';
import {
    BankerPairSvg,
    BankerSvg,
    PlayerPairSvg,
    PlayerSvg,
    TieSvg,
} from '../../../../components/svg-components/bet-panel';
import Types from '../../../../classes/Types';
import BetStatistic from '../../../../components/svg-components/bet-statistic';
import { Card } from '../../../../components/svg-components';
import Icon from '../../../../components/svg-components/icons/icon';
import Chip from '../../../../components/chip';
import BaccaratRoundModel from '../../../../models/baccarat-round-model';
import PlayerInfoModel from '../../../../models/player-info-model';
import BaccaratBetModel from '../../../../models/baccarat-bet-model';
import ChipModel from '../../../../models/chip-model';
import { game as gameActions } from '../../../../store/actions';
import { api } from '../../../../services';

import './bets-panel.scss';
import { number } from '../../../../lang';

function BetsPanel({
    round,
    betsTimer,
    showTimer,
    playerInfo,
    chips,
    dispatch,
    activeChip,
    skipCurrentRound,
    showBetStatistic,
}) {
    const [undoInterval, setUndoInterval] = useState(null);
    const stages = Types.API__ROUND__STAGE;
    const stagesDescription = Types.API__STAGE__DESCRIPTION;
    const status = _.get(round, 'stageName', '');
    const isLowBalance = _.get(playerInfo, 'isLowBalance', true);
    // eslint-disable-next-line no-nested-ternary
    const description = skipCurrentRound
        ? _.get(stagesDescription, 'WaitAnotherRound', '')
        : isLowBalance
            ? _.get(stagesDescription, 'LowBalance', '')
            : _.get(stagesDescription, `[${status}]`, '');
    const isPanelDown = !!((status === stages[3]) || (status === stages[4]) || skipCurrentRound || isLowBalance);
    const roundResult = _.get(round, 'roundResult');
    const betsStat = _.get(round, 'betsStat');
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const currencySymbol = getSymbolFromCurrency(currency);
    const chipsArray = Object.values(chips.toObject()).map((v) => v);

    /*
     * @type:string - player | banker
     */

    const renderCards = (type = 'player') => {
        const isDealCards = ((status === stages[3]) || (status === stages[4]));
        const cards = _.get(round, `${type}Cards`);
        const points = _.get(round, `${type}Score`, 0);
        const showPoints = (Array.isArray(cards) && (cards.length > 0) && isDealCards);
        const score = showPoints ? points : '';
        const primaryCard = _.get(cards, '[0]', null);
        const primaryComponent = (primaryCard && isDealCards) ? (<Card type={primaryCard} />) : ' ';
        const secondaryCard = _.get(cards, '[1]', null);
        const secondaryComponent = (secondaryCard && isDealCards) ? (<Card type={secondaryCard} />) : ' ';
        const additionalCard = _.get(cards, '[2]', null);
        const additionalComponent = (additionalCard && isDealCards) ? (<Card type={additionalCard} />) : ' ';

        return (
            <>
                <div className="Cards__Points">{score}</div>
                <div className="Cards__Wrapper">
                    {(type === 'player') ? (
                        <>
                            <div className="Card Card__Additional">{additionalComponent}</div>
                            <div className="Card Card__Primary">{primaryComponent}</div>
                            <div className="Card Card__Secondary">{secondaryComponent}</div>
                        </>
                    ) : (
                        <>
                            <div className="Card Card__Primary">{primaryComponent}</div>
                            <div className="Card Card__Secondary">{secondaryComponent}</div>
                            <div className="Card Card__Additional">{additionalComponent}</div>
                        </>
                    )}
                </div>
            </>
        );
    };

    const setActiveChip = (id) => {
        dispatch(gameActions.setActiveChip(id));
    };

    /*
    *  @type:number - 1 (player) | 2 (banker) | 3 (tie) | 4 (player pair) | 5 (banker pair)
    */
    const doBet = (type = 1) => {
        if (isPanelDown) return;

        const model = new BaccaratBetModel({
            BetValue: _.get(activeChip, 'nominal', 0),
            BetType: type,
        });

        api.doBet(model);
    };

    /*
    *  @type:number - 1 (player) | 2 (banker) | 3 (tie) | 4 (player pair) | 5 (banker pair)
    */
    const renderBet = (type = 1) => {
        const totalBets = _.get(playerInfo, 'totalBets', []);
        const currentBet = totalBets.find((bet) => _.get(bet, 'betType', 6) === type);
        const nominal = _.get(currentBet, 'betValue', 0);
        let color = 'blue';

        chipsArray.forEach((c) => {
            if (nominal >= c.nominal) { color = c.color; }
        });

        if (nominal > 0) {
            return (
                <div className={`bet-chip-${type}`}>
                    <Chip nominal={nominal} color={color} />
                </div>
            );
        }

        return null;
    };

    /**
     * @param type:string - LastBet | AllBets
     * @returns void
     */
    const doUndoBet = (type = 'LastBet') => {
        api
            .doUndoBet(type)
            .then((res) => {
                console.info('doUndo: ', res);
            })
            .catch((err) => {
                console.error('doUndo: ', err);
            });
    };

    /**
     *  Hold effect
     * */

    const start = () => {
        console.log('START');
    };

    const end = () => {
        if (undoInterval) clearInterval(undoInterval);
    };

    const clickNHold = () => {
        const uInterval = setInterval(() => {
            doUndoBet();
        }, config.get('undoDelay', 1000));
        setUndoInterval(uInterval);
    };

    return (
        <div className={cn({
            BetsPanel: true,
            GamePanel__Col: true,
            GamePanel__Bet__Panel__Wrapper: true,
            down: isPanelDown,
            [status]: !!status,
            skipCurrentRound: !!skipCurrentRound,
            lowBalance: !!isLowBalance,
        })}
        >
            <div className="GamePanel__Col GamePanel__Bet__Panel">
                <div className="GamePanel__Timer">
                    {(showTimer && !skipCurrentRound && !isLowBalance && (status !== stages[1])) && (<Timer length={betsTimer} />)}
                </div>
                <div className="GamePanel__Bet__Panel__Title">
                    {description.replace(/{name}/g, roundResult)}
                </div>
                <div className="GamePanel__Bet__Panel__Zones">
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div
                        className={cn({ Zone: true, Zone__PlayerPair: true, active: !isPanelDown })}
                        onClick={() => { doBet(4); }}
                    >
                        <PlayerPairSvg />
                        <div className="Zone__PlayerPair__Wrapper">
                            <div className="Zone__PlayerPair__Name">
                                P Pair
                                { renderBet(4) }
                            </div>
                            <div className="Zone__PlayerPair__Factor">11:1</div>
                        </div>
                    </div>
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div
                        className={cn({ Zone: true, Zone__Player: true, active: !isPanelDown })}
                        onClick={() => { doBet(1); }}
                    >
                        <PlayerSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Player')) })} />
                        <div className="Zone__Player__Wrapper">
                            <div className={cn('Zone__Player__Statistic', { hidden: !showBetStatistic })}>
                                <div className="Zone__Player__Statistic__Details">
                                    <div className="Details__Percent">
                                        <BetStatistic percent={_.get(betsStat, 'playerPercent', 0)} type="player" />
                                    </div>
                                    <div className="Details__Numbers">
                                        <div className="Numbers__Row">
                                            <div className="Numbers__Col icon">{currencySymbol}</div>
                                            {' '}
                                            <div className="Numbers__Col count">{number({ value: _.get(betsStat, 'playerBetsSum', 0) })}</div>
                                        </div>
                                        <div className="Numbers__Row">
                                            <div className="Numbers__Col icon"><PersonIcon /></div>
                                            {' '}
                                            <div className="Numbers__Col count">{_.get(betsStat, 'playerBetsCount', 0)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="Zone__Player__Cards">
                                {renderCards('player')}
                            </div>
                            <div className="Zone__Player__Name">
                                <div>
                                    <Icon path="player" />
                                </div>
                                <div>Player</div>
                                { renderBet() }
                            </div>
                        </div>
                    </div>
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div
                        className={cn({ Zone: true, Zone__Tie: true, active: !isPanelDown })}
                        onClick={() => { doBet(3); }}
                    >
                        <TieSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Tie')) })} />
                        <div className="Zone__Tie__Wrapper">
                            <div className={cn('Zone__Tie__Statistic', { hidden: !showBetStatistic })}>
                                <div className="Statistic__Percent">
                                    <BetStatistic percent={_.get(betsStat, 'tiePercent', 0)} type="tie" />
                                </div>
                                <div className="Statistic__Numbers">
                                    <div className="Numbers__Row">
                                        <div className="Numbers__Col icon">{currencySymbol}</div>
                                        {' '}
                                        <div className="Numbers__Col count">{number({ value: _.get(betsStat, 'tieBetsSum', 0), maximumFractionDigits: 2 })}</div>
                                    </div>
                                    <div className="Numbers__Row">
                                        <div className="Numbers__Col icon"><PersonIcon /></div>
                                        {' '}
                                        <div className="Numbers__Col count">{_.get(betsStat, 'tieBetsCount', 0)}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="Zone__Tie__Name">
                                Tie 8:1
                                { renderBet(3) }
                            </div>
                        </div>
                    </div>
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div
                        className={cn({ Zone: true, Zone__Banker: true, active: !isPanelDown })}
                        onClick={() => { doBet(2); }}
                    >
                        <BankerSvg className={cn({ 'pulse-fill-opacity': (roundResult && (roundResult === 'Banker')) })} />
                        <div className="Zone__Banker__Wrapper">
                            <div className={cn('Zone__Banker__Statistic', { hidden: !showBetStatistic })}>
                                <div className="Zone__Banker__Statistic__Details">
                                    <div className="Details__Numbers">
                                        <div className="Numbers__Row">
                                            <div className="Numbers__Col icon">{currencySymbol}</div>
                                            {' '}
                                            <div className="Numbers__Col count">{number({ value: _.get(betsStat, 'bankerBetsSum', 0), maximumFractionDigits: 2 })}</div>
                                        </div>
                                        <div className="Numbers__Row">
                                            <div className="Numbers__Col icon"><PersonIcon /></div>
                                            {' '}
                                            <div className="Numbers__Col count">{_.get(betsStat, 'bankerBetsCount', 0)}</div>
                                        </div>
                                    </div>
                                    <div className="Details__Percent">
                                        <BetStatistic percent={_.get(betsStat, 'bankerPercent', 0)} type="banker" />
                                    </div>
                                </div>
                            </div>
                            <div className="Zone__Banker__Cards">
                                {renderCards('banker')}
                            </div>
                            <div className="Zone__Banker__Name">
                                <div>
                                    <Icon path="banker" />
                                </div>
                                <div>Banker</div>
                                { renderBet(2) }
                            </div>
                        </div>
                    </div>
                    {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                    <div
                        className={cn({ Zone: true, Zone__BankerPair: true, active: !isPanelDown })}
                        onClick={() => { doBet(5); }}
                    >
                        <BankerPairSvg />
                        <div className="Zone__BankerPair__Wrapper">
                            <div className="Zone__BankerPair__Name">
                                B Pair
                                { renderBet(5) }
                            </div>
                            <div className="Zone__BankerPair__Factor">11:1</div>
                        </div>
                    </div>
                </div>
                <div className="GamePanel__Bet__Panel__Chips">
                    <div className="Chips__Wrapper">
                        {/* eslint-disable-next-line jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */}
                        <div className="Element">
                            <div className="Element__Text undo">
                                undo
                            </div>
                        </div>
                        <div className="Element">
                            <ClickNHold
                                time={1}
                                onStart={(e) => start(e)}
                                onClickNHold={(e) => clickNHold(e)}
                                onEnd={(e, enough) => end(e, enough)}
                            >
                                <div className="Element__Button undo">
                                    <Icon path="undo" onClick={() => doUndoBet()} />
                                </div>
                            </ClickNHold>
                        </div>
                        {chipsArray.map((chip, key) => (
                            <div className={cn({ Element: true, active: _.get(chip, 'active', false), pulse: _.get(chip, 'active', false) })} key={_.get(chip, 'id', key)}>
                                <div className="Element__Chip">
                                    <Chip nominal={_.get(chip, 'nominal', 1)} color={_.get(chip, 'color', 'blue')} onClick={() => setActiveChip(_.get(chip, 'id', key))} />
                                </div>
                            </div>
                        ))}
                        <div className="Element">
                            <div className="Element__Button double">
                                <Icon path="double" />
                            </div>
                        </div>
                        <div className="Element">
                            <div className="Element__Text double">
                                double
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

BetsPanel.propTypes = {
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    betsTimer: PropTypes.number.isRequired,
    showTimer: PropTypes.bool.isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    chips: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    activeChip: PropTypes.instanceOf(ChipModel).isRequired,
    skipCurrentRound: PropTypes.bool.isRequired,
    showBetStatistic: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => {
    const round = _.get(state, 'game.round');
    const betsTimer = _.get(state, 'game.betsTimer');
    const showTimer = _.get(state, 'game.showTimer');
    const playerInfo = _.get(state, 'game.playerInfo');
    const chips = _.get(state, 'game.chips');
    const activeChip = _.get(state, 'game.activeChip');
    const skipCurrentRound = _.get(state, 'game.skipCurrentRound');
    const showBetStatistic = _.get(state, 'app.mainSettings.showBetStatistic');
    return {
        round,
        betsTimer,
        showTimer,
        playerInfo,
        chips,
        activeChip,
        skipCurrentRound,
        showBetStatistic,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(BetsPanel);
