import React, { useEffect } from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { Redirect, useLocation } from 'react-router-dom';
import {
    isDesktop,
    isTablet,
    isMobile,
} from 'react-device-detect';
import config from '../../config';
import { game as gameActions, app as appActions } from '../../store/actions';
import { initApi, api } from '../../services';

import './home.scss';

function Home({
    readyState,
    dispatch,
    blockStatus,
    maintenanceStatus,
    inactionStatus,
    reconnectStatus,
    queryStore,
}) {
    const useQueryObject = () => {
        const queryObject = {};
        const query = new URLSearchParams(useLocation().search);
        // eslint-disable-next-line no-restricted-syntax
        for (const pair of query.entries()) {
            const [key, value] = pair;
            queryObject[key] = value;
        }
        return queryObject;
    };
    const queryObject = useQueryObject();
    const renderRedirect = (path = '/') => <Redirect to={path} />;
    const setApiInstance = (q = {}) => {
        const { api: apiInstance, computedParams } = initApi(q);
        apiInstance
            .init()
            .then((res) => {
                if (!res) {
                    dispatch(gameActions.setReadyState(false));
                }
                dispatch(gameActions.setQueryComputed(computedParams));
                dispatch(appActions.setReconnectStatus(false));
            })
            .catch((err) => {
                console.error(err);
                dispatch(gameActions.setReadyState(false));
            });
    };

    useEffect(() => {
        // _.delay(() => { dispatch(appActions.setInactionStatus(true)); }, 60000);
    });

    useEffect(() => {
        if ((Object.keys(queryObject).length > 0) && (!queryStore)) dispatch(gameActions.getQuery(queryObject));
        if (!api) setApiInstance(queryObject);
        if (api && reconnectStatus) setApiInstance(queryStore);
    }, [queryObject, dispatch]);

    return (
        <div className="Home">
            {blockStatus && renderRedirect(config.get('apiURLs.issue'))}
            {inactionStatus && renderRedirect(config.get('apiURLs.inaction'))}
            {maintenanceStatus && renderRedirect(config.get('apiURLs.maintenance'))}
            { (!readyState) && renderRedirect(config.get('apiURLs.auth')) }
            { (isDesktop && readyState) && renderRedirect(config.get('apiURLs.desktop')) }
            { (isTablet && readyState) && renderRedirect(config.get('apiURLs.tablet')) }
            { (isMobile && readyState) && renderRedirect(config.get('apiURLs.mobile')) }
        </div>
    );
}

const mapStateToProps = (state) => {
    const readyState = _.get(state, 'game.readyState');
    const blockStatus = _.get(state, 'app.blockStatus');
    const inactionStatus = _.get(state, 'app.inactionStatus');
    const maintenanceStatus = _.get(state, 'app.maintenanceStatus');
    const reconnectStatus = _.get(state, 'app.reconnectStatus');
    const queryStore = _.get(state, 'game.query');
    return {
        readyState,
        blockStatus,
        inactionStatus,
        maintenanceStatus,
        reconnectStatus,
        queryStore,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

Home.defaultProps = {
    queryStore: null,
};

Home.propTypes = {
    readyState: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    blockStatus: PropTypes.bool.isRequired,
    inactionStatus: PropTypes.bool.isRequired,
    maintenanceStatus: PropTypes.bool.isRequired,
    reconnectStatus: PropTypes.bool.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    queryStore: PropTypes.object,
};

export default connect(mapStateToProps, dispatchStateToProps)(Home);
