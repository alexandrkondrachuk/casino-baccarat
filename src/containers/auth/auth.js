import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Animated } from 'react-animated-css';
import CircleLoader from 'react-spinners/CircleLoader';
import { t } from '../../lang';
import config from '../../config';

import './auth.scss';

function Auth({ readyState }) {
    const renderRedirect = (path = '/') => (<Redirect to={path} />);
    const name = config.get('name', '').split(' ');
    return (
        <div className="Auth">
            {readyState && renderRedirect(config.get('apiURLs.main'))}
            <div className="Auth__Container">
                <Animated animationIn="fadeInDown" animationOut="zoomOutDown" animationInDelay={1500} animationInDuration={3000} animationOutDuration={2500} isVisible>
                    <h1>
                        <span className="primary">{ _.get(name, '[0]', '') }</span>
                        {' '}
                        <span className="secondary">{ _.get(name, '[1]', '') }</span>
                        {' '}
                        <small>{ _.get(name, '[2]', '') }</small>
                    </h1>
                </Animated>
                <h2>{ t('The game is loading') }</h2>
                <CircleLoader className="Auth__Loader" color="white" loading />
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    const readyState = _.get(state, 'game.readyState');
    return {
        readyState,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

Auth.propTypes = {
    readyState: PropTypes.bool.isRequired,
    // dispatch: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, dispatchStateToProps)(Auth);
