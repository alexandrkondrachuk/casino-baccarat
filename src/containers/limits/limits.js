import React from 'react';
import * as _ from 'lodash';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Animated } from 'react-animated-css';
import getSymbolFromCurrency from 'currency-symbol-map';
import config from '../../config';
import { Icon } from '../../components/svg-components';
import { number } from '../../lang';
import Tooltip from '../../components/tooltip';
import { game as gameActions } from '../../store/actions';
import PlayerInfoModel from '../../models/player-info-model';
import CurrencyModel from '../../models/currency-model';

import './limits.scss';

const animationDuration = config.get('animationDuration.limits');

function Limits({
    playerInfo, dispatch, showLimits, currencyModel,
}) {
    const { minBet, maxPlayerBankerBet, maxTiePairBet } = currencyModel;
    const name = config.get('name', '').split(' ');
    const currency = _.get(playerInfo, 'currency', config.get('currency'));
    const currencySymbol = getSymbolFromCurrency(currency);

    const toggle = () => {
        dispatch(gameActions.showLimits(!showLimits));
    };

    const show = () => {
        if (!showLimits) dispatch(gameActions.showLimits(true));
    };

    const hide = () => {
        if (showLimits) dispatch(gameActions.showLimits(false));
    };

    return (
        <div className="Limits unselectable">
            <h2>
                {/* eslint-disable-next-line jsx-a11y/mouse-events-have-key-events */}
                <span onMouseOver={show} onMouseOut={hide}>
                    { _.get(name, '[0]', '') }
                    { ' ' }
                    { _.get(name, '[1]', '') }
                    { ' ' }
                    { _.get(name, '[2]', '') }
                    { ' ' }
                    <small>
                        {currencySymbol}
                        {number({ value: minBet })}
                        { '-' }
                        {number({ value: maxPlayerBankerBet })}
                    </small>
                </span>
                <span id="fix-info" className={cn({ on: showLimits, off: !showLimits })}><Icon path="sticky" onClick={toggle} /></span>
                <Tooltip targetId="fix-info">
                    Click to keep info on screen
                </Tooltip>
            </h2>
            <Animated animationIn="fadeInLeft" animationOut="fadeOutLeft" animationInDuration={animationDuration} animationOutDuration={animationDuration} isVisible={showLimits}>
                <div className="Limits__Window">
                    <div className="Window__Row">
                        <div className="Window__Col header">Bet</div>
                        <div className="Window__Col header">Bet limits</div>
                        <div className="Window__Col header">Payout</div>
                    </div>
                    <div className="Window__Row">
                        <div className="Window__Col cell">Player</div>
                        <div className="Window__Col cell">
                            {currencySymbol}
                            {number({ value: minBet })}
                            { '-' }
                            {number({ value: maxPlayerBankerBet })}
                        </div>
                        <div className="Window__Col cell">1:1</div>
                    </div>
                    <div className="Window__Row">
                        <div className="Window__Col cell">Banker</div>
                        <div className="Window__Col cell">
                            {currencySymbol}
                            {number({ value: minBet })}
                            { '-' }
                            {number({ value: maxPlayerBankerBet })}
                        </div>
                        <div className="Window__Col cell">0.95:1</div>
                    </div>
                    <div className="Window__Row">
                        <div className="Window__Col cell">Tie</div>
                        <div className="Window__Col cell">
                            {currencySymbol}
                            {number({ value: minBet })}
                            { '-' }
                            {number({ value: maxTiePairBet })}
                        </div>
                        <div className="Window__Col cell">8:1</div>
                    </div>
                    <div className="Window__Row">
                        <div className="Window__Col cell">P pair/B Pair</div>
                        <div className="Window__Col cell">
                            {currencySymbol}
                            {number({ value: minBet })}
                            { '-' }
                            {number({ value: maxTiePairBet })}
                        </div>
                        <div className="Window__Col cell">11:1</div>
                    </div>
                    <div className="Window__Row d-none">
                        <div className="Window__Col cell">Perfect Pair*</div>
                        <div className="Window__Col cell">
                            {currencySymbol}
                            {number({ value: minBet })}
                            { '-' }
                            {number({ value: 100 })}
                        </div>
                        <div className="Window__Col cell" />
                    </div>
                    <div className="Window__Row d-none">
                        <div className="Window__Col cell secondary">One hand</div>
                        <div className="Window__Col cell secondary" />
                        <div className="Window__Col cell secondary">25:1</div>
                    </div>
                    <div className="Window__Row d-none">
                        <div className="Window__Col cell secondary">Both hands</div>
                        <div className="Window__Col cell secondary" />
                        <div className="Window__Col cell secondary">200:1</div>
                    </div>
                    <div className="Window__Row d-none">
                        <div className="Window__Col cell">Either Pair</div>
                        <div className="Window__Col cell">
                            {currencySymbol}
                            {number({ value: minBet })}
                            { '-' }
                            {number({ value: 5000 })}
                        </div>
                        <div className="Window__Col cell">5:1</div>
                    </div>
                </div>
            </Animated>
        </div>
    );
}

Limits.propTypes = {
    showLimits: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired,
    playerInfo: PropTypes.instanceOf(PlayerInfoModel).isRequired,
    currencyModel: PropTypes.instanceOf(CurrencyModel).isRequired,
};

const mapStateToProps = (state) => {
    const showLimits = _.get(state, 'game.showLimits');
    const playerInfo = _.get(state, 'game.playerInfo');
    const currencyModel = _.get(state, 'game.currency');
    return {
        showLimits,
        playerInfo,
        currencyModel,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});
export default connect(mapStateToProps, dispatchStateToProps)(Limits);
