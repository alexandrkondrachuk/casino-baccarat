import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { stream as streamActions } from '../../store/actions';
import Config from '../../config';

import './webrtc-player.scss';

// eslint-disable-next-line react/prefer-stateless-function
class WebrtcPlayer extends Component {
    static PLAYER__ID = 'webrtc-player';

    static RECONNECT__TIMEOUT = 5 * 1000;

    componentDidMount() {
        this.setupNanoplayer();
    }

    componentWillUnmount() {
        const { player, dispatch } = this.props;
        if (player) {
            player.destroy();
            dispatch(streamActions.getPlayerInstance(null));
        }
    }

    // eslint-disable-next-line consistent-return
    setupNanoplayer = async () => {
        const {
            config, dispatch,
        } = this.props;
        try {
            const nanoPlayer = new window.NanoPlayer(WebrtcPlayer.PLAYER__ID);
            config.events = {
                onReady: (event) => {
                    console.info(`Ready: ${JSON.stringify(event.data.config)}`);
                },
                onStats: _.throttle((event) => {
                    const maxDelay = 2;
                    let bufferDelay = _.get(event, 'data.stats.buffer.delay.current', 1);
                    bufferDelay = (bufferDelay > maxDelay) ? maxDelay : bufferDelay;
                    dispatch(streamActions.getBufferDelay(bufferDelay));
                }, Config.get('playerThrottleDelay')),
                onWarning: (event) => {
                    console.warn(`Warning: ${event.data.message}`);
                },
                onError: (event) => {
                    console.error(`Error: ${event.data.code} ${event.data.message}`);
                    // eslint-disable-next-line react/destructuring-assignment
                    // setTimeout(() => { if (this.props.player) this.props.player.play(); }, WebrtcPlayer.RECONNECT__TIMEOUT);
                },
            };
            await nanoPlayer.setup(config);
            dispatch(streamActions.getPlayerInstance(nanoPlayer));
            return nanoPlayer;
        } catch (e) {
            dispatch(streamActions.getPlayerInstance(null));
            setTimeout(() => { this.setupNanoplayer(); }, WebrtcPlayer.RECONNECT__TIMEOUT);
            console.error(e);
        }
    };

    render() {
        return (
            <div className="WebrtcPlayer">
                <div id={WebrtcPlayer.PLAYER__ID} />
            </div>
        );
    }
}

WebrtcPlayer.defaultProps = {
    player: null,
};

WebrtcPlayer.propTypes = {
    // eslint-disable-next-line react/forbid-prop-types
    config: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
};

const mapStateToProps = (state) => {
    const config = _.get(state, 'stream.config');
    const player = _.get(state, 'stream.playerInstance');

    return {
        config,
        player,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(WebrtcPlayer);
