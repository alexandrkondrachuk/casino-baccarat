import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as axios from 'axios';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import * as cn from 'classnames';
import CircleLoader from 'react-spinners/CircleLoader';
import { Scrollbars } from 'react-custom-scrollbars';
import config from '../../../../config';
import { Icon } from '../../../../components/svg-components';
import HistoryModel from '../../../../models/history-model';
import Types from '../../../../classes/Types';

import './history.scss';

const betTypeClass = Types.API__BET__TYPE__CLASS;

// eslint-disable-next-line react/prefer-stateless-function
class History extends Component {
    static SUCCESS__STATUS = 200;

    static NOT__FOUND__STATUS = 404;

    constructor(props) {
        super(props);
        this.state = {
            history: null,
        };
    }

    componentDidMount() {
        this.getHistory();
    }

    getHistory = async () => {
        const { cid, playerId } = this.props;
        const host = config.get('server.host');
        const dataPoint = config.get('apiDataPoints.history');
        const historyUrl = `${host}${dataPoint}`;
        const params = {
            cid,
            playerId,
        };
        try {
            const res = await axios.get(historyUrl, { params });
            const history = _.get(res, 'data');
            const status = _.get(res, 'status', History.NOT__FOUND__STATUS);
            if (status !== History.SUCCESS__STATUS) return null;
            if (Array.isArray(history) && history.length > 0) {
                const historyModels = history.map((h) => (new HistoryModel({ ...h })));

                this.setState({ history: historyModels.reverse() });
                return history;
            }
            return null;
        } catch (e) {
            console.error(e);
            return null;
        }
    };

    render() {
        const { closeWindow } = this.props;
        const { history } = this.state;
        const close = () => {
            closeWindow();
        };

        return (
            <div className="History">
                <div className="History__Header">
                    <h1>
                        <span className="History__Icon"><Icon path="history" /></span>
                        &nbsp;
                        Game History
                        &nbsp;
                        <span className="History__Close"><Icon path="close" onClick={close} /></span>
                    </h1>
                </div>
                <div className="History__Body">
                    <div className="History__Table Table">
                        <div className="Table__Row">
                            <div className="Table__Col head date">Date</div>
                            <div className="Table__Col head">Bet id</div>
                            <div className="Table__Col head">Round id</div>
                            <div className="Table__Col head bet-name">Bet type</div>
                            <div className="Table__Col head">Bet</div>
                            <div className="Table__Col head">Win/Lose</div>
                        </div>
                        <div className="Table__Body">
                            <Scrollbars
                                className="Table__Scrollbar"
                                renderThumbVertical={({ style, ...props }) => (
                                    <div
                                        {...props}
                                        style={{
                                            ...style, backgroundColor: '#96ffff', width: '6px', opacity: '0.6',
                                        }}
                                    />
                                )}
                            >
                                {!history && (
                                    <div className="Table__Row">
                                        <div className="Table__Col loader">
                                            <CircleLoader className="Table__Loader" color="white" size={24} loading />
                                        </div>
                                    </div>
                                )}
                                {history && history.map((h) => (
                                    <div className="Table__Row data-row" key={_.get(h, 'id', '')}>
                                        <div className="Table__Col data-cell date">{_.get(h, 'creationDateFormatted', '')}</div>
                                        <div className="Table__Col data-cell">{_.get(h, 'id', '')}</div>
                                        <div className="Table__Col data-cell">{_.get(h, 'roundId', '')}</div>
                                        <div className={cn({
                                            Table__Col: true,
                                            'data-cell': true,
                                            'bet-name': true,
                                            [_.get(betTypeClass, _.get(h, 'betName', ''), 'bet')]: true,
                                        })}
                                        >
                                            {_.get(h, 'betName', '')}
                                        </div>
                                        <div className="Table__Col data-cell">{_.get(h, 'betValue', 0)}</div>
                                        <div className={cn({
                                            Table__Col: true,
                                            'data-cell': true,
                                            winLose: true,
                                            red: (_.get(h, 'winLose', 0) < 0),
                                            green: (_.get(h, 'winLose', 0) >= 0),
                                        })}
                                        >
                                            {_.get(h, 'winLose', 0)}
                                        </div>
                                    </div>
                                ))}
                            </Scrollbars>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

History.defaultProps = {
    queryComputed: null,
};

History.propTypes = {
    closeWindow: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    queryComputed: PropTypes.object,
    playerId: PropTypes.string.isRequired,
    cid: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
    const queryComputed = _.get(state, 'game.queryComputed', null);
    const playerId = _.get(state, 'game.playerInfo.playerId');
    const cid = _.get(state, 'game.playerInfo.cid');

    return {
        queryComputed,
        playerId,
        cid,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(History);
