import React from 'react';
import * as PropTypes from 'prop-types';
import { Icon } from '../../../../components/svg-components';
import { VerticalTabs } from './components';

import './help.scss';

function Help({ closeWindow }) {
    const close = () => {
        closeWindow();
    };
    return (
        <div className="Help">
            <div className="Help__Header">
                <h1>
                    <span className="Help__Icon__Help">
                        <Icon path="help" />
                        {' '}
                    </span>
                    Help
                    &nbsp;
                    <span className="Help__Icon__Close">
                        <Icon path="close" onClick={close} />
                        {' '}
                    </span>
                </h1>
            </div>
            <div className="Help__Body">
                <VerticalTabs />
            </div>
        </div>
    );
}

Help.propTypes = {
    closeWindow: PropTypes.func.isRequired,
};

export default Help;
