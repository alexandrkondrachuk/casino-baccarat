import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import {
    Chat, Settings, History, Help,
} from '../index';

function Window({ tag, closeWindow }) {
    const components = {
        chat: Chat,
        settings: Settings,
        history: History,
        help: Help,
    };
    const TagName = _.get(components, `[${tag}]`);
    return <TagName closeWindow={closeWindow} />;
}

Window.defaultProps = {
    tag: 'chat',
};

Window.propTypes = {
    tag: PropTypes.oneOf(['chat', 'settings', 'history', 'help']),
    closeWindow: PropTypes.func.isRequired,
};

export default Window;
