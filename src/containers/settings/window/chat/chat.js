import React, { useState } from 'react';
import * as _ from 'lodash';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import { Icon } from '../../../../components/svg-components';
import { api } from '../../../../services';
import { Message } from './components';

import './chat.scss';

function Chat({ closeWindow, messages }) {
    const [values, setValues] = useState({
        message: '',
    });
    const [touched, setTouched] = useState({
        message: false,
    });
    const close = () => {
        closeWindow();
    };
    const handleMessage = (e) => {
        const { name, value } = e.target;
        setValues((prevState) => ({ ...prevState, [name]: value }));
    };
    const handleTouched = (e) => {
        const { name } = e.target;
        setTouched((prevState) => ({ ...prevState, [name]: true }));
    };
    const handleBlur = (e) => {
        const { name } = e.target;
        setTouched((prevState) => ({ ...prevState, [name]: false }));
    };
    const handleKeyPress = (e) => {
        if (e.key === 'Enter' && touched) {
            api.doSendChatMessage(values.message);
            setValues((prevState) => ({ ...prevState, message: '' }));
        }
    };
    const { message } = values;
    return (
        <div className="Chat">
            <div className="Chat__Title">
                <h1>
                    <span className="Chat__Icon__Base"><Icon path="chat" /></span>
                    Chat
                    <span className="Chat__Icon__Close"><Icon path="close" onClick={close} /></span>
                </h1>
            </div>
            {/* <div className="Chat__Header">
                Chat Header
            </div> */}
            <div className="Chat__Body">
                <Scrollbars
                    className="Chat__Scrollbar"
                    renderThumbVertical={({ style, ...props }) => (
                        <div
                            {...props}
                            style={{
                                ...style, backgroundColor: '#96ffff', width: '6px', opacity: '0.6',
                            }}
                        />
                    )}
                >
                    { (messages && messages.length > 0) && messages.map((m) => (<Message key={m.id} model={m} />)) }
                </Scrollbars>
            </div>
            <div className="Chat__Footer">
                <input
                    className="Chat__Input form-control"
                    type="text"
                    id="message"
                    name="message"
                    autoComplete="off"
                    placeholder="CLICK TO CHAT"
                    value={message}
                    onChange={handleMessage}
                    onFocus={handleTouched}
                    onBlur={handleBlur}
                    onKeyPress={handleKeyPress}
                />
            </div>
        </div>
    );
}

Chat.propTypes = {
    closeWindow: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    messages: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
    const messages = _.get(state, 'chat.filteredMessages', []);
    return {
        messages,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateToProps, dispatchStateToProps)(Chat);
