import Chat from './chat';
import Help from './help';
import History from './history';
import Settings from './settings';

export {
    Chat, Help, History, Settings,
};
