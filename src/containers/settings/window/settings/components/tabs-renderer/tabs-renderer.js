import React from 'react';
import * as PropTypes from 'prop-types';
import { General, Video, Sound } from './tabs';

export default function TabsRenderer({ type }) {
    const components = {
        General,
        Video,
        Sound,
    };

    const TagName = components[type || General];

    return (<TagName />);
}

TabsRenderer.propTypes = {
    type: PropTypes.oneOf(['General', 'Video', 'Sound']).isRequired,
};
