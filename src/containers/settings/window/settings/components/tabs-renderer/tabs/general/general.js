import React from 'react';
import { connect } from 'react-redux';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import MainSettingsModel from '../../../../../../../../models/main-settings-model';
import { app as appActions } from '../../../../../../../../store/actions';

import './general.scss';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing(2),
    },
}));

function General({ mainSettings, dispatch }) {
    const classes = useStyles();

    const handleChange = (event) => {
        const { name, checked } = event.target;
        dispatch(appActions.setMainSetting({ name, value: !!(checked) }));
    };
    const { hideMessages, showBetStatistic } = mainSettings;
    return (
        <div className={cn({ [classes.root]: true, General: true })}>
            <FormControl component="fieldset" className={classes.formControl}>
                <FormGroup>
                    <FormControlLabel
                        control={<Checkbox checked={hideMessages} onChange={handleChange} name="hideMessages" color="primary" />}
                        label="Hide other players chat"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={showBetStatistic} onChange={handleChange} name="showBetStatistic" color="primary" />}
                        label="Show betting statistics"
                    />
                </FormGroup>
            </FormControl>
        </div>
    );
}

General.propTypes = {
    mainSettings: PropTypes.instanceOf(MainSettingsModel).isRequired,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const mainSettings = _.get(state, 'app.mainSettings');
    return {
        mainSettings,
    };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, dispatchStateToProps)(General);
