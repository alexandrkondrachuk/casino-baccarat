import React from 'react';
import VolumeSlider from '../../../volume-slider';

import './sound.scss';

export default function Sound() {
    return (
        <div className="Sound">
            <VolumeSlider />
        </div>
    );
}
