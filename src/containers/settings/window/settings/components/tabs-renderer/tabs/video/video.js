import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

import './video.scss';

export default function Video() {
    const [value, setValue] = React.useState('auto');
    const [valueContent, setValueContent] = React.useState('multiple');

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    const handleChangeContent = (event) => {
        setValueContent(event.target.value);
    };

    return (
        <div className="Video">
            <h4 className="legend">Video Quality</h4>
            <FormControl className="Video__Primary" component="fieldset">
                <RadioGroup aria-label="videoQuality" name="videoQuality1" value={value} onChange={handleChange}>
                    <FormControlLabel value="auto" control={<Radio color="primary" />} label="Auto adjust" />
                    <FormControlLabel value="hd" control={<Radio color="primary" />} label="Hd" />
                    <FormControlLabel value="medium" control={<Radio color="primary" />} label="Medium" />
                    <FormControlLabel value="low" control={<Radio color="primary" />} label="Low" />
                </RadioGroup>
            </FormControl>
            <hr />
            <h4 className="legend">Video Content</h4>
            <FormControl className="Video__Secondary" component="fieldset">
                <RadioGroup aria-label="videoContent" name="videoContent1" value={valueContent} onChange={handleChangeContent}>
                    <FormControlLabel value="multiple" control={<Radio color="primary" />} label="Multiple cameras" />
                    <FormControlLabel value="single" control={<Radio color="primary" />} label="Single camera" />
                </RadioGroup>
            </FormControl>
            <hr />
        </div>
    );
}
