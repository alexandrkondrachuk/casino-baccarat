import General from './general';
import Sound from './sound';
import Video from './video';

export {
    General, Sound, Video,
};
