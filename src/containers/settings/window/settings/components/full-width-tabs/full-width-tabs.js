import React from 'react';
import PropTypes from 'prop-types';
import * as cn from 'classnames';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Types from '../../../../../../classes/Types';
import TabsRenderer from '../tabs-renderer';

import './full-width-tabs.scss';

const tabs = Types.mainSettingsTabs;

function TabPanel(props) {
    const {
        children, value, index, ...other
    } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            className={`tab-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={2}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    // eslint-disable-next-line react/require-default-props
    children: PropTypes.node,
    // eslint-disable-next-line react/forbid-prop-types
    index: PropTypes.any.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: '100%',
    },
}));

export default function FullWidthTabs() {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <div id="Full-width-tabs" className={cn({ [classes.root]: true, 'Full-width-tabs': true })}>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    { (Array.isArray(tabs) && tabs.length > 0) && (
                        tabs.map((tab) => (<Tab key={tab.index} label={tab.label} {...a11yProps(tab.index)} />))
                    ) }
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                {(Array.isArray(tabs) && tabs.length > 0) && (
                    tabs.map((tab) => (
                        <TabPanel key={tab.index} value={value} index={tab.index} dir={theme.direction}>
                            <TabsRenderer type={tab.component} />
                        </TabPanel>
                    ))
                )}
            </SwipeableViews>
        </div>
    );
}
