import React from 'react';
import * as cn from 'classnames';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import VolumeDown from '@material-ui/icons/VolumeDown';
import VolumeUp from '@material-ui/icons/VolumeUp';
import { app as appActions } from '../../../../../../store/actions';

import './volume-slider.scss';
import MainSettingsModel from '../../../../../../models/main-settings-model';

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
});

const settings = {
    min: 0,
    max: 1,
    step: 0.1,
};

function VolumeSlider({ mainSettings, player, dispatch }) {
    const classes = useStyles();
    const { voice, sound } = mainSettings;

    const handleChangeVolume = (event, newValue) => {
        if (player) {
            if (newValue > 0) player.unmute();
            if (newValue === 0) player.mute();
            player.setVolume(newValue);
            dispatch(appActions.setMainSetting({ name: 'voice', value: newValue }));
        }
    };

    const handleChangeGameSound = (event, newValue) => {
        dispatch(appActions.setSoundEffectVolume(newValue));
        dispatch(appActions.setMainSetting({ name: 'sound', value: newValue }));
    };

    return (
        <div className={cn({ [classes.root]: true, VolumeSlider: true })}>
            <div className="VolumeSlider__Volume">
                <Typography id="volume" gutterBottom>
                    Volume
                </Typography>
                <Grid container spacing={2}>
                    <Grid item>
                        <VolumeDown />
                    </Grid>
                    <Grid item xs>
                        <Slider {...settings} value={voice} onChange={handleChangeVolume} aria-labelledby="volume" />
                    </Grid>
                    <Grid item>
                        <VolumeUp />
                    </Grid>
                </Grid>
            </div>
            <div className="VolumeSlider__GameSound">
                <Typography id="game-sound" gutterBottom>
                    Game Sound
                </Typography>
                <Grid container spacing={2}>
                    <Grid item>
                        <VolumeDown />
                    </Grid>
                    <Grid item xs>
                        <Slider {...settings} value={sound} onChange={handleChangeGameSound} aria-labelledby="game-sound" />
                    </Grid>
                    <Grid item>
                        <VolumeUp />
                    </Grid>
                </Grid>
            </div>
        </div>
    );
}

VolumeSlider.defaultProps = {
    player: null,
};

VolumeSlider.propTypes = {
    mainSettings: PropTypes.instanceOf(MainSettingsModel).isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const mainSettings = _.get(state, 'app.mainSettings');
    const player = _.get(state, 'stream.playerInstance');
    return {
        mainSettings,
        player,
    };
};
const dispatchStateToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, dispatchStateToProps)(VolumeSlider);
