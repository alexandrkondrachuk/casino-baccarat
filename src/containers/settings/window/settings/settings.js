import React from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Icon } from '../../../../components/svg-components';
import { FullWidthTabs } from './components';
import config from '../../../../config';

import './settings.scss';

function Settings({ closeWindow }) {
    const version = config.get('version');
    const commitHash = _.get(Baccarat, 'shortHash', '0000');
    const close = () => {
        closeWindow();
    };
    return (
        <div className="Settings__Modal">
            <div className="Settings__Modal__Header">
                <h1>
                    <span className="Settings__Modal__Icon__Settings">
                        <Icon path="settings" />
                        {' '}
                    </span>
                    Settings
                    &nbsp;
                    <span className="Settings__Modal__Icon__Close">
                        <Icon path="close" onClick={close} />
                        {' '}
                    </span>
                </h1>
            </div>
            <div className="Settings__Modal__Body">
                <FullWidthTabs />
            </div>
            <div className="Settings__Modal__Footer">
                <p>
                    <span className="title">Version </span>
                    &nbsp;
                    <span>
                        { version }
                        -
                        { commitHash }
                    </span>
                </p>
            </div>
        </div>
    );
}

Settings.propTypes = {
    closeWindow: PropTypes.func.isRequired,
};

export default Settings;
