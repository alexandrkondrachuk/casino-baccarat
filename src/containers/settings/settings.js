import React, { useRef } from 'react';
import * as _ from 'lodash';
import * as PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Fab from '@material-ui/core/Fab';
import screenfull from 'screenfull';
import { Animated } from 'react-animated-css';
import { Icon } from '../../components/svg-components';
import SettingsModel from '../../models/settings-model';
import { game as gameActions, app as appActions } from '../../store/actions';
import Window from './window/window';
import BaccaratRoundModel from '../../models/baccarat-round-model';
import Tooltip from '../../components/tooltip';
import { useOutsideClick } from '../../hooks';
import config from '../../config';

import './settings.scss';

const animationDuration = config.get('animationDuration.settings');

function Settings({
    settings, player, round, dispatch, dealerName,
}) {
    const {
        enableFullscreen, enableSound, infoOpen, showChat, showConfig, showHistory, showHelp,
    } = settings;
    const roundId = _.get(round, 'id', 1111);
    const ref = useRef(null);

    const toggleFullscreenMode = () => {
        const fullscreenTarget = document.documentElement;

        if (screenfull.isEnabled && !enableFullscreen) {
            dispatch(gameActions.setSettings(new SettingsModel({ ...settings, enableFullscreen: true })));
            screenfull.request(fullscreenTarget);
        } else {
            dispatch(gameActions.setSettings(new SettingsModel({ ...settings, enableFullscreen: false })));
            screenfull.toggle(fullscreenTarget);
        }
    };

    const toggleSound = () => {
        if (!enableSound) {
            dispatch(gameActions.setSettings(new SettingsModel({ ...settings, enableSound: true })));
            dispatch(appActions.setSoundEffectVolume(1));
            dispatch(appActions.setMainSetting({ name: 'voice', value: 1 }));
            dispatch(appActions.setMainSetting({ name: 'sound', value: 1 }));
            if (player) {
                player.unmute();
                player.setVolume(1);
            }
        } else {
            dispatch(gameActions.setSettings(new SettingsModel({ ...settings, enableSound: false })));
            dispatch(appActions.setSoundEffectVolume(0));
            dispatch(appActions.setMainSetting({ name: 'voice', value: 0 }));
            dispatch(appActions.setMainSetting({ name: 'sound', value: 0 }));
            if (player) {
                player.mute();
                player.setVolume(0);
            }
        }
    };

    const openSettingProp = (prop = 'showChat') => {
        if (infoOpen && !_.get(settings, `[${prop}]`)) {
            dispatch(gameActions.setSettings(new SettingsModel({
                enableSound: settings.enableSound,
                enableFullscreen: settings.enableFullscreen,
                showConfig: false,
                showChat: false,
                showHistory: false,
                showHelp: false,
                infoOpen: false,
            })));
            _.delay(() => {
                dispatch(gameActions.setSettings(new SettingsModel({
                    enableSound: settings.enableSound,
                    enableFullscreen: settings.enableFullscreen,
                    [prop]: true,
                    infoOpen: true,
                })));
            }, 0);
        } else {
            dispatch(gameActions.setSettings(new SettingsModel({
                enableSound: settings.enableSound,
                enableFullscreen: settings.enableFullscreen,
                [prop]: true,
                infoOpen: true,
            })));
        }
    };

    const toggleChat = () => openSettingProp();

    const toggleSettings = () => openSettingProp('showConfig');

    const toggleHistory = () => openSettingProp('showHistory');

    const toggleHelp = () => openSettingProp('showHelp');

    const closeWindow = () => {
        dispatch(gameActions.setSettings(new SettingsModel({
            ...settings,
            showConfig: false,
            showChat: false,
            showHistory: false,
            showHelp: false,
            infoOpen: false,
        })));
    };

    useOutsideClick(ref, () => {
        closeWindow();
    });

    return (
        <div className="Settings" ref={ref}>
            <div className="Settings__Panel">
                <div className="Setting">
                    <Fab id="s1" color="primary" onClick={toggleChat}>
                        <Icon path="chat" />
                    </Fab>
                    <Tooltip targetId="s1">
                        Show chat
                    </Tooltip>
                </div>
                <div className="Setting">
                    <Fab color="primary" onClick={toggleSound}>
                        {enableSound ? <Icon path="soundOn" /> : <Icon path="soundOff" />}
                    </Fab>
                </div>
                <div id="s2" className="Setting">
                    <Fab color="primary" onClick={toggleSettings}>
                        <Icon path="settings" />
                    </Fab>
                    <Tooltip targetId="s2">
                        Settings
                    </Tooltip>
                </div>
                <div id="s3" className="Setting">
                    <Fab color="primary" onClick={toggleHistory}>
                        <Icon path="history" />
                    </Fab>
                    <Tooltip targetId="s3">
                        History
                    </Tooltip>
                </div>
                <div id="s4" className="Setting">
                    <Fab color="primary" onClick={toggleHelp}>
                        <Icon path="help" />
                    </Fab>
                    <Tooltip targetId="s4">
                        Help
                    </Tooltip>
                </div>
                <div id="s5" className="Setting">
                    <Fab color="primary" onClick={toggleFullscreenMode}>
                        {enableFullscreen ? <Icon path="fullScreenOn" /> : <Icon path="fullScreenOff" />}
                    </Fab>
                    <Tooltip targetId="s5">
                        Full screen
                    </Tooltip>
                </div>
            </div>
            <div className="Settings__Details">
                <div className="Detail Dealer">
                    <span>Dealer</span>
                    :&nbsp;
                    <span>{dealerName}</span>
                </div>
                <div className="Detail Round">
                    <span>Round ID</span>
                    :&nbsp;
                    <span>{roundId}</span>
                </div>
            </div>
            <div className="Settings__Window">
                <Animated animationIn="fadeInRight" animationOut="fadeOutRight" animationInDuration={animationDuration} animationOutDuration={animationDuration} isVisible={showChat}>
                    {showChat && <Window tag="chat" closeWindow={closeWindow} />}
                </Animated>
                <Animated animationIn="fadeInRight" animationOut="fadeOutRight" animationInDuration={animationDuration} animationOutDuration={animationDuration} isVisible={showConfig}>
                    {showConfig && <Window tag="settings" closeWindow={closeWindow} />}
                </Animated>
                <Animated animationIn="fadeInRight" animationOut="fadeOutRight" animationInDuration={animationDuration} animationOutDuration={animationDuration} isVisible={showHistory}>
                    {showHistory && <Window tag="history" closeWindow={closeWindow} />}
                </Animated>
                <Animated animationIn="fadeInRight" animationOut="fadeOutRight" animationInDuration={animationDuration} animationOutDuration={animationDuration} isVisible={showHelp}>
                    {showHelp && <Window tag="help" closeWindow={closeWindow} />}
                </Animated>
            </div>
        </div>
    );
}

Settings.defaultProps = {
    settings: new SettingsModel(),
    player: null,
};

Settings.propTypes = {
    settings: PropTypes.instanceOf(SettingsModel),
    dispatch: PropTypes.func.isRequired,
    // eslint-disable-next-line react/forbid-prop-types
    player: PropTypes.object,
    round: PropTypes.instanceOf(BaccaratRoundModel).isRequired,
    dealerName: PropTypes.string.isRequired,
};

const mapStateTopProps = (state) => {
    const settings = _.get(state, 'game.settings');
    const player = _.get(state, 'stream.playerInstance');
    const round = _.get(state, 'game.round');
    const dealerName = _.get(state, 'game.round.dealerName', 'Admin');

    return {
        settings,
        player,
        round,
        dealerName,
    };
};

const dispatchStateToProps = (dispatch) => ({
    dispatch,
});

export default connect(mapStateTopProps, dispatchStateToProps)(Settings);
