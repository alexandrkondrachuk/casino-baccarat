import React, { Component } from 'react';
import * as PropTypes from 'prop-types';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import {
    Redirect,
    withRouter,
    BrowserRouter as Router,
    Switch,
} from 'react-router-dom';
import { routes, RouteWithSubRoutes } from '../../routes';
import config from '../../config';
import { I18nPropvider, LOCALES } from '../../lang';
import { stream as streamActions, game as gameActions, app as appActions } from '../../store/actions';
import AlertCustom from '../../components/alert';
import AlertModel from '../../models/alert-model';
import { getServerConfig } from '../../services';

import './App.scss';

// eslint-disable-next-line react/prefer-stateless-function
class App extends Component {
    componentDidMount() {
        this.initGameSession();
    }

    showErrorAlert = () => {
        const { dispatch } = this.props;
        dispatch(appActions.setAlertModel(new AlertModel({
            type: 'error',
            open: true,
            autoHideDuration: 10 * 1000,
            text: 'Internal Server Error. Sorry for the inconvenience. Please, try again later',
        })));
    };

    setStreamConfig = (serverConfig) => {
        const { dispatch } = this.props;
        dispatch(streamActions.updateConfig(_.get(serverConfig, 'playerSettings')));
    };

    setGameReadyState = () => {
        const { dispatch } = this.props;
        dispatch(gameActions.setReadyState(true));
    };

    initGameSession = async () => {
        try {
            const serverConfig = await getServerConfig();
            if (!serverConfig) {
                this.showErrorAlert();
            }
            this.setStreamConfig(serverConfig);
            _.delay(() => { this.setGameReadyState(); }, config.get('authDelay'));
        } catch (err) {
            this.showErrorAlert();
            console.error(err);
        }
    };

    render() {
        const { lang } = this.props;
        return (
            <div className="App">
                <I18nPropvider locale={lang}>
                    <section className="route-section">
                        <Router>
                            <Switch>
                                {routes.map((route) => (
                                    <RouteWithSubRoutes key={route.id} {...route} />
                                ))}
                                <Redirect from="*" to={_.get(routes, '[0].path')} />
                            </Switch>
                        </Router>
                    </section>
                    <AlertCustom />
                </I18nPropvider>
            </div>
        );
    }
}

App.defaultProps = {
    lang: LOCALES.en,
};

App.propTypes = {
    lang: PropTypes.string,
    dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    const lang = _.get(state.app, 'lang');
    return { lang };
};

const mapDispatchToProps = (dispatch) => ({
    dispatch,
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
