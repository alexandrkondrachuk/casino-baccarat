const copydir = require('copy-dir');
const fs = require('fs');
const path = require('path');
const JavaScriptObfuscator = require('javascript-obfuscator');
const colors = require('colors');
const config = require('./config');

const {
    fromDir, toDir, jsDir, levels, level,
} = config;

function addObfuscatorToFile(fromPath, toPath, cb) {
    // Read the file of your original JavaScript Code as text
    fs.readFile(fromPath, 'UTF-8', (err, data) => {
        if (err) {
            throw err;
        }

        // Obfuscate content of the JS file
        const obfuscationResult = JavaScriptObfuscator.obfuscate(data, levels[level]);

        // Write the obfuscated code into a new file
        // eslint-disable-next-line no-shadow,consistent-return
        fs.writeFile(toPath, obfuscationResult.getObfuscatedCode(), (err) => {
            if (err) {
                return console.log(err);
            }
            console.log(`${colors.cyan.underline('Step #3')}: ${colors.green('Obfuscated')} the file ${colors.yellow(toPath)}`);
            cb();
        });
    });
}

function deleteFile(filePath) {
    try {
        fs.unlinkSync(filePath);
        console.log(`${colors.cyan.underline('Step #2')}: ${colors.red('Removed')} the map file ${colors.yellow(filePath)}`);
    } catch (err) {
        console.error(err);
    }
}

function obfuscationInit() {
    if (fs.existsSync(toDir)) {
        try {
            fs.rmdirSync(toDir, { recursive: true });
        } catch (e) {
            console.error(`Error while deleting ${toDir}`);
        }
    }
    copydir(fromDir, toDir, {
        utimes: true, // keep add time and modify time
        mode: true, // keep file mode
        cover: true, // cover file when exists, default is true
    }, (err) => {
        if (err) throw err;
        console.log(`${colors.cyan.underline('Step #1')}: ${colors.green('Build')} files have been copy to ${colors.yellow(path.basename(toDir))}`);
        // eslint-disable-next-line no-shadow
        fs.readdir(path.join(fromDir, jsDir), (err, files) => {
            if (err) {
                throw err;
            }

            // files object contains all files names
            // log them on console
            const jsFilesPatter = RegExp('.js$');
            const mapFilesPatter = RegExp('.map$');
            const jsFiles = files.filter((file) => jsFilesPatter.test(file));
            const mapFiles = files.filter((file) => mapFilesPatter.test(file));
            let jsFilesCounter = 0;

            // Add obfuscator to js files
            jsFiles.forEach((file) => {
                const from = path.join(fromDir, jsDir, file);
                const to = path.join(toDir, jsDir, file);
                // eslint-disable-next-line max-len
                addObfuscatorToFile(from, to, () => { jsFilesCounter += 1; if (jsFilesCounter === jsFiles.length) console.log(`${colors.cyan.underline('Step #4')}: ${colors.green('Success!')} - The ${colors.yellow(path.basename(toDir))} folder is ready to be deployed.`); });
            });
            // Delete *.map files
            mapFiles.forEach((file) => {
                const filePath = path.join(toDir, jsDir, file);
                deleteFile(filePath);
            });
        });
    });
}

module.exports = {
    deleteFile,
    addObfuscatorToFile,
    obfuscationInit,
};
